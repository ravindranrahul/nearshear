package com.nearshear;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.nearshear.api.ApiUtils;
import com.nearshear.model.RegistrationDO;
import com.nearshear.model.RegistrationInfo;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPassword extends  BaseActivity implements View.OnClickListener {
    private LinearLayout laySignin;
    private Button btnRetrieve;
    private EditText edtEmailId;
    private String strEmail;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.color.white_gray);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(this.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        setViews();
    }

    private void setViews() {
        laySignin=findViewById(R.id.laySignIn);
        laySignin.setOnClickListener(this);
        btnRetrieve=findViewById(R.id.btnRetrieve);
        btnRetrieve.setOnClickListener(this);
        edtEmailId=findViewById(R.id.edtEmailId);
    }

    private void callForgotPassword() {
        utils.startProgress();
        ApiUtils.getAPIService().requestForgotPassword(strEmail).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                utils.dismissProgress();
                try {
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();

                    if (success.equals("1")) {
                        ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                        listOfPostData = response.body().getPostdata();
                        RegistrationInfo registrationInfo = listOfPostData.get(0);
                        String id = registrationInfo.getStrKeyId();

                        Intent otp = new Intent(ForgotPassword.this, OtpActivity.class);
                        Bundle mBundle = new Bundle();
                        mBundle.putString("id", id);
                        otp.putExtras(mBundle);
                        startActivity(otp);
//                        finish();

//                        utils.Toast("Check email for reset password");
                    } else if (success.equals("0")) {
                        utils.Toast(msg, coordinatorLayout);
                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                utils.Toast("Please check connection", null);
            }
        });
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.laySignIn:
//                Intent login =new Intent(ForgotPassword.this,SignUpActivity.class);
//                startActivity(login);
                finish();
                break;
            case R.id.btnRetrieve:
                strEmail=edtEmailId.getText().toString();
                if(strEmail.equals("")) {
                    utils.Toast("Enter email id");
                }else{
                    if(utils.isEmailValid(strEmail)){
                        if (utils.isNetConnected()) {
                            callForgotPassword();
                        } else {
                            utils.Toast(R.string.check_net, null);
                        }
                    }else{
                        utils.Toast("Enter valid email");
                    }
                }
                break;
        }
    }


}
