package com.nearshear;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.nearshear.api.ApiUtils;
import com.nearshear.model.RegistrationDO;
import com.nearshear.model.RegistrationInfo;
import com.nearshear.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SignUpActivity extends BaseActivity implements View.OnClickListener {

    private TextView txtSignIn;
    private TextView txtSignUp;
    private TextView txtFB;
    private TextView txtGoogle;
    private TextView using;
    private TextView signinBtn;
    private TextView signupBtn;
    private TextView txtForgotPassword;
    private TextView txtTandC;

    private LinearLayout WithFb;
    private LinearLayout withGp;
    private RelativeLayout laySignIn;
    private RelativeLayout laySignUP;
    private EditText edt_login_Email, edtPassword;
    private String strEmail = "";
    private String strPass = "";
    private Utils utils;

    private EditText etName, etEmailsinup, etMobile, etPasswordsignup;
    private String strName;
    private String strEmailsinup;
    private String strMobile;
    private String strPwdsinup;
    private String strImage = "";

    //
    private GoogleSignInClient mGoogleSignInClient;
    private static final String TAG = "SignInActivity";
    private static final int RC_SIGN_IN = 9001;

    //
    private static final String EMAIL = "email";
    private LoginButton loginButton;
    private CallbackManager callbackManager;
    private LinearLayout layCheckBox;
    private CheckBox chkTerms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.color.white_gray);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(this.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_sign_up);
        init();
        setToolBar();


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    private void init() {
        utils = new Utils(this);
        txtSignIn = findViewById(R.id.txtSignIn);
        txtSignUp = findViewById(R.id.txtSignUp);
        txtForgotPassword = findViewById(R.id.txtForgotPassword);
        txtTandC = findViewById(R.id.txtTandC);
        txtTandC.setOnClickListener(this);

        laySignIn = findViewById(R.id.laySignIn);
        laySignUP = findViewById(R.id.rrsignup);

        WithFb = findViewById(R.id.SignInFb);
        withGp = findViewById(R.id.SignInGp);

        txtFB = findViewById(R.id.txtfb);
        txtGoogle = findViewById(R.id.txtgp);
        using = findViewById(R.id.tvusing);

        edt_login_Email = findViewById(R.id.etemailsignin);
        edtPassword = findViewById(R.id.etpasswordsignin);

        etName = findViewById(R.id.etname);
        etEmailsinup = findViewById(R.id.etemailsignup);
//        etMobile = findViewById(R.id.etphone);
        etPasswordsignup = findViewById(R.id.etpasswordsignup);

        signinBtn = findViewById(R.id.btnSignIn);
        signupBtn = findViewById(R.id.btnSignup);

        txtSignIn.setOnClickListener(this);
        txtSignUp.setOnClickListener(this);
        signinBtn.setOnClickListener(this);
        signinBtn.setOnClickListener(this);
        signupBtn.setOnClickListener(this);
        withGp.setOnClickListener(this);
        WithFb.setOnClickListener(this);
        txtForgotPassword.setOnClickListener(this);
        layCheckBox = findViewById(R.id.layCheckBox);
        layCheckBox.setOnClickListener(this);
        chkTerms = findViewById(R.id.chkTerms);
        chkTerms.setChecked(true);


        callbackManager = CallbackManager.Factory.create();
//        loginButton = (LoginButton) findViewById(R.id.login_button);
//        loginButton.setReadPermissions(Arrays.asList(EMAIL));


//        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//                // App code
//                getUserDetails(loginResult);
//            }
//
//            @Override
//            public void onCancel() {
//                // App code
//            }
//
//            @Override
//            public void onError(FacebookException exception) {
//                // App code
//            }
//        });

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        getUserDetails(loginResult);
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });
    }


    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }
    }

    private void updateUI(@Nullable GoogleSignInAccount account) {
        if (account != null) {
            strName = account.getDisplayName();
            strEmail = account.getEmail();
            try {
                strImage = account.getPhotoUrl().toString();
            } catch (Exception e) {
                strImage = "";
                e.printStackTrace();
            }


            if (utils.isNetConnected()) {
                callLogin("isGplus");
            } else {
                utils.Toast(R.string.check_net, null);
            }
        } else {

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtSignIn:
                txtSignIn.setTextAppearance(this, R.style.txtBigOrangeMedium);
                txtSignUp.setTextAppearance(this, R.style.txtBigOrangeLight);

                laySignUP.setVisibility(View.GONE);
                laySignIn.setVisibility(View.VISIBLE);
                txtSignIn.setTextColor(getResources().getColor(R.color.orange));
                txtSignUp.setTextColor(getResources().getColor(R.color.black_trans));
                txtFB.setText("Sign In with");
                txtGoogle.setText("Sign In with");
                using.setText("Or sign in using social media");
                break;

            case R.id.txtSignUp:
                txtSignUp.setTextAppearance(this, R.style.txtBigOrangeMedium);
                txtSignIn.setTextAppearance(this, R.style.txtBigOrangeLight);

                laySignIn.setVisibility(View.GONE);
                laySignUP.setVisibility(View.VISIBLE);
                txtSignUp.setTextColor(getResources().getColor(R.color.orange));
                txtSignIn.setTextColor(getResources().getColor(R.color.black_trans));
                txtFB.setText("Sign Up with");
                txtGoogle.setText("Sign Up with");
                using.setText("Or sign up using social media");
                break;

            case R.id.btnSignIn:
                strEmail = edt_login_Email.getText().toString();
                strPass = edtPassword.getText().toString();
                if (strEmail.isEmpty()) {
                    utils.Toast("Email Required", null);
                } else if (!utils.isEmailValid(strEmail)) {
                    utils.Toast("Incorrect email format", null);
                } else if (strPass.isEmpty()) {
                    utils.Toast("Password required", null);
                } else {
                    if (utils.isNetConnected()) {
                        callLogin("isApp");
                    } else {
                        utils.Toast(R.string.check_net, null);
                    }
                }
                break;

            case R.id.btnSignup:
                strName = etName.getText().toString();
                strEmailsinup = etEmailsinup.getText().toString();
//                strMobile = etMobile.getText().toString();
                strPwdsinup = etPasswordsignup.getText().toString();

                if (strName.isEmpty()) {
                    utils.Toast("Name Required", null);
                } else if (strEmailsinup.isEmpty()) {
                    utils.Toast("Email Required", null);
                } else if (!utils.isEmailValid(strEmailsinup)) {
                    utils.Toast("Incorrect email format", null);
                }
//                else if (strMobile.isEmpty()) {
//                    utils.Toast("Phone Number Required", null);
//                }
                else if (strPwdsinup.isEmpty()) {
                    utils.Toast("password Required", null);
                } else {
                    if (chkTerms.isChecked()) {
                        if (utils.isNetConnected()) {
                            callRegistration("isApp");
                        } else {
                            utils.Toast(R.string.check_net, null);
                        }
                    } else {
                        utils.Toast("Please accept term and conditions");
                    }
                }
                break;


            case R.id.SignInGp:
                if (utils.isNetConnected()) {
                    signIn();
                } else {
                    utils.Toast(R.string.check_net, null);
                }
                break;

            case R.id.SignInFb:
                if (utils.isNetConnected()) {
                    LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile"));
                } else {
                    utils.Toast(R.string.check_net, null);
                }
                break;

            case R.id.txtForgotPassword:
                Intent forgot = new Intent(SignUpActivity.this, ForgotPassword.class);
                startActivity(forgot);
//                finish();
                break;
            case R.id.layCheckBox:
                if (chkTerms.isChecked()) {
                    chkTerms.setChecked(false);
                } else {
                    chkTerms.setChecked(true);
                }
                break;

            case R.id.txtTandC:
                dialog();
                break;
        }
    }

    public void dialog() {
        final Dialog dialogTandC= new Dialog(this);
        dialogTandC.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogTandC.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        dialogTandC.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogTandC.setContentView(R.layout.dialog_term_and_condition);
        dialogTandC.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        dialogTandC.setCanceledOnTouchOutside(false);
        dialogTandC.show();

        WebView mWebview = dialogTandC.findViewById(R.id.webView);
        ImageView imgBack =dialogTandC.findViewById(R.id.imgBack);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogTandC.dismiss();
            }
        });

        mWebview.setWebViewClient(new WebViewClient() {

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                utils.startProgress();

                view.loadUrl(url);

                return true;
            }

            public void onPageFinished(WebView view, String url) {
                try {
                    utils.dismissProgress();
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        });

        mWebview.getSettings().setJavaScriptEnabled(true); // enable javascript


        mWebview.loadUrl(ApiUtils.BASE_URL+"terms_and_conditions.html");
    }

    protected void getUserDetails(LoginResult loginResult) {
        GraphRequest data_request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject json_object,
                            GraphResponse response) {
//                        Intent intent = new Intent(SignUpActivity.this,
//                                MainActivity.class);
//                        intent.putExtra("userProfile", json_object.toString());
//                        startActivity(intent);
                        Log.d("FB::::", json_object.toString());
                        try {
                            String id = json_object.getString("id");
                            String name = json_object.getString("name");
                            String email = json_object.getString("email");
                            JSONObject jsonData = json_object.getJSONObject("picture");
//                            JSONArray arrJson = jsonData.getJSONArray("data");
                            try {
                                strImage = json_object.getJSONObject("picture").getJSONObject("data").getString("url");
                            } catch (JSONException e) {
                                strImage = "";
                                e.printStackTrace();
                            }


                            strName = name;
                            strEmail = email;
                            if (utils.isNetConnected()) {
                                callLogin("isFb");
                            } else {
                                utils.Toast(R.string.check_net, null);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                });
        Bundle permission_param = new Bundle();
        permission_param.putString("fields", "id,name,email,picture.width(120).height(120)");
        data_request.setParameters(permission_param);
        data_request.executeAsync();

    }


    private void callLogin(final String logintype) {
        try {
            if (Constant.PREF_DEVICE_TOKEN.equals("")) {
                Constant.PREF_DEVICE_TOKEN = FirebaseInstanceId.getInstance().getToken();
            }
        } catch (Exception e) {
        }
        utils.startProgress();
        ApiUtils.getAPIService().requestLogin(strName, strImage, strEmail, strPass, logintype, Constant.PREF_DEVICE_TOKEN, "android", utils.timeZoneId()).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                utils.dismissProgress();
                try {
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();

                    if (success.equals("1")) {
                        ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                        listOfPostData = response.body().getPostdata();
                        RegistrationInfo registrationInfo = listOfPostData.get(0);
                        String id = registrationInfo.getStrKeyId();
                        String name = registrationInfo.getStrFirstName();
                        String email = registrationInfo.getStrEmail();
                        String phone = registrationInfo.getStrMobile();
                        String image = registrationInfo.getImage();
//                        String area = registrationInfo.getArea();
//                        String pin = registrationInfo.getPin();
//                        String city = registrationInfo.getCity();


                        utils.setPreference(Constant.PREF_ID, id);
                        utils.setPreference(Constant.PREF_NAME, name);
                        utils.setPreference(Constant.PREF_EMAIL, email);
                        utils.setPreference(Constant.PREF_PHONE, phone);
//                        utils.setPreference(Constant.PREF_IMAGE, strImage);
                        utils.setPreference(Constant.PREF_IMAGE, image);
                        utils.setPreference(Constant.PREF_LOGIN_TYPE, logintype);
//                        utils.setPreference(Constant.PREF_SOCIETY_NAME, society_name);
//                        utils.setPreference(Constant.PREF_AREA, area);
//                        utils.setPreference(Constant.PREF_PIN, pin);
//                        utils.setPreference(Constant.PREF_CITY, city);

                        startActivity(new Intent(SignUpActivity.this, MainActivity.class));
                        finish();

                    }
//                    else if (success.equals("2")) {
//                        ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
//                        listOfPostData = response.body().getPostdata();
//                        RegistrationInfo registrationInfo = listOfPostData.get(0);
//                        String id = registrationInfo.getStrKeyId();
//
//                        Intent otp = new Intent(SignUpActivity.this, OtpActivity.class);
//                        Bundle mBundle = new Bundle();
//                        mBundle.putString("id", id);
//                        otp.putExtras(mBundle);
//                        startActivity(otp);
//                        finish();
//                    }
                    else if (success.equals("0")) {
                        utils.Toast(msg, null);
                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                utils.Toast("Please check connection", null);
            }
        });
    }

    private void callRegistration(final String Ltype) {
        try {
            if (Constant.PREF_DEVICE_TOKEN.equals("")) {
                Constant.PREF_DEVICE_TOKEN = FirebaseInstanceId.getInstance().getToken();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        utils.startProgress();
        ApiUtils.getAPIService().requestUserRegistration(strName, strEmailsinup, strPwdsinup, strMobile, Constant.PREF_DEVICE_TOKEN, "android", utils.timeZoneId(), Ltype).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                utils.dismissProgress();
                try {
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();
                    if (success.equals("1")) {
                        ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                        listOfPostData = response.body().getPostdata();
                        RegistrationInfo registrationInfo = listOfPostData.get(0);

                        String id = registrationInfo.getStrKeyId();
                        String image = registrationInfo.getImage();


                        utils.setPreference(Constant.PREF_ID, id);
                        utils.setPreference(Constant.PREF_NAME, strName);
                        utils.setPreference(Constant.PREF_EMAIL, strEmailsinup);
                        utils.setPreference(Constant.PREF_PHONE, strMobile);
                        utils.setPreference(Constant.PREF_IMAGE, image);
                        utils.setPreference(Constant.PREF_LOGIN_TYPE, Ltype);

                        startActivity(new Intent(SignUpActivity.this, MainActivity.class));
                        finish();
//                        Intent intent = new Intent(SignUpActivity.this, OtpActivity.class);
//                        Bundle mBundle = new Bundle();
//                        mBundle.putString("id", id);
//                        intent.putExtras(mBundle);
//                        startActivity(intent);

                    } else {
                        utils.Toast(msg, coordinatorLayout);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.Toast("Please check connection", null);
            }
        });
    }


}
