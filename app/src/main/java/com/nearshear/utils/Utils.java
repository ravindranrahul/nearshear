/**
 * @author Vivek Shah
 * 
 * This class is made as common or utility class where the all common methods and
 * also common variables are stored which can be used in whole development time.
 *  
 * You should declare this class  in every activity to use common things like alert and toast even , 
 * just need to pass context to constructor from there.
 * 
 */
package com.nearshear.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.telephony.TelephonyManager;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.Toast;


import com.nearshear.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;


public class Utils extends Application {

	// This variable store the context of Application
	public static Context applicationContext;

	// private context made because no one can use ot store in this context
	private Context context = null;
	private String LOG_TAG = "NewsPage";
	private SharedPreferences preferences = null;
	private SharedPreferences.Editor editor = null;
	private ProgressDialog dialog;
	Animation animation;
	private ImageView imageProgress;
	private char[] aToz = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
			'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
			'x', 'y', 'z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
			'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
			'x', 'y', 'z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
			'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
			'x', 'y', 'z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
			'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
			'x', 'y', 'z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
			'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
			'x', 'y', 'z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
			'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
			'x', 'y', 'z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
			'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
			'x', 'y', 'z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
			'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
			'x', 'y', 'z' };

	/*
	 * Parameterized Constructor made because getting fresh context every time
	 * and to make methods easy.
	 */
	public Utils(Context con) {
		context = con;
		dialog = new ProgressDialog(con);
		preferences = PreferenceManager.getDefaultSharedPreferences(con);
		editor = preferences.edit();
//		animation = AnimationUtils.loadAnimation(con, R.anim.rotate);
//		imageProgress = (ImageView) ((Activity) con)
//				.findViewById(R.id.imageProgress);
		// LOG_TAG = context.getPackageName().toString();
	}


	public Context getCon(){

		return context;
	}

	/**
	 * @param message
	 *            Pass message to show user
	 * @return It will return long toast message whatever you pass in your
	 *         application
	 */
	public void Toast(String message) {
		final String onTimeMsg = message;
		((Activity) context).runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(context, onTimeMsg, Toast.LENGTH_SHORT).show();
			}
		});
	}

	public char getChar() {
		char randomChar = aToz[new Random().nextInt(aToz.length)];
		return randomChar;
	}

	public String getWord() {
		String word = "";
		for (int i = 0; i < 4; i++) {
			word += getChar();
		}
		return word;
	}

	public void startProgress() {
		try {
			// for making transparent background
			dialog.show();
			dialog.getWindow().setBackgroundDrawable(
					new ColorDrawable(android.graphics.Color.TRANSPARENT));
			dialog.setCancelable(false);
			dialog.setCanceledOnTouchOutside(false);
			dialog.setContentView(R.layout.dialog_progress);

			// for heart animation
//			imageProgress.setVisibility(View.VISIBLE);
//			imageProgress.startAnimation(animation);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void dismissProgress() {
		try {
//			imageProgress.clearAnimation();
//			imageProgress.setVisibility(View.GONE);
//			animation.cancel();

			// dialog cancel
			dialog.cancel();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
//	public void showProgress() {
//		dialog.setTitle("Please wait");
//		dialog.setMessage("Loading...");
//		dialog.setCancelable(false);
//		dialog.setCanceledOnTouchOutside(false);
//		dialog.show();
//	}
//
//	public void cancelProgress() {
//		dialog.cancel();
//	}


	public void showAlertMessage(String buttonName, String title, String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.setPositiveButton(buttonName,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).show();
	}

	public void showAlertMessage(String buttonName, int title, int message) {
		final String strTitle = context.getResources().getString(title);
		final String strMsg = context.getResources().getString(message);
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(strTitle);
		builder.setMessage(strMsg);
		builder.setPositiveButton(buttonName,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).show();
	}

	public void showAlertMessage(int btnName, int title, int message) {
		final String strMessage = context.getResources().getString(message);
		final String strTitle = context.getResources().getString(title);
		final String strBtnName = context.getResources().getString(btnName);
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(strMessage);
		builder.setTitle(strTitle);
		builder.setPositiveButton(strBtnName,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).show();
	}


	/**
	 * @param buttonName
	 *            set yes no or cancel
	 * @param message
	 *            message in alert box
	 * 
	 * @return AlertBox to use as user message
	 */
	public void showAlertMessage(String buttonName, String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context,R.style.dialogTheme);
		builder.setMessage(message);
		builder.setPositiveButton(buttonName,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).show();
	}

	/**
	 * @param emailAddress
	 *            Passyour emiailaddress string to check
	 * @return It will return true if email address is valid or false in case
	 *         email is not valid
	 */
	public boolean isEmailValid(String emailAddress) {
		String regExpn = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
				+ "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
				+ "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
				+ "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
				+ "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
				+ "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

		Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(emailAddress);

		if (matcher.matches())
			return true;
		else
			return false;
	}
	
	/**
	 * phone number validation
	 */
	public boolean isPhoneValid(String phone){
		if(phone.length() < 10)
        {
           return false;
        }
        else
        {
            return true;
        }
	}

	/**
	 * @return It will check your Internet connection.True if any net connected.
	 */
	public boolean isNetConnected() {
		ConnectivityManager connectivity = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null)
				for (int i = 0; i < info.length; i++)
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}

		}
		return false;
	}

	/**
	 * @return This method returns system current time , change format as per
	 *         your reuirement , Locale is also set as english so take care of
	 *         that also.It is HH:mm 24 hour format
	 */
	public String getCurrentTime() {
		return new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH)
				.format(new Date());
	}

	public String getCurrentTimePath() {
		return new SimpleDateFormat("HHmmss", Locale.ENGLISH)
				.format(new Date());
	}

	/**
	 * @return This method returns system current date in dd/MM/yyyy format
	 */
	public String getCurrentDate() {
		return new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH)
				.format(new Date());
	}

	/**
	 * set string Preference
	 */
	public void setPreference(String key, String value) {
		editor.putString(key, value);
		editor.commit();
	}

	/**
	 * get string preferences
	 */
	public String getPreference(String key) {
		return preferences.getString(key, "");
	}

	public void setIntPreference(String key, int value) {
		editor.putInt(key, value);
		editor.commit();
	}

	/**
	 * get string preferences
	 */
	public int getIntPreference(String key) {
		return preferences.getInt(key, 0);
	}

	/**
	 * Use to set boolean preference
	 */
	public void setBooleanPrefrences(String key, boolean value) {
		editor.putBoolean(key, value);
		editor.commit();
	}

	/**
	 * use to get boolean preference
	 */
	public boolean getBooleanPrefrences(String key) {
		return preferences.getBoolean(key, false);
	}

	/**
	 * @return This method kills all processes of application which is running
	 *         in back ground , we can also use
	 *         android.os.Process.killProcess(pid) to exit from application
	 */
	public void killProcess(Context context) {
		int pid = 0;
		ActivityManager am = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		List<ActivityManager.RunningAppProcessInfo> pids = am
				.getRunningAppProcesses();
		for (int i = 0; i < pids.size(); i++) {
			ActivityManager.RunningAppProcessInfo info = pids.get(i);
			if (info.processName.equalsIgnoreCase("com.dbb.activity")) {
				pid = info.pid;
			}
		}
		android.os.Process.killProcess(pid);
	}

	/**
	 * @return true is tablet resolution is there
	 */
	public boolean isTablet() {
		boolean xlarge = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4);
		boolean large = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
		return (xlarge || large);
	}

	/**
	 * @return IMEI NO
	 */
	public String getImeiNo() {
		TelephonyManager teleManager = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		return teleManager.getDeviceId();
	}

	public boolean isUsingWiFi() {
		ConnectivityManager connectivity = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo wifiInfo = connectivity
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		if (wifiInfo.getState() == NetworkInfo.State.CONNECTED
				|| wifiInfo.getState() == NetworkInfo.State.CONNECTING) {
			return true;
		}

		return false;
	}

	public static void CopyStream(InputStream is, OutputStream os) {
		final int buffer_size = 1024;
		try {
			byte[] bytes = new byte[buffer_size];
			for (;;) {
				int count = is.read(bytes, 0, buffer_size);
				if (count == -1)
					break;
				os.write(bytes, 0, count);
			}
		} catch (Exception ex) {
		}
	}

	public static String streamToString(InputStream is) throws IOException {
		String str = "";

		if (is != null) {
			StringBuilder sb = new StringBuilder();
			String line;

			try {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(is));

				while ((line = reader.readLine()) != null) {
					sb.append(line);
				}

				reader.close();
			} finally {
				is.close();
			}

			str = sb.toString();
		}

		return str;
	}
	public String addressLine(double lat, double longi) {
		String address = "";
		try {
			String subAddress = "";
			Geocoder geocoder;
			List<Address> addresses;
			geocoder = new Geocoder(context, Locale.getDefault());

			addresses = geocoder.getFromLocation(lat, longi, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

			subAddress = addresses.get(0).getAddressLine(0);
			if (subAddress != null && !subAddress.equals("") && !subAddress.equals("null")) {
				address += subAddress;
			}

			subAddress = addresses.get(0).getSubLocality();
			if (subAddress != null && !subAddress.equals("") && !subAddress.equals("null")) {
				address += ", " + subAddress;
			}

			subAddress = addresses.get(0).getLocality();
			if (subAddress != null && !subAddress.equals("") && !subAddress.equals("null")) {
				address += ", " + subAddress;
			}

			subAddress = addresses.get(0).getAdminArea();
			if (subAddress != null && !subAddress.equals("") && !subAddress.equals("null")) {
				address += ", " + subAddress;
			}

			subAddress = addresses.get(0).getCountryName();
			if (subAddress != null && !subAddress.equals("") && !subAddress.equals("null")) {
				address += ", " + subAddress;
			}

			subAddress = addresses.get(0).getPostalCode();
			if (subAddress != null && !subAddress.equals("") && !subAddress.equals("null")) {
				address += ", " + subAddress;
			}
		} catch (Exception e) {

		}
		return address;
	}

	/**
	 * @param message Pass message to show user
	 * @return It will return long toast message whatever you pass in your
	 * application
	 */
	public void Toast(String message, CoordinatorLayout coordinatorLayout) {
		try {
			if (isAboveKitkat()&& coordinatorLayout != null) {
				Snackbar snackbar = Snackbar
						.make(coordinatorLayout, message, Snackbar.LENGTH_LONG);
				snackbar.show();
			} else {
				final String onTimeMsg = message;
				((Activity) context).runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(context, onTimeMsg, Toast.LENGTH_SHORT).show();
					}
				});
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void Toast(int id, CoordinatorLayout coordinatorLayout) {
		try {
			final String str = context.getResources().getString(id);
			if (isAboveKitkat()&& coordinatorLayout != null) {
				Snackbar snackbar = Snackbar
						.make(coordinatorLayout, str, Snackbar.LENGTH_LONG);
				snackbar.show();
			} else {
//				final String onTimeMsg = message;
				((Activity) context).runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(context, str, Toast.LENGTH_SHORT).show();
					}
				});
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public String getStringRes (int id){
		final String str = context.getResources().getString(id);
		return str;
	}
	public boolean isAboveMarsh() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			return true;
		} else {
			return false;
		}
	}
	public boolean isAboveKitkat() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			return true;
		} else {
			return false;
		}
	}
	public String timeZoneId() {
		return TimeZone.getDefault().getID();
	}

	SecretKey secret;
	public  SecretKey generateKey(String string)
			throws NoSuchAlgorithmException, InvalidKeySpecException
	{

		return secret = new SecretKeySpec(string.getBytes(), "AES");
	}

	public static byte[] encryptMsg(String message, SecretKey secret)
			throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidParameterSpecException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException
	{
   /* Encrypt the message. */
		Cipher cipher = null;
		cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, secret);
		byte[] cipherText = cipher.doFinal(message.getBytes("UTF-8"));
		return cipherText;
	}

	public static String decryptMsg(byte[] cipherText, SecretKey secret)
			throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidParameterSpecException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, UnsupportedEncodingException
	{
    /* Decrypt the message, given derived encContentValues and initialization vector. */
		Cipher cipher = null;
		cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, secret);
		String decryptString = new String(cipher.doFinal(cipherText), "UTF-8");
		return decryptString;
	}

}
