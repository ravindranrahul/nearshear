package com.nearshear;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.nearshear.api.ApiUtils;
import com.nearshear.model.RegistrationDO;
import com.nearshear.model.RegistrationInfo;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtpActivity extends BaseActivity implements View.OnClickListener {
    private EditText edtFirst;
    private EditText edtSecond;
    private EditText edtThird;
    private EditText edtForth;

    private Button btnRetrieve;
    private TextView txtResend;

    private String strUserId = "";
    private String strOtp = "";
    private String strFirst = "";
    private String strSecond = "";
    private String strThird = "";
    private String strForth = "";

    private boolean isTimeFinished = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.color.white);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(this.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        setViews();
        setToolBar();

        new CountDownTimer(30000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                isTimeFinished = true;
                txtResend.setText("Please wait for : " + millisUntilFinished / 1000 + "s");
            }

            @Override
            public void onFinish() {
                txtResend.setText("Resend");
                txtResend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callResendOTP();
                    }
                });
            }
        }.start();


        edtFirst.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (edtFirst.getText().toString().length() == 1)     //size as per your requirement
                {
                    edtSecond.requestFocus();
                }
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });

        edtSecond.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (edtSecond.getText().toString().length() == 1)     //size as per your requirement
                {
                    edtThird.requestFocus();
                }
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });

        edtThird.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (edtThird.getText().toString().length() == 1)     //size as per your requirement
                {
                    edtForth.requestFocus();
                }
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });

    }

    private void setViews() {
        Bundle extras = getIntent().getExtras();
        String value;
        if (extras != null) {
            strUserId = extras.getString("id");
        }

        edtFirst = findViewById(R.id.edtFirst);
        edtSecond = findViewById(R.id.edtSecond);
        edtThird = findViewById(R.id.edtThird);
        edtForth = findViewById(R.id.edtForth);
        btnRetrieve = findViewById(R.id.btnRetrieve);
        txtResend=findViewById(R.id.txtResend);

        btnRetrieve.setOnClickListener(this);
        txtResend.setOnClickListener(this);

    }

    private void callVerifyOtp() {
        utils.startProgress();
        ApiUtils.getAPIService().requestVerifyOtp(strUserId, strOtp).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                utils.dismissProgress();
                try {
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();

                    if (success.equals("1")) {
                        ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                        listOfPostData = response.body().getPostdata();
                        RegistrationInfo registrationInfo = listOfPostData.get(0);
                        String id = registrationInfo.getStrKeyId();

                        Intent otp = new Intent(OtpActivity.this,  NewPasswordActivity.class);
                        Bundle mBundle = new Bundle();
                        mBundle.putString("id", id);
                        otp.putExtras(mBundle);
                        startActivity(otp);
                        finish();
                    } else if (success.equals("0")) {
                        utils.Toast(msg, coordinatorLayout);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                utils.Toast("Please check connection", null);
            }
        });
    }

    public void callResendOTP() {
        utils.startProgress();

        ApiUtils.getAPIService().requestResendOTP(strUserId).enqueue(new Callback<RegistrationDO>() {

            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                try {
                    utils.dismissProgress();
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();
                    switch (success) {
                        case "1": {
                            utils.Toast(msg);
                            break;
                        }
                        case "0": {
                            utils.Toast(msg);
                            break;
                        }
                        default:
                            utils.Toast(msg);
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                finish();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRetrieve:
                strOtp = "";
                strFirst = edtFirst.getText().toString();
                strSecond = edtSecond.getText().toString();
                strThird = edtThird.getText().toString();
                strForth = edtForth.getText().toString();

                if (strFirst.equals("")) {
                    utils.Toast("Enter valid otp");
                } else if (strSecond.equals("")) {
                    utils.Toast("Enter valid otp");
                } else if (strThird.equals("")) {
                    utils.Toast("Enter valid otp");
                } else if (strForth.equals("")) {
                    utils.Toast("Enter valid otp");
                } else {
                    strOtp = strFirst + strSecond + strThird + strForth;
                    if (utils.isNetConnected()) {
                        callVerifyOtp();
                    } else {
                        utils.Toast(R.string.check_net, coordinatorLayout);
                    }
                }
                break;
//            case R.id.txtResend:
//callResendOTP();
//                break;
        }
    }
}
