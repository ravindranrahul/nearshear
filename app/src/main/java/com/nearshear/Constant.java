package com.nearshear;


import com.google.android.gms.maps.model.LatLng;
import com.nearshear.model.SubCategoryDo;

import java.util.ArrayList;

public class Constant {
    public static double CURRENT_LAT =0 ;
    public static double CURRENT_LONG =0 ;
    public static LatLng CURRENT_LATLON =null ;
    public static String PREF_ID = "userId";
    public static String PREF_NAME = "name";
    public static String PREF_IMAGE= "img";
        public static String PREF_EMAIL = "email";
        public static String PREF_PHONE = "Phone";
    public static String PREF_LOGIN_TYPE= "logint";
    public static String PREF_DEVICE_TOKEN = "";
    public static String PREF_CUR_LAT="lat";
    public static String PREF_CUR_LON="lon";
    public static String PREF_CUR_LOCATION="location";
    public static String PREF_SOCIETY_NAME="sname";
    public static String PREF_AREA="area";
    public static String PREF_PIN="pin";
    public static String PREF_CITY="city";
    public static String PREF_FAV_COUNT="fcount";

    public static ArrayList<SubCategoryDo> allSubCatList ;

}
