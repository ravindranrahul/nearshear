package com.nearshear.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.zxing.client.result.VINParsedResult;
import com.nearshear.Constant;
import com.nearshear.MainActivity;
import com.nearshear.R;
import com.nearshear.adapter.GetAllServicesAdapter;
import com.nearshear.api.RequestAPI;
import com.nearshear.model.Datum;
import com.nearshear.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class GetAllServices extends Fragment implements View.OnClickListener{

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private LinearLayoutManager layoutManager;
    private Utils utils;
    private View rootView;

    private List<com.nearshear.model.GetAllServices> getAllServicesList;
    private List<Datum> datum;

    MenuItem item;
    MenuItem notification;
    private ImageView noDataImg;
    private TextView text1;
    private TextView text2;
    private FrameLayout flrecycler;



    public GetAllServices() {
        // Required empty public constructor
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        super.onCreateOptionsMenu(menu, inflater);

//        inflater.inflate(R.menu.menu_main,menu);
//        notification = menu.findItem(R.id.action_settings);
//        notification.setVisible(false);
//        notification.setEnabled(false);

        inflater.inflate(R.menu.menu_add,menu);
        item = menu.findItem(R.id.action_add);
        item.setVisible(true);

//        return item;

        super.onCreateOptionsMenu(menu, inflater);

//        return super.onCreateOptionsMenu(menu,inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_add :
            {
                Log.d("saurabh"," button clicked..");
                ((MainActivity) getActivity()).setBecomePartner("Become a Partner");
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

//        setHasOptionsMenu(true);

        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        rootView = inflater.inflate(R.layout.recycler_test, container,false);

        setView();

//        noDataImg.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d("become","image clicked");
//                ((MainActivity) getActivity()).setBecomePartner("Become a Partner");
//            }
//        });

        return rootView;
    }

    private void setView() {
        utils = new Utils(getActivity());

        noDataImg = (ImageView) rootView.findViewById(R.id.noDataImg);
//        noDataImg.setOnClickListener(this);

        RecyclerView.LayoutManager lLayout = new LinearLayoutManager(getContext());
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        Log.d("recycler123 ",""+recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(lLayout);

//        noDataImg = (ImageView) rootView.findViewById(R.id.noDataImg);
//        text1 = (TextView) rootView.findViewById(R.id.text1);
//        text2 = (TextView) rootView.findViewById(R.id.text2);
        flrecycler = rootView.findViewById(R.id.flrecycler);

        flrecycler.setOnClickListener(this);

//        noDataImg.setOnClickListener(this);

//        noDataImg.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d("become","image clicked");
//                ((MainActivity) getActivity().getApplicationContext()).setBecomePartner("Become a Partner");
//            }
//        });

        callReq();
    }

    private void callReq() {

        utils.startProgress();

        Log.d("callReq","method called");

        String id = utils.getPreference(Constant.PREF_ID);
        Log.d("rahul","user id  "+id);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        final RecyclerView rv = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(layoutManager);

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://nearshear.com/staging/backend/api/partners/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final RequestAPI requestAPI = retrofit.create(RequestAPI.class);

        Call<com.nearshear.model.GetAllServices> call = requestAPI.getAllServices(id);

        Log.d("rahul","after api call");
//        Log.d("rahul","user id"+ MediaType.parse("text/plain"));

//        String id = utils.getPreference(Constant.PREF_ID);
//        Log.d("rahul","user id  "+id);

        call.enqueue(new Callback<com.nearshear.model.GetAllServices>() {
            @Override
            public void onResponse(Call<com.nearshear.model.GetAllServices> call, Response<com.nearshear.model.GetAllServices> response) {
                if (!response.isSuccessful()){
//                    textViewResult.setText("code: "+response.code());
                    Log.d("isSuccefull error ",""+response.code());
                    return;
                }

                Log.d("rahul","after api call"+response.body());

                Log.d("rahul ",""+response.body());

                Log.d("rahulSuccess ",""+response.code());

                com.nearshear.model.GetAllServices getAllServices = response.body();

//                Log.d("rahul getAllServices ",""+getAllServices.getData());

                List<Datum> datum = getAllServices.getData();

                List<Object> imgDatum = getAllServices.getImages();

//                List list = getAllServices.getData();

//                Log.d("rahul list data ",""+list);

               List list = new ArrayList<>();


                Log.d("saurabh ",""+datum.size());

                if (datum.size() == 0){
                    Log.d("saurabh ","size is 0");
                    noDataImg.setVisibility(rootView.VISIBLE);
//                    text1.setVisibility(rootView.VISIBLE);
//                    text2.setVisibility(rootView.VISIBLE);
                    noDataImg.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Log.d("become","click");
                        }
                    });
//                    text1.setVisibility(rootView.getVisibility());
//                    text2.setVisibility(rootView.getVisibility());
                }

                for (int i = 0; i<datum.size(); i++){
                    Datum datum1 = new Datum(
                            datum.get(i).getPartnerId(),
                            datum.get(i).getName(),
                            datum.get(i).getPhone(),
                            datum.get(i).getServiceName(),
                            datum.get(i).getArea(),
                            datum.get(i).getPin(),
                            datum.get(i).getCity(),
                            datum.get(i).getAddress(),
                            datum.get(i).getOfferBonusTitle(),
                            datum.get(i).getCatName(),
                            datum.get(i).getSubCatName(),
                            datum.get(i).getRating(),
                            datum.get(i).getStatus(),
                            datum.get(i).getActive(),
                            datum.get(i).getCoverImage(),
                            datum.get(i).getSocietyName(),
                            datum.get(i).getCatId(),
                            datum.get(i).getSubCatId(),
                            datum.get(i).getServicePhone()
                    );
                    datum1.setName(datum.get(i).getName());
                    datum1.setDescription(datum.get(i).getDescription());
                    datum1.setImage(datum.get(i).getImage());
                    datum1.setRating(datum.get(i).getRating());
                    datum1.setStatus(datum.get(i).getStatus());
                    datum1.setActive(datum.get(i).getActive());
                    datum1.setCoverImage(datum.get(i).getCoverImage());
                    datum1.setSocietyName(datum.get(i).getSocietyName());
                    datum1.setCatId(datum.get(i).getCatId());
                    datum1.setSubCatId(datum.get(i).getSubCatId());
                    datum1.setServicePhone(datum.get(i).getServicePhone());

//                    Datum datum2 = new Datum(datum.get(i).getPartnerId());

                    Log.d("rahul","check parent id in loop"+datum.get(i).getPartnerId());

                    list.add(datum1);
                }

                adapter = new GetAllServicesAdapter(list,getContext());
                rv.setAdapter(adapter);

                Log.d("rahul adapter",""+rv);
                utils.dismissProgress();
            }

            @Override
            public void onFailure(Call<com.nearshear.model.GetAllServices> call, Throwable t) {
                Log.d("error in request",t.getMessage());
                noDataImg.setVisibility(rootView.VISIBLE);
//                text1.setVisibility(rootView.getVisibility());
//                text2.setVisibility(rootView.getVisibility());
//                text1.setVisibility(rootView.VISIBLE);
//                text2.setVisibility(rootView.VISIBLE);
                utils.dismissProgress();

                noDataImg.isClickable();
//                noDataImg.callOnClick();
            }
        });
    }


    @Override
    public void onClick(View v) {

        Log.d("become","page click");

//        switch (v.getId()){
//            case R.id.noDataImg:
//                Log.d("become","image clicked");
//                ((MainActivity) getActivity()).setBecomePartner("Become a Partner");
//                break;
//            case R.id.flrecycler:
//                Log.d("flrecyler","clicked");
//        }


//        noDataImg.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d("become","image clicked");
//                ((MainActivity) getActivity()).setBecomePartner("Become a Partner");
//            }
//        });
    }

    public void testing(View v){
        Log.d("test","click");
    }
}
