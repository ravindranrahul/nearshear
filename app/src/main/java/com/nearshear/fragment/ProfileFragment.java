package com.nearshear.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.nearshear.Constant;
import com.nearshear.MainActivity;
import com.nearshear.R;
import com.nearshear.SignUpActivity;
import com.nearshear.api.ApiUtils;
import com.nearshear.api.KeyAbstract;
import com.nearshear.model.RegistrationDO;
import com.nearshear.model.RegistrationInfo;
import com.nearshear.utils.Utils;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;

public class ProfileFragment extends Fragment implements View.OnClickListener {
    private View rootView;
    private Button btnLogout;
    private Utils utils;
    private String strUserImage;
    private String strName = "";
    private String strEmail = "";
    private String strPhone = "";

    private ImageView imgUser;
    private TextView txtEmail;
    private TextView txtPhone;
    private TextView txtEdit;
    private TextView txtName;
    private TextView txtRegister;

    private int GALlERY_REQUEST_CODE = 100;
    private int CAMERA_REQUEST_CODE = 200;

    private String strImage = "";
    private ImageView imgAddImage;
    private String loginType;

    private LinearLayout layAbout;
    private LinearLayout layHelpCenter;
    private LinearLayout layInvite;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        setViews();

        return rootView;
    }

    private void setViews() {
        utils = new Utils(getActivity());

        loginType = utils.getPreference(Constant.PREF_LOGIN_TYPE);

        btnLogout = rootView.findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(this);

        strName = utils.getPreference(Constant.PREF_NAME);
        strEmail = utils.getPreference(Constant.PREF_EMAIL);
        strPhone = utils.getPreference(Constant.PREF_PHONE);

        txtName = rootView.findViewById(R.id.txtName);
        txtEmail = rootView.findViewById(R.id.txtEmail);
        txtPhone = rootView.findViewById(R.id.txtPhone);
        txtEdit = rootView.findViewById(R.id.txtEdit);
        imgUser = rootView.findViewById(R.id.imgUser);
        txtRegister = rootView.findViewById(R.id.txtRegister);
        imgAddImage = rootView.findViewById(R.id.imgAddImage);
        layAbout = rootView.findViewById(R.id.layAbout);
        layHelpCenter = rootView.findViewById(R.id.layHelpCenter);
        layInvite = rootView.findViewById(R.id.layInvite);
        txtRegister.setOnClickListener(this);
        imgUser.setOnClickListener(this);
        txtEdit.setOnClickListener(this);
        imgAddImage.setOnClickListener(this);
        layAbout.setOnClickListener(this);
        layHelpCenter.setOnClickListener(this);
        layInvite.setOnClickListener(this);

        txtName.setText(strName);
        txtEmail.setText(strEmail);
        txtPhone.setText(strPhone);

        try {
            strUserImage = utils.getPreference(Constant.PREF_IMAGE);

//            Glide.with(this).load("https://plus.google.com/_/focus/photos/private/AIbEiAIAAABECNLB64aK3pvTjQEiC3ZjYXJkX3Bob3RvKig4NjcxNTlhYzE2YTA1YTI1YmVlNGMyZDlhMjRiNjE4ZjA0Yzg0YzFjMAEf6bwlvtrDPucU2pS3bihsy_PrVA")
//                    .thumbnail(0.5f)
//                    .crossFade()
//                    .diskCacheStrategy(DiskCacheStrategy.ALL)
//                    .into(imgUser);


            Glide.with(this).load(strUserImage).placeholder(R.drawable.user_img).into(imgUser);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (loginType.equals("isApp")) {
            imgAddImage.setVisibility(View.VISIBLE);
        }
    }

    private void captureImg() {
        final CharSequence[] options = {"Camera", "Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Camera")) {
                    Intent m_intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File file = new File(Environment.getExternalStorageDirectory(), "MyPhoto.jpg");
                    Uri uri = FileProvider.getUriForFile(getContext(), getApplicationContext().getPackageName() + ".provider", file);
                    m_intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, uri);
                    startActivityForResult(m_intent, CAMERA_REQUEST_CODE);
                } else if (options[item].equals("Gallery")) {
                    Intent intent = new Intent(getActivity(), AlbumSelectActivity.class);
                    intent.putExtra(Constants.INTENT_EXTRA_LIMIT, 1);
                    startActivityForResult(intent, GALlERY_REQUEST_CODE);
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                //File object of camera image
                File file = new File(Environment.getExternalStorageDirectory(), "MyPhoto.jpg");
                //Uri of camera image
//                    Uri uri = FileProvider.getUriForFile(this, this.getApplicationContext().getPackageName() + ".provider", file);
                Uri uri = Uri.fromFile(file);

                //crop image
                CropImage.activity(uri).setAspectRatio(4, 4)
                        .start(getActivity());
            }
        }

        if (requestCode == GALlERY_REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            ArrayList<Image> images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);
            Uri uri = Uri.parse("file://" + images.get(0).path);

            //crop image
            CropImage.activity(uri).setAspectRatio(4, 4)
                    .start(getActivity());
        }

    }

    public void setProfilePic(Uri resultUri) {
        if (utils.isNetConnected()) {
//            Glide.with(this).load(resultUri)
//                    .thumbnail(0.5f)
//                    .crossFade()
//                    .diskCacheStrategy(DiskCacheStrategy.ALL)
//                    .into(imgUser);

            Glide.with(this).load(resultUri).placeholder(R.drawable.user_img).into(imgUser);
            callUpdate();
        } else {
            utils.Toast(R.string.check_net, null);
        }
    }

    private void callUpdate() {
        utils.startProgress();

        MultipartBody.Part image;
        image = null;
        try {
            MainActivity.strImage = MainActivity.strImage.substring(7);

//            strImage=strImage.substring(7);
            File file1 = new File(MainActivity.strImage);
            image = MultipartBody.Part.createFormData(KeyAbstract.KEY_IMAGE, file1.getName(), RequestBody.create(MediaType.parse("image/*"), file1));
        } catch (Exception e) {
            e.printStackTrace();
        }

        String id = utils.getPreference(Constant.PREF_ID);
        RequestBody bodyUserId = RequestBody.create(MediaType.parse("text/plain"), id);

        ApiUtils.getAPIService().requestUpdateProfileImageAPI(bodyUserId, image).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                utils.dismissProgress();
                try {
                    Log.d("::", String.valueOf(response));
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();
                    if (success.equals("1")) {
                        ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                        listOfPostData = response.body().getPostdata();
                        RegistrationInfo registrationInfo = listOfPostData.get(0);

                        String imageLink = registrationInfo.getImage();

                        utils.setPreference(Constant.PREF_IMAGE, imageLink);

                        MainActivity.strImage = null;

                        utils.Toast(msg, null);
                    } else {
                        utils.Toast(msg, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                utils.Toast("Network error", null);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogout:
                String strLoginType = utils.getPreference(Constant.PREF_LOGIN_TYPE);
                if (strLoginType.equals("isGplus")) {
                    MainActivity.mGoogleSignInClient.signOut()
                            .addOnCompleteListener(getActivity(), new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    utils.setPreference(Constant.PREF_ID, "");
                                    utils.setPreference(Constant.PREF_DEVICE_TOKEN, "");
                                    utils.setPreference(Constant.PREF_NAME, "");
                                    utils.setPreference(Constant.PREF_EMAIL, "");
                                    utils.setPreference(Constant.PREF_PHONE, "");
                                    utils.setPreference(Constant.PREF_CUR_LOCATION, "");
                                    utils.setPreference(Constant.PREF_CUR_LAT, "");
                                    utils.setPreference(Constant.PREF_CUR_LON, "");
                                }
                            });

                } else if (strLoginType.equals("isApp")) {
                    utils.setPreference(Constant.PREF_ID, "");
                    utils.setPreference(Constant.PREF_DEVICE_TOKEN, "");
                    utils.setPreference(Constant.PREF_NAME, "");
                    utils.setPreference(Constant.PREF_EMAIL, "");
                    utils.setPreference(Constant.PREF_PHONE, "");
                    utils.setPreference(Constant.PREF_CUR_LOCATION, "");
                    utils.setPreference(Constant.PREF_CUR_LAT, "");
                    utils.setPreference(Constant.PREF_CUR_LON, "");
                } else if (strLoginType.equals("isFb")) {
                    utils.setPreference(Constant.PREF_ID, "");
                    utils.setPreference(Constant.PREF_DEVICE_TOKEN, "");
                    utils.setPreference(Constant.PREF_NAME, "");
                    utils.setPreference(Constant.PREF_EMAIL, "");
                    utils.setPreference(Constant.PREF_PHONE, "");
                    utils.setPreference(Constant.PREF_CUR_LOCATION, "");
                    utils.setPreference(Constant.PREF_CUR_LAT, "");
                    utils.setPreference(Constant.PREF_CUR_LON, "");

                    try {
                        LoginManager.getInstance().logOut();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                Intent login = new Intent(getContext(), SignUpActivity.class);
                startActivity(login);
                ((MainActivity) getActivity()).exitApp();

                break;

            case R.id.txtRegister:
                ((MainActivity) getActivity()).setAdService("Manage Services");
                break;

            case R.id.imgUser:
                String loginType = utils.getPreference(Constant.PREF_LOGIN_TYPE);
                if (loginType.equals("isApp")) {
                    captureImg();
                } else {
                    if(loginType.equals("isGplus")){
                        utils.Toast("You cannot edit your profile because you have logged in using Gmail");
                    }else if(loginType.equals("isFb")){
                        utils.Toast("You cannot edit your profile because you have logged in using Facebook");
                    }
                }
                break;

            case R.id.txtEdit:
                ((MainActivity) getActivity()).setEditProfile("Edit Profile Details");
                break;

            case R.id.imgAddImage:
                captureImg();
                break;
            case R.id.layInvite:
                String shareBody = "https://play.google.com/store/apps/details?id=com.nearshear";
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Near Shear");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share using"));
                break;
            case R.id.layAbout:
                ((MainActivity) getActivity()).setAboutUs("About Us");
                break;
            case R.id.layHelpCenter:
                ((MainActivity) getActivity()).setHelpCenter("Help Center");
                break;

        }
    }


}
