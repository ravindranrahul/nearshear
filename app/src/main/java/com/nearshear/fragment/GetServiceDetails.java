package com.nearshear.fragment;


import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.nearshear.Constant;
import com.nearshear.R;
import com.nearshear.adapter.FeaturedServicesListAdapter;
import com.nearshear.adapter.PhoneListAdapter;
import com.nearshear.adapter.ReviewsListAdapter;
import com.nearshear.adapter.ViewPagerAdapter;
import com.nearshear.api.ApiUtils;
import com.nearshear.api.RequestAPI;
import com.nearshear.model.PhoneDo;
import com.nearshear.model.RegistrationDO;
import com.nearshear.model.ReviewDo;
import com.nearshear.model.SellerDo;
import com.nearshear.model.ServiceDetailsDo;
import com.nearshear.model.SliderDo;
import com.nearshear.utils.Utils;

import java.util.ArrayList;

import io.supercharge.shimmerlayout.ShimmerLayout;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class GetServiceDetails extends Fragment implements View.OnClickListener {

    private View rootView;
    private Utils utils;
    private ViewPagerAdapter pagerAdapter;
    private ViewPager viewPager;
    private CircleIndicator indicator;
    private RecyclerView rViewReviewList;
    private LinearLayout layReviews;
    private TextView txtServiceTitle;
    private TextView txtPhone;
    private TextView txtAddress;
    private TextView txtDescription;
    private TextView txtSellerName;
    private SimpleRatingBar ratingBar;
    private ImageView imgFavourite;
    private TextView txtRateCount;
    private TextView txtRateCountReview;
    private LinearLayout layLoadMore;

    private LinearLayout layMyRating;
    private LinearLayout layRatingOne;
    private LinearLayout layRatingTwo;
    private LinearLayout layRatingThree;
    private LinearLayout layRatingFour;
    private LinearLayout layRatingFive;

    private SimpleRatingBar ratingBarOne;
    private SimpleRatingBar ratingBarTwo;
    private SimpleRatingBar ratingBarThree;
    private SimpleRatingBar ratingBarFour;
    private SimpleRatingBar ratingBarFive;
//    private SimpleRatingBar ratingBarReview;

    private TextView txtWriteReview;
    private TextView txtRateOne;
    private TextView txtRateTwo;
    private TextView txtRateThree;
    private TextView txtRateFour;
    private TextView txtRateFive;
    private TextView txtReview;
    private Dialog dialogReview;

    private String strServiceId;
    private double selectedRating;
    private ServiceDetailsDo model;
    private TextView txtTotalRating;

    ArrayList<ReviewDo> listOfReview;
    private int reviewCounter = 0;
    private int total_review = 0;
    private ReviewsListAdapter reviewsListAdapter;
    //Other services
    private RecyclerView rViewListFeaturedServices;
    private LinearLayoutManager layoutManager;
    private ShimmerLayout shimmerText;
    private FeaturedServicesListAdapter featuredServicesListAdapter;
    private LinearLayout layOtherServices;

    private PhoneListAdapter phoneListAdapter;
    private RecyclerView rViewPhone;
    private TextView txtOfferBonusTitle;
    private LinearLayout layMain;
    int colorWhite = Color.parseColor("#FFFFFF");

    public GetServiceDetails() {
        // Required empty public constructor
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_get_service_details, container, false);
        setViews();

        Bundle extras = getArguments();

        Log.d("rahul","get extras value "+extras);
        Log.d("rahul","get partner id "+extras.getString("id"));

        if (utils.isNetConnected()) {
            if (extras != null) {
                strServiceId = extras.getString("id");
//                strServiceId = "48";
                callServiceDetails();
//                callServiceDetailsCopy();
            }
        } else {
            layMain.setVisibility(View.GONE);
            utils.Toast(R.string.check_net, null);
        }
        return rootView;
    }

    private void callServiceDetailsCopy() {
        String id = utils.getPreference(Constant.PREF_ID);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://nearshear.com/staging/backend/api/partners/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final RequestAPI requestAPI = retrofit.create(RequestAPI.class);

        Call<RegistrationDO> call = requestAPI.getDetailsOfService();

        call.enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                Log.d("Response",""+response.raw().request().url());
                Log.d("Response code:", "" + response.code());

                try {
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();

                    if (success.equals("1")) {
                        ArrayList<ServiceDetailsDo> serviceDetailsData = new ArrayList<>();
                        serviceDetailsData = response.body().getService_details_data();
                        model = serviceDetailsData.get(0);

                        ArrayList<SliderDo> listOfSliderData = new ArrayList<>();
                        listOfSliderData = response.body().getSlider_data();

                        pagerAdapter = new ViewPagerAdapter(getActivity(), listOfSliderData);
                        viewPager.setAdapter(pagerAdapter);
                        indicator.setViewPager(viewPager);

                        ArrayList<PhoneDo> listOfPhoneData = response.body().getPhone_data();
                        phoneListAdapter = new PhoneListAdapter(listOfPhoneData, "details");
                        rViewPhone.setAdapter(phoneListAdapter);

                        txtSellerName.setText(model.getSeller_name());
                        txtDescription.setText(model.getDescription());
                        txtOfferBonusTitle.setText(model.getOffer_bonus_title());
                        txtAddress.setText(model.getAddress());
                        txtServiceTitle.setText(model.getOrg_name());
                        try {
                            float rating = Float.parseFloat(model.getRate());
                            if (rating == 0) {
                                txtTotalRating.setBackgroundResource(R.drawable.rate_zero);
                                txtTotalRating.setText(model.getRating_text());
                            }
                            if (rating >= 1 && rating < 2) {
                                txtTotalRating.setBackgroundResource(R.drawable.rate_one_two);
                                txtTotalRating.setText(model.getRate());
                            }
                            if (rating >= 2 && rating < 3) {
                                txtTotalRating.setBackgroundResource(R.drawable.rate_one_two);
                                txtTotalRating.setText(model.getRate());
                            }
                            if (rating >= 3 && rating < 4) {
                                txtTotalRating.setBackgroundResource(R.drawable.rate_three);
                                txtTotalRating.setText(model.getRate());
                            }
                            if (rating >= 4 && rating <= 5) {
                                txtTotalRating.setBackgroundResource(R.drawable.rate_four_five);
                                txtTotalRating.setText(model.getRate());
                            }
//                            if (rating <= 5) {
//                                txtTotalRating.setBackgroundResource(R.color.rate_five);
//                                txtTotalRating.setText(model.getRate());
//                            }
//                            txtTotalRating.setText(model.getRate());
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }


                        if (model.getFavourite_flag().equals("1")) {
                            imgFavourite.setImageResource(R.mipmap.favourited);
                        } else {
                            imgFavourite.setImageResource(R.mipmap.favourite);
                        }

                        try {
                            ratingBar.setRating(Float.parseFloat(model.getRate()));
//                            ratingBarReview.setRating(Float.parseFloat(model.getRate()));
                            txtRateCount.setText("(" + model.getRate_count() + ")");

                            if(model.getRate_count().equals("0") || model.getRate_count().equals("")){
                                txtRateCountReview.setVisibility(View.GONE);
                            }else{
                                txtRateCountReview.setVisibility(View.VISIBLE);
                                txtRateCountReview.setText("(" + model.getRate_count() + ")");
                            }
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }

                        listOfReview = new ArrayList<>();
                        reviewCounter = 0;

                        //other services
                        ArrayList<SellerDo> listOfSellerData = new ArrayList<>();
                        listOfSellerData = response.body().getSeller_data();
                        if(listOfSellerData.size()>0){
                            layOtherServices.setVisibility(View.VISIBLE);
                            featuredServicesListAdapter = new FeaturedServicesListAdapter(listOfSellerData);
                            rViewListFeaturedServices.setAdapter(featuredServicesListAdapter);
                        }else{
                            layOtherServices.setVisibility(View.GONE);
                        }


//                        callReview();
//                        callOtherServiceList();
                    } else if (success.equals("0")) {
                        utils.Toast(msg, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                Log.d("error in request",t.getMessage());
            }
        });
    }

    private void setViews() {
        utils = new Utils(getActivity());
        listOfReview = new ArrayList<>();

        layMain = rootView.findViewById(R.id.layMain);
        viewPager = rootView.findViewById(R.id.viewPager);
        indicator = rootView.findViewById(R.id.indicator);
        ratingBar = rootView.findViewById(R.id.ratingBar);
//        ratingBarReview = rootView.findViewById(R.id.ratingBarReview);
        txtAddress = rootView.findViewById(R.id.txtAddress);
        txtDescription = rootView.findViewById(R.id.txtDescription);
        txtSellerName = rootView.findViewById(R.id.txtSellerName);
//        txtPhone = rootView.findViewById(R.id.txtPhone);
        txtServiceTitle = rootView.findViewById(R.id.txtServiceTitle);
        txtReview = rootView.findViewById(R.id.txtReview);
        layReviews = rootView.findViewById(R.id.layReviews);
        txtRateCount = rootView.findViewById(R.id.txtRateCount);
        txtRateCountReview = rootView.findViewById(R.id.txtRateCountReview);
        layLoadMore = rootView.findViewById(R.id.layLoadMore);
        layLoadMore.setOnClickListener(this);
        layOtherServices = rootView.findViewById(R.id.layOtherServices);
        txtTotalRating = rootView.findViewById(R.id.txtRating);


        RecyclerView.LayoutManager lLayout = new LinearLayoutManager(getContext());
        rViewReviewList = rootView.findViewById(R.id.rViewReviewList);
        rViewReviewList.setHasFixedSize(true);
        rViewReviewList.setLayoutManager(lLayout);

        RecyclerView.LayoutManager lLayout1 = new LinearLayoutManager(getContext());
        rViewPhone = rootView.findViewById(R.id.rViewPhoneList);
        rViewPhone.setHasFixedSize(true);
        rViewPhone.setLayoutManager(lLayout1);

        //rating
        layMyRating = rootView.findViewById(R.id.layMyRating);
        layRatingOne = rootView.findViewById(R.id.layRatingOne);
        layRatingTwo = rootView.findViewById(R.id.layRatingTwo);
        layRatingThree = rootView.findViewById(R.id.layRatingThree);
        layRatingFour = rootView.findViewById(R.id.layRatingFour);
        layRatingFive = rootView.findViewById(R.id.layRatingFive);

        ratingBarOne = rootView.findViewById(R.id.ratingBarOne);
        ratingBarTwo = rootView.findViewById(R.id.ratingBarTwo);
        ratingBarThree = rootView.findViewById(R.id.ratingBarThree);
        ratingBarFour = rootView.findViewById(R.id.ratingBarFour);
        ratingBarFive = rootView.findViewById(R.id.ratingBarFive);

        txtWriteReview = rootView.findViewById(R.id.txtWriteReview);
        txtRateOne = rootView.findViewById(R.id.txtRateOne);
        txtRateTwo = rootView.findViewById(R.id.txtRateTwo);
        txtRateThree = rootView.findViewById(R.id.txtRateThree);
        txtRateFour = rootView.findViewById(R.id.txtRateFour);
        txtRateFive = rootView.findViewById(R.id.txtRateFive);
        txtWriteReview.setOnClickListener(this);
        imgFavourite = rootView.findViewById(R.id.imgFavourite);
        imgFavourite.setOnClickListener(this);

        shimmerText = rootView.findViewById(R.id.shimmer_text);

        txtOfferBonusTitle = rootView.findViewById(R.id.txtOfferBonusTitle);

//        ServicesListAdapter.noteList


        layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rViewListFeaturedServices = rootView.findViewById(R.id.rViewListFeaturedServices);
        rViewListFeaturedServices.setHasFixedSize(true);
        rViewListFeaturedServices.setLayoutManager(layoutManager);
//        featuredServicesListAdapter = new FeaturedServicesListAdapter(ServicesListAdapter.noteList);
//        rViewListFeaturedServices.setAdapter(featuredServicesListAdapter);


    }

    private void callServiceDetails() {
        String id = utils.getPreference(Constant.PREF_ID);
        Log.d("rahul","inside callServiceDetails "+id+" "+strServiceId);
        utils.startProgress();
//        final ShimmerLayout shimmerText = (ShimmerLayout) rootView.findViewById(R.id.shimmer_text);
//        shimmerText.startShimmerAnimation();
        Log.d("rahul","get service id"+strServiceId);
        ApiUtils.getAPIService().requestServiceDetails(id, strServiceId).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                utils.dismissProgress();
//                shimmerText.stopShimmerAnimation();
//                shimmerText.setVisibility(View.GONE);
                try {
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();

                    if (success.equals("1")) {
                        ArrayList<ServiceDetailsDo> serviceDetailsData = new ArrayList<>();
                        serviceDetailsData = response.body().getService_details_data();
                        model = serviceDetailsData.get(0);

                        ArrayList<SliderDo> listOfSliderData = new ArrayList<>();
                        listOfSliderData = response.body().getSlider_data();

                        pagerAdapter = new ViewPagerAdapter(getActivity(), listOfSliderData);
                        viewPager.setAdapter(pagerAdapter);
                        indicator.setViewPager(viewPager);

                        ArrayList<PhoneDo> listOfPhoneData = response.body().getPhone_data();
                        phoneListAdapter = new PhoneListAdapter(listOfPhoneData, "details");
                        rViewPhone.setAdapter(phoneListAdapter);

                        txtSellerName.setText(model.getSeller_name());
                        txtDescription.setText(model.getDescription());
                        txtOfferBonusTitle.setText(model.getOffer_bonus_title());
                        txtAddress.setText(model.getAddress());
                        txtServiceTitle.setText(model.getOrg_name());
                        try {
                            float rating = Float.parseFloat(model.getRate());
                            if (rating == 0) {
                                txtTotalRating.setBackgroundResource(R.drawable.rate_zero);
                                txtTotalRating.setText(model.getRating_text());
                            }
                            if (rating >= 1 && rating < 2) {
                                txtTotalRating.setBackgroundResource(R.drawable.rate_one_two);
                                txtTotalRating.setText(model.getRate());
                            }
                            if (rating >= 2 && rating < 3) {
                                txtTotalRating.setBackgroundResource(R.drawable.rate_one_two);
                                txtTotalRating.setText(model.getRate());
                            }
                            if (rating >= 3 && rating < 4) {
                                txtTotalRating.setBackgroundResource(R.drawable.rate_three);
                                txtTotalRating.setText(model.getRate());
                            }
                            if (rating >= 4 && rating <= 5) {
                                txtTotalRating.setBackgroundResource(R.drawable.rate_four_five);
                                txtTotalRating.setText(model.getRate());
                            }
//                            if (rating <= 5) {
//                                txtTotalRating.setBackgroundResource(R.color.rate_five);
//                                txtTotalRating.setText(model.getRate());
//                            }
//                            txtTotalRating.setText(model.getRate());
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }


                        if (model.getFavourite_flag().equals("1")) {
                            imgFavourite.setImageResource(R.mipmap.favourited);
                        } else {
                            imgFavourite.setImageResource(R.mipmap.favourite);
                        }

                        try {
                            ratingBar.setRating(Float.parseFloat(model.getRate()));
//                            ratingBarReview.setRating(Float.parseFloat(model.getRate()));
                            txtRateCount.setText("(" + model.getRate_count() + ")");

                            if(model.getRate_count().equals("0") || model.getRate_count().equals("")){
                                txtRateCountReview.setVisibility(View.GONE);
                            }else{
                                txtRateCountReview.setVisibility(View.VISIBLE);
                                txtRateCountReview.setText("(" + model.getRate_count() + ")");
                            }
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }

                        listOfReview = new ArrayList<>();
                        reviewCounter = 0;

                        //other services
                        ArrayList<SellerDo> listOfSellerData = new ArrayList<>();
                        listOfSellerData = response.body().getSeller_data();
                        if(listOfSellerData.size()>0){
                            layOtherServices.setVisibility(View.VISIBLE);
                            featuredServicesListAdapter = new FeaturedServicesListAdapter(listOfSellerData);
                            rViewListFeaturedServices.setAdapter(featuredServicesListAdapter);
                        }else{
                            layOtherServices.setVisibility(View.GONE);
                        }


//                        callReview();
//                        callOtherServiceList();
                    } else if (success.equals("0")) {
                        utils.Toast(msg, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                utils.Toast("Please check connection", null);
            }
        });
    }

    @Override
    public void onClick(View v) {

    }
}
