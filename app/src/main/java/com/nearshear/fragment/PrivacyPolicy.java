package com.nearshear.fragment;


import android.annotation.TargetApi;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.nearshear.Constant;
import com.nearshear.R;
import com.nearshear.api.ApiUtils;
import com.nearshear.utils.Utils;

public class PrivacyPolicy extends Fragment {
    private View rootView;
    private WebView mWebview;
    private Utils utils;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_privacy_policy, container, false);
        setViews();


//        mWebview.setWebViewClient(new WebViewClient() {
//            @SuppressWarnings("deprecation")
//            @Override
//            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                Toast.makeText(getActivity(), description, Toast.LENGTH_SHORT).show();
//            }
//            @TargetApi(android.os.Build.VERSION_CODES.M)
//            @Override
//            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
//                // Redirect to deprecated method, so you can use it in all SDK versions
//                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
//            }
//        });


        return rootView;
    }

    private void setViews() {
        utils = new Utils(getActivity());

        mWebview = rootView.findViewById(R.id.webView);

        mWebview.setWebViewClient(new WebViewClient() {

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                utils.startProgress();

                view.loadUrl(url);

                return true;
            }

            public void onPageFinished(WebView view, String url) {
                try {
                    utils.dismissProgress();
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        });

        mWebview.getSettings().setJavaScriptEnabled(true); // enable javascript
        mWebview.loadUrl(ApiUtils.BASE_URL+"privacy_policy.html");
    }
}
