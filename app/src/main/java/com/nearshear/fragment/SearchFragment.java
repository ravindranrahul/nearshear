package com.nearshear.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nearshear.Constant;
import com.nearshear.R;
import com.nearshear.adapter.ServicesListAdapter;
import com.nearshear.api.ApiUtils;
import com.nearshear.model.RegistrationDO;
import com.nearshear.model.SellerDo;
import com.nearshear.utils.Utils;

import java.util.ArrayList;

import io.supercharge.shimmerlayout.ShimmerLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchFragment extends Fragment{
    private View rootView;
    private RecyclerView rViewList;
    private Utils utils;
    private ServicesListAdapter sellerAdapter;
    String subCatId;
    ShimmerLayout shimmerText;
    public  static String word="";
    private TextView txtNoResultText;
    private LinearLayout layNoResult;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView=inflater.inflate(R.layout.fragment_search, container, false);
        setViews();

        return rootView;
    }

    private void setViews() {
        utils = new Utils(getActivity());
        RecyclerView.LayoutManager lLayout = new LinearLayoutManager(getContext());
        rViewList = rootView.findViewById(R.id.rViewSellerList);
        rViewList.setHasFixedSize(true);
        rViewList.setLayoutManager(lLayout);
        shimmerText =  rootView.findViewById(R.id.shimmer_text);
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
        txtNoResultText=rootView.findViewById(R.id.txtNoResultText);
        layNoResult=rootView.findViewById(R.id.layNoResult);
    }

    public void callServiceList(final String word) {
        this.word=word;
        String id=utils.getPreference(Constant.PREF_ID);
        shimmerText.setVisibility(View.VISIBLE);
        shimmerText.startShimmerAnimation();
        String lat=utils.getPreference(Constant.PREF_CUR_LAT);
        String lon=utils.getPreference(Constant.PREF_CUR_LON);

        ApiUtils.getAPIService().requestSearchList(id,word,lat,lon).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                shimmerText.stopShimmerAnimation();
                shimmerText.setVisibility(View.GONE);
                try {
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();

                    if (success.equals("1")) {
                        ArrayList<SellerDo> listOfSellerData = new ArrayList<>();
                        listOfSellerData  = response.body().getSeller_data();

                        layNoResult.setVisibility(View.GONE);
                        rViewList.setVisibility(View.VISIBLE);
                        sellerAdapter = new ServicesListAdapter(listOfSellerData);
                        rViewList.setAdapter(sellerAdapter);

                    } else if (success.equals("0")) {
                        rViewList.setVisibility(View.GONE);
                        layNoResult.setVisibility(View.VISIBLE);
                        utils.Toast(msg, null);
                        txtNoResultText.setText("'' " +word+" ''");
                        rViewList.setAdapter(null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                utils.Toast("Please check connection", null);
            }
        });
    }

    @Override
    public void onResume() {
        if(!word.equals("")){
            if (utils.isNetConnected()) {
                callServiceList(word);
            } else {
                utils.Toast(R.string.check_net, null);
            }
        }
        super.onResume();
    }
}
