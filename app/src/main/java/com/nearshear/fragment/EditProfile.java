package com.nearshear.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.nearshear.Constant;
import com.nearshear.MainActivity;
import com.nearshear.R;
import com.nearshear.api.ApiUtils;
import com.nearshear.api.KeyAbstract;
import com.nearshear.model.RegistrationDO;
import com.nearshear.model.RegistrationInfo;
import com.nearshear.utils.Utils;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfile extends Fragment implements View.OnClickListener {
    View rootView;
    private Utils utils;
    private EditText edtName;
    private EditText edtPhone;
    private Button btnSave;
    private ImageView imgUser;
    private int GALlERY_REQUEST_CODE = 100;
    private int CAMERA_REQUEST_CODE = 200;
    private String strUserImage;
    private String strName;
    private String strPhone;
    String loginType;
    private ImageView imgAddImage;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        setViews();
        return rootView;
    }

    private void setViews() {
        utils = new Utils(getActivity());
        loginType = utils.getPreference(Constant.PREF_LOGIN_TYPE);

        edtName = rootView.findViewById(R.id.edtName);
        edtPhone = rootView.findViewById(R.id.edtPhone);
        btnSave = rootView.findViewById(R.id.btnSave);
        imgUser = rootView.findViewById(R.id.imgUser);
        imgAddImage = rootView.findViewById(R.id.imgAddImage);
        imgUser.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        imgAddImage.setOnClickListener(this);

        strName=utils.getPreference(Constant.PREF_NAME);
        strPhone=utils.getPreference(Constant.PREF_PHONE);

        try {
            edtName.setText(strName);
            edtPhone.setText(strPhone);

            strUserImage = utils.getPreference(Constant.PREF_IMAGE);
//            Glide.with(this).load(strUserImage)
//                    .thumbnail(0.5f)
//                    .crossFade()
//                    .diskCacheStrategy(DiskCacheStrategy.ALL)
//                    .into(imgUser);
            Glide.with(this).load(strUserImage).placeholder(R.drawable.user_img).into(imgUser);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (loginType.equals("isApp")) {
            imgAddImage.setVisibility(View.VISIBLE);
        }
    }

    public void setProfilePic(Uri resultUri) {
        Glide.with(this).load(resultUri)
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgUser);
    }

    private void captureImg() {
        final CharSequence[] options = {"Camera", "Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Camera")) {
                    Intent m_intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File file = new File(Environment.getExternalStorageDirectory(), "MyPhoto.jpg");
                    Uri uri = FileProvider.getUriForFile(getContext(), getActivity().getApplicationContext().getPackageName() + ".provider", file);
                    m_intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, uri);
                    startActivityForResult(m_intent, CAMERA_REQUEST_CODE);
                } else if (options[item].equals("Gallery")) {
                    Intent intent = new Intent(getActivity(), AlbumSelectActivity.class);
                    intent.putExtra(Constants.INTENT_EXTRA_LIMIT, 1);
                    startActivityForResult(intent, GALlERY_REQUEST_CODE);
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                //File object of camera image
                File file = new File(Environment.getExternalStorageDirectory(), "MyPhoto.jpg");
                //Uri of camera image
//                    Uri uri = FileProvider.getUriForFile(this, this.getApplicationContext().getPackageName() + ".provider", file);
                Uri uri = Uri.fromFile(file);

                //crop image
                CropImage.activity(uri).setAspectRatio(4, 4)
                        .start(getActivity());
            }
        }

        if (requestCode == GALlERY_REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            ArrayList<Image> images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);
            Uri uri = Uri.parse("file://" + images.get(0).path);

            //crop image
            CropImage.activity(uri).setAspectRatio(4, 4)
                    .start(getActivity());
        }

    }

    private void callUpdate() {
        utils.startProgress();

        MultipartBody.Part image;
        image = null;
        try {
            MainActivity.strImage=MainActivity.strImage.substring(7);
            File file1 = new File(MainActivity.strImage);
            image = MultipartBody.Part.createFormData(KeyAbstract.KEY_IMAGE, file1.getName(), RequestBody.create(MediaType.parse("image/*"), file1));
        } catch (Exception e) {
            e.printStackTrace();
        }

        String id = utils.getPreference(Constant.PREF_ID);
        RequestBody bodyUserId = RequestBody.create(MediaType.parse("text/plain"), id);

        Log.d("rahul","get user id from edit profile"+id);

        RequestBody bodyName = RequestBody.create(MediaType.parse("text/plain"), strName);
        RequestBody bodyPhone = RequestBody.create(MediaType.parse("text/plain"), strPhone);

        ApiUtils.getAPIService().requestUpdateProfileAPI(bodyUserId, bodyName, bodyPhone, image).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                utils.dismissProgress();
                try {
                    Log.d("::", String.valueOf(response));
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();
                    if (success.equals("1")) {
                        ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                        listOfPostData = response.body().getPostdata();
                        RegistrationInfo registrationInfo = listOfPostData.get(0);

                        String imageLink = registrationInfo.getImage();

                        utils.setPreference(Constant.PREF_IMAGE, imageLink);
                        utils.setPreference(Constant.PREF_NAME, strName);
//                        utils.setPreference(Constant.PREF_EMAIL, email);
                        utils.setPreference(Constant.PREF_PHONE, strPhone);

                        MainActivity.strImage=null;

                        utils.Toast(msg, null);

                    } else {
                        utils.Toast(msg, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                utils.Toast("Network error", null);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSave:
                strName=edtName.getText().toString();
                strPhone=edtPhone.getText().toString();
                if (strName.equals("")) {
                    utils.Toast("Emter name");
                }
//                else if (strPhone.equals("")) {
//                    utils.Toast("Enter phone");
//                }
                else {
                    if (utils.isNetConnected()) {

                        callUpdate();
                    } else {
                        utils.Toast(R.string.check_net, null);
                    }
                }
                break;
            case R.id.imgUser:
                String loginType = utils.getPreference(Constant.PREF_LOGIN_TYPE);
                if (loginType.equals("isApp")) {
                    captureImg();
                } else {
                    if(loginType.equals("isGplus")){
                        utils.Toast("You cannot edit your profile because you have logged in using Gmail");
                    }else if(loginType.equals("isFb")){
                        utils.Toast("You cannot edit your profile because you have logged in using Facebook");
                    }
                }
                break;
            case R.id.imgAddImage:
                captureImg();
                break;
        }
    }
}
