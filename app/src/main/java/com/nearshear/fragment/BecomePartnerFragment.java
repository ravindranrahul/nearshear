package com.nearshear.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.MenuRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.nearshear.Constant;
import com.nearshear.MainActivity;
import com.nearshear.R;
import com.nearshear.adapter.CatSpinnerListAdapter;
import com.nearshear.adapter.CategoryListAdapter;
import com.nearshear.adapter.PhoneListAdapter;
import com.nearshear.adapter.SubCatSpinnerListAdapter;
import com.nearshear.adapter.VehiclePhotosListAdapter;
import com.nearshear.api.ApiUtils;
import com.nearshear.api.KeyAbstract;
import com.nearshear.model.CategoryDo;
import com.nearshear.model.ImageList;
import com.nearshear.model.PhoneDo;
import com.nearshear.model.RegistrationDO;
import com.nearshear.model.RegistrationInfo;
import com.nearshear.model.SubCategoryDo;
import com.nearshear.utils.Utils;

import org.michaelbel.bottomsheet.BottomSheet;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Part;

import static com.facebook.FacebookSdk.getApplicationContext;
import static com.nearshear.api.KeyAbstract.KEY_AREA;
import static com.nearshear.api.KeyAbstract.KEY_DESCRIPTION;
import static com.nearshear.api.KeyAbstract.KEY_ID;
import static com.nearshear.api.KeyAbstract.KEY_NAME;
import static com.nearshear.api.KeyAbstract.KEY_OFFER_TITLE;
import static com.nearshear.api.KeyAbstract.KEY_PIN;
import static com.nearshear.api.KeyAbstract.KEY_SOCIETY_NAME;

public class BecomePartnerFragment extends Fragment implements View.OnClickListener {
    private View rootView;
    private Utils utils;
    private EditText edtName;
    private EditText edtSocityName;
    private EditText edtArea;
    private EditText edtPin;
    private EditText edtCity;
    private EditText edtPhone;
    //    private EditText edtOfferTitle;
    private EditText edtDescription;
    private ImageView imgCover;
    private Button btnSubmit;
    private Spinner spinnerCategory;
    private Spinner spinnerSubCategory;
    private ImageView imgAddPhone;


    private String strCoverImage;
    private String strName;
    private String strSocietyName;
    private String strArea;
    private String strPin;
    private String strCity;
    private String strPhone;
    //    private String strOfferTitle;
    private String strDescription;
    private String strSelectedCatId = "";
    private String strStrSelectedSubCatId = "";
    private String strOfferBonusTitle = "";

    public LinearLayoutManager layoutManager;
    private RecyclerView rViewImageList;
    public static ArrayList<ImageList> imageLists;
    private VehiclePhotosListAdapter adapter;
    private CatSpinnerListAdapter adapterCatSpinner;
    private SubCatSpinnerListAdapter adapterSubCatSpinner;
    private ArrayList<PhoneDo> arrayPhone;
    private PhoneListAdapter adapterPhone;
    private RecyclerView rViewPhone;
    private EditText edtOfferBonusTitle;

    private ImageView imgOne;
    private ImageView imgTwo;
    private ImageView imgThree;
    private ImageView imgFour;
    private ImageView imgFive;

    private ImageView imgEditOne;
    private ImageView imgEditTwo;
    private ImageView imgEditThree;
    private ImageView imgEditFour;
    private ImageView imgEditFive;
    private ImageView imgEditCover;

    private String strImageOne = "";
    private String strImageTwo = "";
    private String strImageThree = "";
    private String strImageFour = "";
    private String strImageFive = "";
    private String strEditCover = "";

    private int REQUEST_CODE_ONE = 101;
    private int REQUEST_CODE_TWO = 102;
    private int REQUEST_CODE_THREE = 103;
    private int REQUEST_CODE_FOUR = 104;
    private int REQUEST_CODE_FIVE = 105;


    private int PLACE_PICKER_REQUEST = 1;
    private ImageView imgLocation;
    private String strLat;
    private String strLon;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_become_partner, container, false);
        setViews();
        callCategory();

        spinnerCategory.setPrompt("Select");

        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CategoryDo list = (CategoryDo) parent.getItemAtPosition(position);
                ArrayList<SubCategoryDo> sub_category_data = list.getSub_category_data();
                strSelectedCatId = list.getCategory_id();

                if (!strSelectedCatId.equals("")) {
                    ((TextView) spinnerCategory.getSelectedView()).setTextColor(getResources().getColor(R.color.black));
                }

                SubCategoryDo subCategoryDo = new SubCategoryDo();
                subCategoryDo.setSub_category_id("");
                subCategoryDo.setSub_category_name("Select Sub Category");
                sub_category_data.add(0, subCategoryDo);

                adapterSubCatSpinner = new SubCatSpinnerListAdapter(getContext(), sub_category_data);
                spinnerSubCategory.setAdapter(adapterSubCatSpinner);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerSubCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SubCategoryDo list = (SubCategoryDo) parent.getItemAtPosition(position);
                strStrSelectedSubCatId = list.getSub_category_id();
                if (!strStrSelectedSubCatId.equals("")) {
                    ((TextView) spinnerSubCategory.getSelectedView()).setTextColor(getResources().getColor(R.color.black));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


//        ImageList list;
//        for (int i1 = 0; i1 < 5;  i1++) {
//            list = new ImageList();
//            list.setImage("");
//            imageLists.add(list);
//        }
//        adapter = new VehiclePhotosListAdapter(imageLists );
//        rViewImageList.setAdapter(adapter);
        return rootView;
    }

    private void setViews() {

        imageLists = new ArrayList<>();
        utils = new Utils(getActivity());
        arrayPhone = new ArrayList<>();

        layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rViewImageList = (RecyclerView) rootView.findViewById(R.id.rViewImageList);
        rViewImageList.setHasFixedSize(true);
        rViewImageList.setLayoutManager(layoutManager);

        edtName = rootView.findViewById(R.id.edtName);
        edtSocityName = rootView.findViewById(R.id.edtSocityName);
        edtArea = rootView.findViewById(R.id.edtArea);
        edtPin = rootView.findViewById(R.id.edtPin);
        edtCity = rootView.findViewById(R.id.edtCity);
        edtPhone = rootView.findViewById(R.id.edtPhone);
//        edtOfferTitle = rootView.findViewById(R.id.edtOfferTitle);
        edtDescription = rootView.findViewById(R.id.edtDescription);
        imgCover = rootView.findViewById(R.id.imgCover);
        btnSubmit = rootView.findViewById(R.id.btnSubmit);
        spinnerCategory = rootView.findViewById(R.id.spinnerCategory);
        spinnerSubCategory = rootView.findViewById(R.id.spinnerSubCategory);
        imgCover.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        imgAddPhone = rootView.findViewById(R.id.imgAddPhone);
        imgAddPhone.setOnClickListener(this);

        RecyclerView.LayoutManager lLayout = new LinearLayoutManager(getContext());
        rViewPhone = rootView.findViewById(R.id.rViewPhoneList);
        rViewPhone.setHasFixedSize(true);
        rViewPhone.setLayoutManager(lLayout);
        edtOfferBonusTitle = rootView.findViewById(R.id.edtOfferBonusTitle);

        imgOne = rootView.findViewById(R.id.imgOne);
        imgTwo = rootView.findViewById(R.id.imgTwo);
        imgThree = rootView.findViewById(R.id.imgThree);
        imgFour = rootView.findViewById(R.id.imgFour);
        imgFive = rootView.findViewById(R.id.imgFive);

        imgOne.setOnClickListener(this);
        imgTwo.setOnClickListener(this);
        imgThree.setOnClickListener(this);
        imgFour.setOnClickListener(this);
        imgFive.setOnClickListener(this);

        imgEditOne = rootView.findViewById(R.id.imgEditOne);
        imgEditTwo = rootView.findViewById(R.id.imgEditTwo);
        imgEditThree = rootView.findViewById(R.id.imgEditThree);
        imgEditFour = rootView.findViewById(R.id.imgEditFour);
        imgEditFive = rootView.findViewById(R.id.imgEditFive);
        imgEditCover = rootView.findViewById(R.id.imgEditCover);

        imgLocation = rootView.findViewById(R.id.imgLocation);
        imgLocation.setOnClickListener(this);

        edtSocityName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    PlacePicker.IntentBuilder builder1 = new PlacePicker.IntentBuilder();

                    try {
                        utils.startProgress();
                        startActivityForResult(builder1.build(getActivity()), PLACE_PICKER_REQUEST);
                    } catch (GooglePlayServicesRepairableException e) {
                        e.printStackTrace();
                    } catch (GooglePlayServicesNotAvailableException e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }
        });

//        strName=utils.getPreference(Constant.PREF_NAME);
//        strArea=utils.getPreference(Constant.PREF_AREA);
//        strPhone=utils.getPreference(Constant.PREF_PHONE);
//        strCity=utils.getPreference(Constant.PREF_CITY);
//        strSocietyName=utils.getPreference(Constant.PREF_SOCIETY_NAME);
//        strPin=utils.getPreference(Constant.PREF_PIN);
//        edtName.setText(strName);
//        edtArea.setText(strArea);
//        edtPhone.setText(strPhone);
//        edtCity.setText(strCity);
//        edtSocityName.setText(strSocietyName);
//        edtPin.setText(strPin);

//        imgAddPhone2nd = rootView.findViewById(R.id.imgAddPhone2nd);
//        imgAddPhone3rd = rootView.findViewById(R.id.imgAddPhone3rd);
//        lay2ndPhone = rootView.findViewById(R.id.lay2ndPhone);
//        lay3ndPhone = rootView.findViewById(R.id.lay3ndPhone);
//        lay2ndPhone.setOnClickListener(this);
//        lay3ndPhone.setOnClickListener(this);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            ArrayList<Image> images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);
            ImageList list;

            for (int i = 0; i < images.size(); i++) {
                list = new ImageList();
                list.setImage(images.get(i).path);
                imageLists.add(list);
            }
            setImage();
        }


        if (requestCode == REQUEST_CODE_ONE && resultCode == Activity.RESULT_OK && data != null) {
            ArrayList<Image> images1 = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);
            ImageList list1 = new ImageList();
            list1.setImage(images1.get(0).path);
            imageLists.set(0, list1);

            Glide.with(getActivity()).load(imageLists.get(0).getImage())
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgCover);

            Glide.with(getActivity()).load(imageLists.get(0).getImage())
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgOne);
        }

        if (requestCode == REQUEST_CODE_TWO && resultCode == Activity.RESULT_OK && data != null) {
            ArrayList<Image> images1 = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);
            ImageList list1 = new ImageList();
            list1.setImage(images1.get(0).path);
            imageLists.set(1, list1);

            Glide.with(getActivity()).load(imageLists.get(1).getImage())
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgTwo);
        }

        if (requestCode == REQUEST_CODE_THREE && resultCode == Activity.RESULT_OK && data != null) {
            ArrayList<Image> images1 = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);
            ImageList list1 = new ImageList();
            list1.setImage(images1.get(0).path);
            imageLists.set(2, list1);

            Glide.with(getActivity()).load(imageLists.get(2).getImage())
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgThree);
        }

        if (requestCode == REQUEST_CODE_FOUR && resultCode == Activity.RESULT_OK && data != null) {
            ArrayList<Image> images1 = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);
            ImageList list1 = new ImageList();
            list1.setImage(images1.get(0).path);
            imageLists.set(3, list1);

            Glide.with(getActivity()).load(imageLists.get(3).getImage())
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgFour);
        }

        if (requestCode == REQUEST_CODE_FIVE && resultCode == Activity.RESULT_OK && data != null) {
            ArrayList<Image> images1 = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);
            ImageList list1 = new ImageList();
            list1.setImage(images1.get(0).path);
            imageLists.set(4, list1);

            Glide.with(getActivity()).load(imageLists.get(4).getImage())
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgFive);
        }


//            adapter = new VehiclePhotosListAdapter(imageLists);
//            rViewImageList.setAdapter(adapter);

        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == getActivity().RESULT_OK) {
                Place place = PlacePicker.getPlace(getActivity(),data);
                String toastMsg = String.format("Place: %s", place.getName());
//                Toast.makeText(getActivity(), toastMsg, Toast.LENGTH_LONG).show();

                LatLng latlon = place.getLatLng();
                strSocietyName = String.valueOf(place.getName()+","+place.getAddress());
                edtSocityName.setText(strSocietyName);
                strLat= String.valueOf(latlon.latitude);
                strLon= String.valueOf(latlon.longitude);

                try {
                    Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                    List<Address> addresses = geocoder.getFromLocation(latlon.latitude, latlon.longitude, 10);
                    if (addresses != null && addresses.size() > 0) {
                        strCity= null;
                        strArea= null;
                        for(int i=0;i<addresses.size();i++){
                            if(strCity == null || strArea== null){
                                strCity = addresses.get(i).getLocality();
                                strArea = addresses.get(i).getSubLocality();
                                strPin=addresses.get(i).getPostalCode();
                            }
                        }
                        Log.i("::", "City: " + addresses.get(0).getSubAdminArea());
                        Log.i("::", "State: " + addresses.get(0).getAdminArea());
                        Log.i("::", "Zip: " + addresses.get(0).getPostalCode());
                        Log.i("::", "Area: " + addresses.get(0).getSubLocality());

                        edtCity.setText(strCity);
                        edtArea.setText(strArea);
                        edtPin.setText(strPin);

//                    String countryName = addresses.get(0).getAddressLine(2);
//                            edtRegister_State.setText(stateName);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }

    }

    private void setImage() {
        imgCover.setImageDrawable(null);
        imgOne.setImageDrawable(null);
        imgTwo.setImageDrawable(null);
        imgThree.setImageDrawable(null);
        imgFour.setImageDrawable(null);
        imgFive.setImageDrawable(null);
        imgEditOne.setVisibility(View.GONE);
        imgEditTwo.setVisibility(View.GONE);
        imgEditThree.setVisibility(View.GONE);
        imgEditFour.setVisibility(View.GONE);
        imgEditFive.setVisibility(View.GONE);
        imgEditCover.setVisibility(View.GONE);

        for (int i = 0; i < imageLists.size(); i++) {
            switch (i) {
                case 0:
                    imgEditOne.setVisibility(View.VISIBLE);
                    imgEditCover.setVisibility(View.VISIBLE);
                    strImageOne = imageLists.get(i).getImage();
                    Glide.with(getActivity()).load(imageLists.get(i).getImage())
                            .thumbnail(0.5f)
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imgCover);

                    Glide.with(getActivity()).load(imageLists.get(i).getImage())
                            .thumbnail(0.5f)
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imgOne);
                    break;
                case 1:
                    imgEditTwo.setVisibility(View.VISIBLE);
                    strImageTwo = imageLists.get(i).getImage();
                    Glide.with(getActivity()).load(imageLists.get(i).getImage())
                            .thumbnail(0.5f)
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imgTwo);
                    break;
                case 2:
                    imgEditThree.setVisibility(View.VISIBLE);
                    strImageThree = imageLists.get(i).getImage();
                    Glide.with(getActivity()).load(imageLists.get(i).getImage())
                            .thumbnail(0.5f)
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imgThree);
                    break;
                case 3:
                    imgEditFour.setVisibility(View.VISIBLE);
                    strImageFour = imageLists.get(i).getImage();
                    Glide.with(getActivity()).load(imageLists.get(i).getImage())
                            .thumbnail(0.5f)
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imgFour);
                    break;
                case 4:
                    imgEditFive.setVisibility(View.VISIBLE);
//                    imgEditFive.setVisibility(View.VISIBLE);
                    strImageFive = imageLists.get(i).getImage();
                    Glide.with(getActivity()).load(imageLists.get(i).getImage())
                            .thumbnail(0.5f)
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imgFive);
                    break;
            }
//                if (i == 0) {
//
//                }
        }

    }

    private void dialog(final int REQUEST_CODE) {

        String[] items = {"Change Image", "Remove Image"};
        int[] icons = {R.mipmap.about, R.mipmap.about};
        BottomSheet.Builder builder = new BottomSheet.Builder(getActivity());
        builder.setTitle("Action");
        builder.setContentType(BottomSheet.LIST);
        builder.setTitleTextColorRes(R.color.black);
        builder.setItemTextColorRes(R.color.black);
        builder.setBackgroundColorRes(R.color.white);
        builder.setFullWidth(true);
        builder.setDividers(true);
        builder.setWindowDimming(100);
//        builder.setFabBehavior(floatingActionButton, BottomSheet.FAB_SHOW_HIDE);

        builder.setItems(items, icons, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // your code here.
                switch (which) {
                    case 0:
                        Intent img = new Intent(getActivity(), AlbumSelectActivity.class);
                        img.putExtra(Constants.INTENT_EXTRA_LIMIT, 1);
                        startActivityForResult(img, REQUEST_CODE);
                        break;

                    case 1:
                        if (REQUEST_CODE == 101) {
                            imageLists.remove(0);
                        }

                        if (REQUEST_CODE == 102) {
                            imageLists.remove(1);
                        }

                        if (REQUEST_CODE == 103) {
                            imageLists.remove(2);
                        }

                        if (REQUEST_CODE == 104) {
                            imageLists.remove(3);
                        }

                        if (REQUEST_CODE == 105) {
                            imageLists.remove(4);
                        }
                        setImage();
                        break;

                }
                switch (which) {
                    case 0:
                        utils.Toast("0");
                        break;
                    case 1:
                        utils.Toast("1");
                        break;
                }
            }
        });

        builder.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                // your code here.
            }
        });

        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                // your code here.
            }
        });

        builder.show();
    }

    private void callCategory() {
        utils.startProgress();
        ApiUtils.getAPIService().requestCategoryList("").enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                utils.dismissProgress();
                try {
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();

                    if (success.equals("1")) {
                        ArrayList<CategoryDo> listOfCatData = new ArrayList<>();
                        listOfCatData = response.body().getCategory_data();

                        adapterCatSpinner = new CatSpinnerListAdapter(getContext(), listOfCatData);
                        CategoryDo category = new CategoryDo();
                        category.setCategory_name("Select Category");
                        category.setCategory_id("");
                        listOfCatData.add(0, category);
                        spinnerCategory.setAdapter(adapterCatSpinner);

                    } else if (success.equals("0")) {
                        utils.Toast(msg, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                utils.Toast("Please check connection", null);
            }
        });
    }

    private void callBecomePartner() {
        utils.startProgress();
        MultipartBody.Part image = null;
        List<MultipartBody.Part> imagearray;
        imagearray = null;


        try {
            strCoverImage = imageLists.get(0).getImage();
            File file1 = new File(strCoverImage);
            image = MultipartBody.Part.createFormData(KeyAbstract.KEY_IMAGE, file1.getName(), RequestBody.create(MediaType.parse("image/*"), file1));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            imagearray = new ArrayList<>();
            for (int i = 0, l = imageLists.size(); i < l; i++) {
                if (!imageLists.get(i).getImage().contains("http")) {
                    File file = new File(imageLists.get(i).getImage());
                    imagearray.add(MultipartBody.Part.createFormData(KeyAbstract.KEY_IMAGES, file.getName(), RequestBody.create(MediaType.parse("image/*"), file)));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        String id = utils.getPreference(Constant.PREF_ID);
        RequestBody rbId = RequestBody.create(MediaType.parse("text/plain"), id);
        RequestBody rbName = RequestBody.create(MediaType.parse("text/plain"), strName);
        RequestBody rbSocietyName = RequestBody.create(MediaType.parse("text/plain"), strSocietyName);
        RequestBody rbArea = RequestBody.create(MediaType.parse("text/plain"), strArea);
        RequestBody rbPin = RequestBody.create(MediaType.parse("text/plain"), strPin);
        RequestBody rbCity = RequestBody.create(MediaType.parse("text/plain"), strCity);
        RequestBody rbPhone = RequestBody.create(MediaType.parse("text/plain"), strPhone);
        RequestBody rbOfferTitle = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody rbDescription = RequestBody.create(MediaType.parse("text/plain"), strDescription);
        RequestBody rbCatId = RequestBody.create(MediaType.parse("text/plain"), strSelectedCatId);
        RequestBody rbSubCatId = RequestBody.create(MediaType.parse("text/plain"), strStrSelectedSubCatId);
        RequestBody rbBonusTitle = RequestBody.create(MediaType.parse("text/plain"), strOfferBonusTitle);
        RequestBody rbTzone= RequestBody.create(MediaType.parse("text/plain"), utils.timeZoneId());
        RequestBody rbLat = RequestBody.create(MediaType.parse("text/plain"), strLat);
        RequestBody rbLon = RequestBody.create(MediaType.parse("text/plain"), strLon);


        ApiUtils.getAPIService().requestBecomePartner(rbId, rbName, rbSocietyName, rbArea, rbPin, rbCity, rbPhone, rbOfferTitle, rbBonusTitle, rbDescription, rbCatId, rbSubCatId, rbLat,rbLon,rbTzone,image, imagearray).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                utils.dismissProgress();
                try {
                    Log.d("::", String.valueOf(response));
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();
                    if (success.equals("1")) {
                        utils.showAlertMessage("OK",msg);
                        ((MainActivity) getActivity()).setProfile("Profile");
//                        utils.setPreference(Constant.PREF_AREA,strArea);
//                        utils.setPreference(Constant.PREF_PHONE,strPhone);
//                        utils.setPreference(Constant.PREF_CITY,strCity);
//                        utils.setPreference(Constant.PREF_SOCIETY_NAME,strSocietyName);
//                        utils.setPreference(Constant.PREF_PIN,strPin);
                    } else {
                        utils.Toast(msg, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                utils.Toast("Network error", null);
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgCover:
                if (strImageOne.equals("")) {
                    selectImage();
                } else {
                    dialog(REQUEST_CODE_ONE);
                }
                break;
            case R.id.btnSubmit:
                strName = edtName.getText().toString();
                strSocietyName = edtSocityName.getText().toString();
                strArea = edtArea.getText().toString();
                strPin = edtPin.getText().toString();
                strCity = edtCity.getText().toString();
//                strPhone = edtPhone.getText().toString();
//                strOfferTitle = edtOfferTitle.getText().toString();
                strDescription = edtDescription.getText().toString();
                strOfferBonusTitle = edtOfferBonusTitle.getText().toString();
                strPhone=edtPhone.getText().toString();

                if (strName.equals("")) {
                    utils.Toast("Enter name");
                } else if (strSocietyName.equals("")) {
                    utils.Toast("Enter society name");
                } else if (strArea.equals("")) {
                    utils.Toast("Enter area");
                } else if (strPin.equals("")) {
                    utils.Toast("Enter pin");
                } else if (strCity.equals("")) {
                    utils.Toast("Enter city");
                } else if (arrayPhone.size() < 1 && strPhone.equals("")) {
                    utils.Toast("Enter phone");
                }
//                else if (strOfferTitle.equals("")) {
//                    utils.Toast("Enter offer title");
//                }
                else if (strSelectedCatId.equals("")) {
                    utils.Toast("Select category");
                } else if (strStrSelectedSubCatId.equals("")) {
                    utils.Toast("Select sub category");
                }
//                else if (strOfferBonusTitle.equals("")) {
//                    utils.Toast("Enter bonus title");
//                }
                else if (strDescription.equals("")) {
                    utils.Toast("Enter description");
                } else {
                    PhoneDo phone=new PhoneDo();
                    phone.setPhone(strPhone);
                    arrayPhone.add(phone);


                    for (int i = 0; i < arrayPhone.size(); i++) {
                        if (i == 0) {
                            strPhone = arrayPhone.get(i).getPhone();
                        } else {
                            strPhone = strPhone + "," + arrayPhone.get(i).getPhone();
                        }
                    }
                    if (utils.isNetConnected()) {
                        callBecomePartner();
                    } else {
                        utils.Toast(R.string.check_net, null);
                    }
                }
                break;
            case R.id.imgAddPhone:
                String strPhone = edtPhone.getText().toString();
                PhoneDo phone = null;
                if (!strPhone.equals("")) {
                    phone = new PhoneDo();

                    phone.setPhone(strPhone);
                    arrayPhone.add(phone);
                    Collections.reverse(arrayPhone);
                    adapterPhone = new PhoneListAdapter(arrayPhone, "add");
                    rViewPhone.setAdapter(adapterPhone);
                    edtPhone.setText("");
                } else {
                    utils.Toast("Enter phone no.");
                }
//                imgAddPhone.setImageResource(R.mipmap.favourite);
//                lay2ndPhone.setVisibility(View.VISIBLE);
                break;
            case R.id.imgOne:
                if (strImageOne.equals("")) {
                    selectImage();
                } else {
                    dialog(REQUEST_CODE_ONE);
//                    Intent img = new Intent(getActivity(), AlbumSelectActivity.class);
//                    img.putExtra(Constants.INTENT_EXTRA_LIMIT, 1);
//                    startActivityForResult(img, REQUEST_CODE_ONE);
                }
                break;
            case R.id.imgTwo:
                if (strImageTwo.equals("")) {
                    selectImage();
                } else {
                    dialog(REQUEST_CODE_TWO);
//                    Intent img = new Intent(getActivity(), AlbumSelectActivity.class);
//                    img.putExtra(Constants.INTENT_EXTRA_LIMIT, 1);
//                    startActivityForResult(img, REQUEST_CODE_TWO);
                }
                break;
            case R.id.imgThree:
                if (strImageThree.equals("")) {
                    selectImage();
                } else {
                    dialog(REQUEST_CODE_THREE);
//                    Intent img = new Intent(getActivity(), AlbumSelectActivity.class);
//                    img.putExtra(Constants.INTENT_EXTRA_LIMIT, 1);
//                    startActivityForResult(img, REQUEST_CODE_THREE);
                }
                break;
            case R.id.imgFour:
                if (strImageFour.equals("")) {
                    selectImage();
                } else {
                    dialog(REQUEST_CODE_FOUR);
//                    Intent img = new Intent(getActivity(), AlbumSelectActivity.class);
//                    img.putExtra(Constants.INTENT_EXTRA_LIMIT, 1);
//                    startActivityForResult(img, REQUEST_CODE_FOUR);
                }
                break;
            case R.id.imgFive:
                if (strImageFive.equals("")) {
                    selectImage();
                } else {
                    dialog(REQUEST_CODE_FIVE);
//                    Intent img = new Intent(getActivity(), AlbumSelectActivity.class);
//                    img.putExtra(Constants.INTENT_EXTRA_LIMIT, 1);
//                    startActivityForResult(img, REQUEST_CODE_FIVE);
                }
                break;
            case R.id.imgLocation:
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

                try {
                    utils.startProgress();
                    startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
                break;

        }
    }

    private void selectImage() {
        Intent intent = new Intent(getActivity(), AlbumSelectActivity.class);
        intent.putExtra(Constants.INTENT_EXTRA_LIMIT, 5 - imageLists.size());
        startActivityForResult(intent, Constants.REQUEST_CODE);
    }

    @Override
    public void onResume() {
        super.onResume();
    utils
            .dismissProgress();
    }
}
