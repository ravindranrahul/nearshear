package com.nearshear.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.nearshear.R;
import com.nearshear.adapter.ServiceTestAdapter;
import com.nearshear.api.RequestAPI;
import com.nearshear.model.ListItem;
import com.nearshear.model.Post;
import com.nearshear.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddServiceTest extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private LinearLayoutManager layoutManager;
    private Utils utils;
    private View rootView;

    private List<ListItem> listItems;


    public AddServiceTest() {
        // Required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.recycler_test, container, false);

        setView();

        return rootView;
    }

    private void setView(){
        utils = new Utils(getActivity());
//        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        RecyclerView.LayoutManager lLayout = new LinearLayoutManager(getContext());
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        Log.d("recycler123 ",""+recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(lLayout);

//        listItems = new ArrayList<>();
//
//        for (int i = 0; i < 11; i++){
//            ListItem listItem = new ListItem(
//                    i,
//                    "heading "+i,
//                    "text " + i
//            );
//
//            listItems.add(listItem);
//        }
//
//
//        adapter = new ServiceTestAdapter(listItems,getContext());
//        recyclerView.setAdapter(adapter);

        callReq();
    }

    private void callReq(){

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        final RecyclerView rv = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(layoutManager);


//        textViewResult = findViewById(R.id.text_view_result);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        RequestAPI requestAPI = retrofit.create(RequestAPI.class);

        Call<List<Post>> call = requestAPI.getPost();

        call.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {

                if (!response.isSuccessful()){
//                    textViewResult.setText("code: "+response.code());
                    Log.d("isSuccefull error ",""+response.code());
                    return;
                }

                Log.d("rahul ",""+response.body());

                Log.d("succes ",""+response.code());

//                JSONArray jsonArray = new JSONArray(response.body());
//                JSONObject jsonObject = new JSONObject(jsonArray);
//                Log.d("rahul ",""+jsonArray);

                List<Post> posts = response.body();

                String[] postList = new String[posts.size()];

                Log.d("jsonArray ",""+posts);
                Log.d("jsonArray ",""+posts.size());
                Log.d("jsonArrayResponse ",""+response.body());
                listItems = new ArrayList<>();


                for (int i = 0; i < posts.size(); i++){
                    //                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                    ListItem listItem = new ListItem(
                            i,
                            posts.get(i).getTitle(),
                            posts.get(i).getText()
                    );

                    listItem.setId(i);
                    listItem.setTitle(posts.get(i).getTitle());
                    listItem.setText(posts.get(i).getText());

                    listItems.add(listItem);

                }

                Log.d("saurabh ",""+listItems);

                adapter = new ServiceTestAdapter(listItems,getContext());

                Log.d("saurabh ",""+rv);

                rv.setAdapter(adapter);

//                recyclerView.setAdapter(adapter);

//                List<Post> posts = response.body();


//                for (Post post:posts) {
//                    ListItem listItem = new ListItem(
//                            post.getId(),
//                            post.getTitle(),
//                            post.getText()
//                    );
//
//                    listItems.add(listItem);
//                }
////
//                adapter = new ServiceTestAdapter(listItems,getContext());
////
//                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
//                textViewResult.setText(t.getMessage());
                Log.d("error ",t.getMessage());
            }
        });
//        to be continued...


    }

}
