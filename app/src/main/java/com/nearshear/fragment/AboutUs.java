package com.nearshear.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nearshear.MainActivity;
import com.nearshear.R;

public class AboutUs extends Fragment {
    private View rootView;
    private CardView cardTermCondition;
    private CardView cardPrivacyPolicy;
    private CardView cardAbout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_about_us, container, false);
        setViews();

        cardAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).setAboutUsInfo("About Us");
            }
        });

        cardPrivacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).setPrivacyPolicy("Privacy Policy");
            }
        });

        cardTermCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).setTermsCondition("Terms & Conditions");
            }
        });
        return rootView;
    }

    private void setViews() {
        cardAbout = rootView.findViewById(R.id.cardAbout);
        cardTermCondition = rootView.findViewById(R.id.cardTermCondition);
        cardPrivacyPolicy = rootView.findViewById(R.id.cardPrivacyPolicy);
    }
}
