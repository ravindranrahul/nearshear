package com.nearshear.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nearshear.Constant;
import com.nearshear.R;
import com.nearshear.adapter.FavouriteListAdapter;
import com.nearshear.adapter.ServicesListAdapter;
import com.nearshear.api.ApiUtils;
import com.nearshear.model.RegistrationDO;
import com.nearshear.model.SellerDo;
import com.nearshear.utils.Utils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavouriteFragment extends Fragment {
    private View rootView;
    private Utils utils;
    private FavouriteListAdapter favouriteListAdapter;
    private RecyclerView rViewList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_favourite, container, false);
        setViews();

        if (utils.isNetConnected()) {
            callFavouriteList();
        } else {
            utils.Toast(R.string.check_net, null);
        }
        return rootView;
    }

    private void setViews() {
        utils = new Utils(getActivity());
        RecyclerView.LayoutManager lLayout = new LinearLayoutManager(getContext());
        rViewList = rootView.findViewById(R.id.rViewFavouriteList);
        rViewList.setHasFixedSize(true);
        rViewList.setLayoutManager(lLayout);
    }

    private void callFavouriteList() {
        String id = utils.getPreference(Constant.PREF_ID);
//        utils.startProgress();
//        final ShimmerLayout shimmerText = (ShimmerLayout) rootView.findViewById(R.id.shimmer_text);
//        shimmerText.startShimmerAnimation();
        ApiUtils.getAPIService().requestFavoriteList(id).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
//                utils.dismissProgress();
//                shimmerText.stopShimmerAnimation();
//                shimmerText.setVisibility(View.GONE);
                try {
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();

                    if (success.equals("1")) {
                        ArrayList<SellerDo> listOfFavData = new ArrayList<>();
                        listOfFavData= response.body().getFavourite_data();

                        favouriteListAdapter = new FavouriteListAdapter(listOfFavData);
                        rViewList.setAdapter(favouriteListAdapter);

                    } else if (success.equals("0")) {
                        utils.Toast(msg, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                utils.Toast("Please check connection", null);
            }
        });
    }

}
