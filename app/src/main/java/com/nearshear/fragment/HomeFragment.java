package com.nearshear.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.nearshear.Constant;
import com.nearshear.MainActivity;
import com.nearshear.OtpActivity;
import com.nearshear.R;
import com.nearshear.SignUpActivity;
import com.nearshear.adapter.CategoryListAdapter;
import com.nearshear.adapter.RecyclerViewDataAdapter;
import com.nearshear.adapter.ViewPagerAdapter;
import com.nearshear.api.ApiUtils;
import com.nearshear.model.CategoryDo;
import com.nearshear.model.RegistrationDO;
import com.nearshear.model.RegistrationInfo;
import com.nearshear.model.SliderDo;
import com.nearshear.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.supercharge.shimmerlayout.ShimmerLayout;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private Utils utils;
    private View rootView;
    private RecyclerView rViewList;
    private RecyclerViewDataAdapter categoryAdapter;
    private ViewPagerAdapter pagerAdapter;
    private ViewPager viewPager;
    private CircleIndicator indicator;
    private TextView txtEditLocation;
    private EditText edtSearch;
    private TextView txtLocation;

    int PLACE_PICKER_REQUEST = 1;
    private ShimmerLayout shimmerText;

    GoogleApiClient googleApiClient;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_home, container, false);
        setViews();

        if (utils.isNetConnected()) {
            callCategory();
        } else {
            utils.Toast(R.string.check_net, null);
        }
        return rootView;
    }

    private void setViews() {
        utils = new Utils(getActivity());
        RecyclerView.LayoutManager lLayout = new LinearLayoutManager(getContext());
        rViewList = rootView.findViewById(R.id.rViewCatList);
        rViewList.setHasFixedSize(true);
        rViewList.setLayoutManager(lLayout);


        viewPager = rootView.findViewById(R.id.viewPager);
        indicator = rootView.findViewById(R.id.indicator);
        txtEditLocation = rootView.findViewById(R.id.txtEditLocation);
        txtEditLocation.setOnClickListener(this);

        shimmerText = rootView.findViewById(R.id.shimmer_text);
        edtSearch = rootView.findViewById(R.id.edtSearch);
        edtSearch.setOnClickListener(this);
        txtLocation = rootView.findViewById(R.id.txtLocation);

        String current_location = utils.getPreference(Constant.PREF_CUR_LOCATION);
        if (!current_location.equals("")) {
            txtLocation.setText(current_location);
        } else {
            checkLoc();
        }
    }


    private void callCategory() {
        shimmerText.setVisibility(View.VISIBLE);
        shimmerText.startShimmerAnimation();
        String id = utils.getPreference(Constant.PREF_ID);
        ApiUtils.getAPIService().requestCategoryList(id).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                shimmerText.stopShimmerAnimation();
                shimmerText.setVisibility(View.GONE);
                try {
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();

                    if (success.equals("1")) {
                        ArrayList<CategoryDo> listOfCatData = new ArrayList<>();
                        listOfCatData = response.body().getCategory_data();

                        ArrayList<SliderDo> listOfSliderData = new ArrayList<>();
                        listOfSliderData = response.body().getSlider_data();

                        pagerAdapter = new ViewPagerAdapter(getActivity(), listOfSliderData);
                        viewPager.setAdapter(pagerAdapter);
                        indicator.setViewPager(viewPager);

                        categoryAdapter = new RecyclerViewDataAdapter(getActivity(), listOfCatData);
                        rViewList.setAdapter(categoryAdapter);

                        ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                        listOfPostData = response.body().getPostdata();
                        String favourite_count = listOfPostData.get(0).getTotal_fav_count();
                        ((MainActivity) getActivity()).setFavouriteCount(favourite_count);

//                        Log.d("size", String.valueOf(listOfCatData.size()));
//                        Log.d("index", String.valueOf(listOfCatData.get(2).getCategory_name()));

                    } else if (success.equals("0")) {
                        utils.Toast(msg, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                utils.Toast("Please check connection", null);
            }
        });
    }

    public void checkLoc() {
        utils.startProgress();
        if (googleApiClient != null) {
            googleApiClient.disconnect();
        }
//        if (googleApiClient == null) {
        googleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        googleApiClient.connect();

        final LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        //**************************
        builder.setAlwaysShow(true); //this is the key ingredient
        //**************************

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @SuppressLint("MissingPermission")
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.

                        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, new com.google.android.gms.location.LocationListener() {
                            @Override
                            public void onLocationChanged(Location location) {
                                Constant.CURRENT_LAT = location.getLatitude();
                                Constant.CURRENT_LONG = location.getLongitude();
                                Log.d("::::", Constant.CURRENT_LAT + " , " + Constant.CURRENT_LONG);

                                try {
                                    Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                                    List<Address> addresses = geocoder.getFromLocation(Constant.CURRENT_LAT, Constant.CURRENT_LONG, 10);
                                    if (addresses != null && addresses.size() > 0) {
                                        String city = null;
                                        String area = null;
                                        for(int i=0;i<addresses.size();i++){
                                            if(city == null || area == null){
                                                city = addresses.get(i).getLocality();
                                                area = addresses.get(i).getSubLocality();
                                            }
                                        }
//                                        Log.i("::", "City: " + addresses.get(0).getSubAdminArea());
//                                        Log.i("::", "State: " + addresses.get(0).getAdminArea());
//                                        Log.i("::", "Zip: " + addresses.get(0).getPostalCode());
//                                        Log.i("::", "Area: " + addresses.get(0).getSubLocality());


                                        utils.setPreference(Constant.PREF_CUR_LAT, String.valueOf(Constant.CURRENT_LAT));
                                        utils.setPreference(Constant.PREF_CUR_LON, String.valueOf(Constant.CURRENT_LONG));
                                        if(area == null){
                                            utils.setPreference(Constant.PREF_CUR_LOCATION, String.valueOf(city));
                                            txtLocation.setText(city);
                                        }else{
                                            utils.setPreference(Constant.PREF_CUR_LOCATION, String.valueOf(area + "," + city));
                                            txtLocation.setText(area + "," + city);
                                        }

                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }


                                LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
                                // save your location here
                            }
                        });



                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    getActivity(), 1000);

                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.

                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.

                        break;
                }

            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == PLACE_PICKER_REQUEST) {
//            if (resultCode == Activity.RESULT_OK) {
////                Place place = PlacePicker.getPlace(data, getActivity());
////                String toastMsg = String.format("Place: %s", place.getLocale());
////                Toast.makeText(getActivity(), toastMsg, Toast.LENGTH_LONG).show();
//                Place place = PlaceAutocomplete.getPlace(getActivity(), data);
//                Log.i("location", "Place: " + place.getName());
//                LatLng latlon = place.getLatLng();
//                utils.setPreference(Constant.PREF_CUR_LAT, String.valueOf(latlon.latitude));
//                utils.setPreference(Constant.PREF_CUR_LON, String.valueOf(latlon.longitude));
//                utils.setPreference(Constant.PREF_CUR_LOCATION, String.valueOf(place.getName()));
//                txtLocation.setText(place.getName());
//            }
//
//        }
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == getActivity().RESULT_OK) {
                Place place = PlacePicker.getPlace(getActivity(),data);
                String toastMsg = String.format("Place: %s", place.getName());

                LatLng latlon = place.getLatLng();

                try {
                    Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                    List<Address> addresses = geocoder.getFromLocation(latlon.latitude, latlon.longitude, 10);
                    if (addresses != null && addresses.size() > 0) {
                        String city = null;
                        String area = null;
                        for(int i=0;i<addresses.size();i++){
                            if(city == null || area == null){
                                city = addresses.get(i).getLocality();
                                area = addresses.get(i).getSubLocality();
                            }
                        }
//                        Log.i("::", "City: " + addresses.get(0).getSubAdminArea());
//                        Log.i("::", "State: " + addresses.get(0).getAdminArea());
//                        Log.i("::", "Zip: " + addresses.get(0).getPostalCode());
//                        Log.i("::", "Area: " + addresses.get(0).getSubLocality());



                        utils.setPreference(Constant.PREF_CUR_LAT, String.valueOf(latlon.latitude));
                        utils.setPreference(Constant.PREF_CUR_LON, String.valueOf(latlon.longitude));
                        if(area == null){
                            utils.setPreference(Constant.PREF_CUR_LOCATION, String.valueOf(city));
                            txtLocation.setText(city);
                        }else{
                            utils.setPreference(Constant.PREF_CUR_LOCATION, String.valueOf(area + "," + city));
                            txtLocation.setText(area + "," + city);
                        }

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtEditLocation:
//                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
//                try {
//                    startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
//                } catch (GooglePlayServicesRepairableException e) {
//                    e.printStackTrace();
//                } catch (GooglePlayServicesNotAvailableException e) {
//                    e.printStackTrace();
//                }

//                AutocompleteFilter autocompleteFilter = new AutocompleteFilter.Builder()
//                        .setTypeFilter(Place.TYPE_LOCALITY)
//                        .setCountry("IN")
//                        .build();
//                Intent intent = null;
//                try {
//                    intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
//                            .setFilter(autocompleteFilter)
//                            //.setBoundsBias(new LatLngBounds(new LatLng(36.90731625763393, -86.51778523864743), new LatLng(37.02763411292923, -86.37183015289304)))
//                            .build(getActivity());
//                } catch (GooglePlayServicesRepairableException e) {
//                    e.printStackTrace();
//                } catch (GooglePlayServicesNotAvailableException e) {
//                    e.printStackTrace();
//                }
//                startActivityForResult(intent, PLACE_PICKER_REQUEST);
//
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

                try {
                    utils.startProgress();
                    startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }

                break;
            case R.id.edtSearch:
                ((MainActivity) getActivity()).setSearch("Search");
                break;
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        Log.d("frag:::", "pause");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("frag:::", "resume");
        utils.dismissProgress();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d("frag:::", "attach");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d("frag:::", "detach");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("frag:::", "destroy");
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
