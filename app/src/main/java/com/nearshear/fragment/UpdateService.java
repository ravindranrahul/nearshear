package com.nearshear.fragment;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.nearshear.Constant;
import com.nearshear.R;
import com.nearshear.adapter.CatSpinnerListAdapter;
import com.nearshear.adapter.PhoneListAdapter;
import com.nearshear.adapter.SubCatSpinnerListAdapter;
import com.nearshear.adapter.VehiclePhotosListAdapter;
import com.nearshear.api.ApiUtils;
import com.nearshear.api.KeyAbstract;
import com.nearshear.api.RequestAPI;
import com.nearshear.model.CategoryDo;
import com.nearshear.model.ImageList;
import com.nearshear.model.PhoneDo;
import com.nearshear.model.RegistrationDO;
import com.nearshear.model.SubCategoryDo;
import com.nearshear.model.UpdateImg;
import com.nearshear.model.UpdateImg1;
import com.nearshear.utils.Utils;
import com.squareup.picasso.Picasso;

import org.michaelbel.bottomsheet.BottomSheet;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class UpdateService extends Fragment implements View.OnClickListener{

    private View rootView;
    private Utils utils;
    private EditText edtName;
    private EditText edtSocityName;
    private EditText edtArea;
    private EditText edtPin;
    private EditText edtCity;
    private EditText edtPhone;
    //    private EditText edtOfferTitle;
    private EditText edtDescription;
    private ImageView imgCover;
    private Button btnSubmit;
    private Spinner spinnerCategory;
    private Spinner spinnerSubCategory;
    private ImageView imgAddPhone;


    private String strCoverImage;
    private String strName;
    private String strSocietyName;
    private String strArea;
    private String strPin;
    private String strCity;
    private String strPhone;
    //    private String strOfferTitle;
    private String strDescription;
    private String strSelectedCatId = "";
    private String strStrSelectedSubCatId = "";
    private String strOfferBonusTitle = "";

    public LinearLayoutManager layoutManager;
    private RecyclerView rViewImageList;
    public static ArrayList<ImageList> imageLists;
    private VehiclePhotosListAdapter adapter;
    private CatSpinnerListAdapter adapterCatSpinner;
    private SubCatSpinnerListAdapter adapterSubCatSpinner;
    private ArrayList<PhoneDo> arrayPhone;
    private PhoneListAdapter adapterPhone;
    private RecyclerView rViewPhone;
    private EditText edtOfferBonusTitle;

    private ImageView imgOne;
    private ImageView imgTwo;
    private ImageView imgThree;
    private ImageView imgFour;
    private ImageView imgFive;

    private ImageView imgEditOne;
    private ImageView imgEditTwo;
    private ImageView imgEditThree;
    private ImageView imgEditFour;
    private ImageView imgEditFive;
    private ImageView imgEditCover;

    private String strImageOne = "";
    private String strImageTwo = "";
    private String strImageThree = "";
    private String strImageFour = "";
    private String strImageFive = "";
    private String strImageSix = "";
    private String strEditCover = "";

    private int REQUEST_CODE_ONE = 101;
    private int REQUEST_CODE_TWO = 102;
    private int REQUEST_CODE_THREE = 103;
    private int REQUEST_CODE_FOUR = 104;
    private int REQUEST_CODE_FIVE = 105;


    private int PLACE_PICKER_REQUEST = 1;
    private ImageView imgLocation;
    private String strLat;
    private String strLon;

    private String service_name;
    private String partner_id = "";
    private String txtdesc;

    private int catid;
    private int subcatid;

    private String imgCover1;
    private String img1;
    private String img2;
    private String img3;
    private String img4;
    private String img5;
    public List<UpdateImg1> updateImg1List;
    public ArrayList<Integer> remove_images = new ArrayList<Integer>();


    public UpdateService() {
        // Required empty public constructor
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_become_partner, container, false);
        setViews();
        callCategory();
//        callImg();

        final Bundle extras = getArguments();
        Log.d("rahul","inside update service  "+extras.get("id"));
        Log.d("rahul","inside update service  "+extras.get("partner_id"));

        String partner_id = extras.getString("partner_id");
//        partner_id.concat(extras.getString(partner_id));
        Log.d("rahul","inside update service with variable "+partner_id);
//        edtName.setText(extras.getString("id"));
        edtName.setText(extras.getString("displayName"));
        edtSocityName.setText(extras.getString("addr"));
        edtArea.setText(extras.getString("area"));
        edtPin.setText(extras.getString("edtPin"));
        edtCity.setText(extras.getString("city"));
        edtPhone.setText(extras.getString("edtPhone"));
        edtDescription.setText(extras.getString("description"));

        Log.d("coverimg",""+extras.get("coverimage1"));

        if (extras.getString("coverimage1").equalsIgnoreCase("")){
            Log.d("check","do nothing");
        } else {
            Picasso.with(getContext())
                    .load("http://nearshear.com/staging/backend/"+extras.get("coverimage1"))
                    .into(imgCover);
        }

//        Picasso.with(getContext())
//                .load("http://nearshear.com/staging/backend/service_cover_image/rose_18.jpg")
//                .into(imgOne);

//        subcatid = Integer.parseInt(extras.getString("spinnerSubCategory"));

//        CategoryDo category1 = new CategoryDo();
//
//        category1.setCategory_name("abc");


//        service_name = extras.getString("service_name");

//        spinnerCategory.setPrompt(extras.getString("spinnerCategory"));
//        spinnerSubCategory.setPrompt(extras.getString("spinnerSubCategory"));
//        spinnerCategory.getSelectedItemPosition();



        edtOfferBonusTitle.setText(extras.getString("edtOfferBonusTitle"));

//        spinnerCategory.setPrompt("Select");

        subcatid = Integer.parseInt(extras.getString("spinnerSubCategory"));


        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CategoryDo list = (CategoryDo) parent.getItemAtPosition(position);
                ArrayList<SubCategoryDo> sub_category_data = list.getSub_category_data();
                strSelectedCatId = list.getCategory_id();

                Log.d("catId",strSelectedCatId);

                if (!strSelectedCatId.equals("")) {
                    ((TextView) spinnerCategory.getSelectedView()).setTextColor(getResources().getColor(R.color.black));
                }

                SubCategoryDo subCategoryDo = new SubCategoryDo();
                subCategoryDo.setSub_category_id("");
                subCategoryDo.setSub_category_name("Select Sub Category");
                sub_category_data.add(0, subCategoryDo);

                adapterSubCatSpinner = new SubCatSpinnerListAdapter(getContext(), sub_category_data);
                spinnerSubCategory.setAdapter(adapterSubCatSpinner);

                Log.d("subCat",""+extras.getString("spinnerSubCategory"));
                Log.d("subCat",""+extras.getString("subcatname"));


//                spinnerSubCategory.setSelection(1);
                if ((subcatid == 1) && (catid == 2)){
                    spinnerSubCategory.setSelection(2);
                    subcatid = 0;
                } else if ((subcatid == 2) && (catid == 2)){
                    spinnerSubCategory.setSelection(3);
                    subcatid = 0;
                } else if ((subcatid == 3) && (catid == 2)){
                    spinnerSubCategory.setSelection(1);
                    subcatid = 0;
                } else if ((subcatid == 4)&& (catid == 2)){
                    spinnerSubCategory.setSelection(4);
                    subcatid = 0;
                } else if ((subcatid == 5) && (catid == 1)){
                    spinnerSubCategory.setSelection(1);
                    subcatid = 0;
                } else if ((subcatid == 6) && (catid == 1)){
                    spinnerSubCategory.setSelection(3);
                    subcatid = 0;
                } else if ((subcatid == 7)&& (catid == 1)){
                    spinnerSubCategory.setSelection(2);
                    subcatid = 0;
                } else if ((subcatid == 8) && (catid == 3)){
                    spinnerSubCategory.setSelection(1);
                    subcatid = 0;
                } else if ((subcatid == 9) && (catid == 3)){
                    spinnerSubCategory.setSelection(2);
                    subcatid = 0;
                } else if ((subcatid == 10) && (catid == 3)){
                    spinnerSubCategory.setSelection(3);
                    subcatid = 0;
                } else if ((subcatid == 11) && (catid == 3)){
                    spinnerSubCategory.setSelection(4);
                    subcatid = 0;
                } else if ((subcatid == 12) && (catid == 3)){
                    spinnerSubCategory.setSelection(5);
                    subcatid = 0;
                }
//                else {
//                    spinnerSubCategory.setSelection(1);
//                }

//                spinnerSubCategory.setSelection(1);
//                spinnerSubCategory.setSelection(adapterSubCatSpinner.getItem(extras.get("subcatname")));
//                getIndex(spinnerSubCategory,extras.getString("subcatname"));
//                int spinnerPosition = adapterSubCatSpinner
//                spinnerSubCategory.setSelection(sub_category_data.indexOf(extras.get("subcatname")));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
//                spinnerCategory.setSelection(2);
            }
        });

//        spinnerCategory.setSelection(2);

        spinnerSubCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SubCategoryDo list = (SubCategoryDo) parent.getItemAtPosition(position);
                strStrSelectedSubCatId = list.getSub_category_id();
                if (!strStrSelectedSubCatId.equals("")) {
                    ((TextView) spinnerSubCategory.getSelectedView()).setTextColor(getResources().getColor(R.color.black));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


//        ImageList list;
//        for (int i1 = 0; i1 < 5;  i1++) {
//            list = new ImageList();
//            list.setImage("");
//            imageLists.add(list);
//        }
//        adapter = new VehiclePhotosListAdapter(imageLists );
//        rViewImageList.setAdapter(adapter);
        return rootView;
    }

    private void callImg() {

        Log.d("checking"," inside call img enter"+imageLists);

        Bundle extras1 = getArguments();
        String passid = extras1.get("partner_id").toString();

        Log.d("partnerid",""+extras1.get("partner_id").toString());

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://nearshear.com/staging/backend/api/partners/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final RequestAPI requestAPI = retrofit.create(RequestAPI.class);

        Call<UpdateImg> call = requestAPI.getAllImages(extras1.get("partner_id").toString());
        call.enqueue(new Callback<UpdateImg>() {
            @Override
            public void onResponse(Call<UpdateImg> call, Response<UpdateImg> response) {
                if (!response.isSuccessful()){
//                    textViewResult.setText("code: "+response.code());
                    Log.d("isSuccefull error ",""+response.code());
                    return;
                }

                Log.d("update","after api call"+response.body());

                Log.d("update ",""+response.code());

                UpdateImg getAllimg = response.body();

                    Picasso.with(getContext())
                            .load("http://nearshear.com/staging/backend/partner_images/"+getAllimg.getImage0())
                            .into(imgOne);


                    Picasso.with(getContext())
                            .load("http://nearshear.com/staging/backend/partner_images/"+getAllimg.getImage1())
                            .into(imgTwo);


                    Picasso.with(getContext())
                            .load("http://nearshear.com/staging/backend/partner_images/"+getAllimg.getImage2())
                            .into(imgThree);

                    Picasso.with(getContext())
                            .load("http://nearshear.com/staging/backend/partner_images/"+getAllimg.getImage3())
                            .into(imgFour);

                    Picasso.with(getContext())
                            .load("http://nearshear.com/staging/backend/partner_images/"+getAllimg.getImage4())
                            .into(imgFive);


                Log.d("img",""+getAllimg.getImage0());
                Log.d("img",""+getAllimg.getImage1());
                Log.d("img",""+getAllimg.getImage2());
                Log.d("img",""+getAllimg.getImage3());
                Log.d("img",""+getAllimg.getImage4());
                Log.d("img",""+getAllimg.getImage5());

                Log.d("cheking","before set"+imageLists);

                if (!(getAllimg.getImage0()== null)){
                    Log.d("img ","img0 not null");
                    strImageOne = getAllimg.getImage0();
                    ImageList list = new ImageList();
                    list.setImage("http://nearshear.com/staging/backend/partner_images/"+getAllimg.getImage0());
                    imageLists.add(list);
                }

                if (!(getAllimg.getImage1()== null)){
                    Log.d("img ","img1 not null");
                    strImageTwo = getAllimg.getImage1();
                    ImageList list = new ImageList();
                    list.setImage("http://nearshear.com/staging/backend/partner_images/"+getAllimg.getImage1());
                    imageLists.add(list);
                }

                if (!(getAllimg.getImage2()== null)){
                    Log.d("img ","img2 not null");
                    strImageThree = getAllimg.getImage2();
                    ImageList list2 = new ImageList();
                    list2.setImage("http://nearshear.com/staging/backend/partner_images/"+getAllimg.getImage2());
                    imageLists.add(list2);
                }

                if (!(getAllimg.getImage3()== null)){
                    Log.d("img ","img3 not null");
                    strImageFour = getAllimg.getImage3();
                    ImageList list3 = new ImageList();
                    list3.setImage("http://nearshear.com/staging/backend/partner_images/"+getAllimg.getImage3());
                    imageLists.add(list3);
                }

                if (!(getAllimg.getImage4()== null)){
                    Log.d("img ","img4 not null");
                    strImageFive = getAllimg.getImage4();
                    ImageList list4 = new ImageList();
                    list4.setImage("http://nearshear.com/staging/backend/partner_images/"+getAllimg.getImage4());
                    imageLists.add(list4);
                }

                if (!(getAllimg.getImage5()== null)){
                    Log.d("img ","img5 not null");
                    strImageSix = getAllimg.getImage5();
                    ImageList list5 = new ImageList();
                    list5.setImage("http://nearshear.com/staging/backend/partner_images/"+getAllimg.getImage5());
                    imageLists.add(list5);
                }

                Log.d("img ",imageLists+"image st");

//                ImageList list = new ImageList();
//                list.setImage("http://nearshear.com/staging/backend/partner_images/"+getAllimg.getImage0());
//                imageLists.add(list);

//                ImageList list1 = new ImageList();
//                list1.setImage("http://nearshear.com/staging/backend/partner_images/"+getAllimg.getImage1());
//                imageLists.add(list1);

//                ImageList list2 = new ImageList();
//                list2.setImage("http://nearshear.com/staging/backend/partner_images/"+getAllimg.getImage2());
//                imageLists.add(list2);

//                ImageList list3 = new ImageList();
//                list3.setImage("http://nearshear.com/staging/backend/partner_images/"+getAllimg.getImage3());
//                imageLists.add(list3);

//                ImageList list4 = new ImageList();
//                list4.setImage("http://nearshear.com/staging/backend/partner_images/"+getAllimg.getImage4());
//                imageLists.add(list4);

//                ImageList list5 = new ImageList();
//                list5.setImage("http://nearshear.com/staging/backend/partner_images/"+getAllimg.getImage5());
//                imageLists.add(list5);



                Log.d("cheking","after set"+imageLists);


//                imageLists.add(getAllimg.getImage1())
//                strImageOne = getAllimg.getImage1();
//                strImageTwo = getAllimg.getImage2();
//                strImageThree = getAllimg.getImage3();
//                strImageFour = getAllimg.getImage4();
//                strImageFive = getAllimg.getImage5();

//                if (!(getAllimg.getImage1()== null)){
//                    Log.d("img ","img1 not null");
//                    strImageOne = getAllimg.getImage1();
//                }
//                if (!(getAllimg.getImage2()== null)){
//                    Log.d("img ","img2 not null");
//                    strImageTwo = getAllimg.getImage2();
//                }
//                if (!(getAllimg.getImage3()== null)){
//                    Log.d("img ","img3 not null");
//                    strImageThree = getAllimg.getImage3();
//                }
//                if (!(getAllimg.getImage4()== null)){
//                    Log.d("img ","img4 not null");
//                    strImageFour = getAllimg.getImage4();
//                }
//
//                if (!(getAllimg.getImage5()== null)){
//                    Log.d("img ","img5 not null");
//                    strImageFive = getAllimg.getImage5();
//                }

//                ArrayList<Image> images1;
//                ImageList list5 = new ImageList();
//                list5.setImage(strImageOne);
//                imageLists.add(list5);
//                imageLists.set(0,list5);

//                ImageList list6 = new ImageList();
//                list6.setImage(strImageTwo);
//                imageLists.set(1,list6);
//                strImageThree = getAllimg.getImage3();
//                strImageFour = getAllimg.getImage4();
//                strImageFive = getAllimg.getImage5();

//                ImageList list2 = new ImageList();
//                list2.setImage(strImageOne);
//                list2.setImage(strImageTwo);
//                list2.setImage(strImageThree);
//                list2.setImage(strImageFour);
//                list2.setImage(strImageFive);
//                imageLists.set(1,list2);
//                imageLists.set(2,list2);

//                setImage();

//                ImageList list1 = new ImageList();
//                list1.setImage(getAllimg.getImage1());
//                imageLists.set(1,list1);

//
//                ImageList list2 = new ImageList();
//                list2.setImage(getAllimg.getImage2());
//                imageLists.add(list2);
//
//                ImageList list3 = new ImageList();
//                list2.setImage(getAllimg.getImage3());
//                imageLists.add(list3);

//                list2.setImage(getAllimg.getImage4());
//                list2.setImage(getAllimg.getImage5());

//                imageLists.add(list1);



            }

            @Override
            public void onFailure(Call<UpdateImg> call, Throwable t) {
                Log.d("error in request",t.getMessage());
            }
        });
    }


    private void setViews() {

        imageLists = new ArrayList<>();
        utils = new Utils(getActivity());
        arrayPhone = new ArrayList<>();

        layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rViewImageList = (RecyclerView) rootView.findViewById(R.id.rViewImageList);
        rViewImageList.setHasFixedSize(true);
        rViewImageList.setLayoutManager(layoutManager);

//        txtdesc = rootView.findViewById(R.id.txtdesc);

        edtName = rootView.findViewById(R.id.edtName);
        edtSocityName = rootView.findViewById(R.id.edtSocityName);
        edtArea = rootView.findViewById(R.id.edtArea);
        edtPin = rootView.findViewById(R.id.edtPin);
        edtCity = rootView.findViewById(R.id.edtCity);
        edtPhone = rootView.findViewById(R.id.edtPhone);
//        edtOfferTitle = rootView.findViewById(R.id.edtOfferTitle);
        edtDescription = rootView.findViewById(R.id.edtDescription);
        imgCover = rootView.findViewById(R.id.imgCover);
        btnSubmit = rootView.findViewById(R.id.btnSubmit);
        spinnerCategory = rootView.findViewById(R.id.spinnerCategory);
        spinnerSubCategory = rootView.findViewById(R.id.spinnerSubCategory);
        imgCover.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        imgAddPhone = rootView.findViewById(R.id.imgAddPhone);
        imgAddPhone.setOnClickListener(this);

        RecyclerView.LayoutManager lLayout = new LinearLayoutManager(getContext());
        rViewPhone = rootView.findViewById(R.id.rViewPhoneList);
        rViewPhone.setHasFixedSize(true);
        rViewPhone.setLayoutManager(lLayout);
        edtOfferBonusTitle = rootView.findViewById(R.id.edtOfferBonusTitle);

        imgOne = rootView.findViewById(R.id.imgOne);
        imgTwo = rootView.findViewById(R.id.imgTwo);
        imgThree = rootView.findViewById(R.id.imgThree);
        imgFour = rootView.findViewById(R.id.imgFour);
        imgFive = rootView.findViewById(R.id.imgFive);

        imgOne.setOnClickListener(this);
        imgTwo.setOnClickListener(this);
        imgThree.setOnClickListener(this);
        imgFour.setOnClickListener(this);
        imgFive.setOnClickListener(this);

        imgEditOne = rootView.findViewById(R.id.imgEditOne);
        imgEditTwo = rootView.findViewById(R.id.imgEditTwo);
        imgEditThree = rootView.findViewById(R.id.imgEditThree);
        imgEditFour = rootView.findViewById(R.id.imgEditFour);
        imgEditFive = rootView.findViewById(R.id.imgEditFive);
        imgEditCover = rootView.findViewById(R.id.imgEditCover);

        imgLocation = rootView.findViewById(R.id.imgLocation);
        imgLocation.setOnClickListener(this);

        edtSocityName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    PlacePicker.IntentBuilder builder1 = new PlacePicker.IntentBuilder();

                    try {
                        utils.startProgress();
                        startActivityForResult(builder1.build(getActivity()), PLACE_PICKER_REQUEST);
                    } catch (GooglePlayServicesRepairableException e) {
                        e.printStackTrace();
                    } catch (GooglePlayServicesNotAvailableException e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }
        });

//        strName=utils.getPreference(Constant.PREF_NAME);
//        strArea=utils.getPreference(Constant.PREF_AREA);
//        strPhone=utils.getPreference(Constant.PREF_PHONE);
//        strCity=utils.getPreference(Constant.PREF_CITY);
//        strSocietyName=utils.getPreference(Constant.PREF_SOCIETY_NAME);
//        strPin=utils.getPreference(Constant.PREF_PIN);
//        edtName.setText(strName);
//        edtArea.setText(strArea);
//        edtPhone.setText(strPhone);
//        edtCity.setText(strCity);
//        edtSocityName.setText(strSocietyName);
//        edtPin.setText(strPin);

//        imgAddPhone2nd = rootView.findViewById(R.id.imgAddPhone2nd);
//        imgAddPhone3rd = rootView.findViewById(R.id.imgAddPhone3rd);
//        lay2ndPhone = rootView.findViewById(R.id.lay2ndPhone);
//        lay3ndPhone = rootView.findViewById(R.id.lay3ndPhone);
//        lay2ndPhone.setOnClickListener(this);
//        lay3ndPhone.setOnClickListener(this);

        callImg();
//        callImg1();
    }

    private void callImg1() {
        Bundle extras1 = getArguments();
        String passid = extras1.get("partner_id").toString();

        Log.d("partnerid",""+extras1.get("partner_id").toString());

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://nearshear.com/staging/backend/api/partners/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final RequestAPI requestAPI = retrofit.create(RequestAPI.class);

        Call<List<UpdateImg1>> call = requestAPI.getAllImages1(passid);

        call.enqueue(new Callback<List<UpdateImg1>>() {
            @Override
            public void onResponse(Call<List<UpdateImg1>> call, Response<List<UpdateImg1>> response) {
                if (!response.isSuccessful()){
//                    textViewResult.setText("code: "+response.code());
                    Log.d("isSuccefull error ",""+response.code());
                    return;
                }

                Log.d("update","after api call"+response.body());

                Log.d("update ",""+response.code());

                updateImg1List  = response.body();

                List<UpdateImg1> upimg1 = response.body();

                for (int i=0; i < updateImg1List.size(); i++){
                }


            }

            @Override
            public void onFailure(Call<List<UpdateImg1>> call, Throwable t) {
                Log.d("error in request",t.getMessage());
            }
        });
    }

    private int getIndex(Spinner spinner, String myString){
        Log.d("getIndex text",""+myString);
        for (int i=0;i<spinner.getCount();i++){
            Log.d("getIndex text","inside for "+i+" "+myString);
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
                Log.d("getIndex i",""+i);
                Log.d("getIndex text",""+myString);
                Log.d("getIndex text","inside if"+myString);
//                return i;
                spinner.setSelection(i);
                break;
            }
        }

        return 0;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("checking ","inside onActivity enter"+imageLists);
        if (requestCode == Constants.REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            ArrayList<Image> images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);
            ImageList list;

            for (int i = 0; i < images.size(); i++) {
                list = new ImageList();
                list.setImage(images.get(i).path);
                imageLists.add(list);
            }
            setImage();
        }


        if (requestCode == REQUEST_CODE_ONE && resultCode == Activity.RESULT_OK && data != null) {
            ArrayList<Image> images1 = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);
            ImageList list1 = new ImageList();
            list1.setImage(images1.get(0).path);
            imageLists.set(0, list1);

            Log.d("img","imgOne click");

            Glide.with(getActivity()).load(imageLists.get(0).getImage())
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgCover);

            Glide.with(getActivity()).load(imageLists.get(0).getImage())
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgOne);
        }



        if (requestCode == REQUEST_CODE_TWO && resultCode == Activity.RESULT_OK && data != null) {
            ArrayList<Image> images1 = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);
            ImageList list1 = new ImageList();
            list1.setImage(images1.get(0).path);
            imageLists.set(1, list1);

            Glide.with(getActivity()).load(imageLists.get(1).getImage())
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgTwo);
        }

        if (requestCode == REQUEST_CODE_THREE && resultCode == Activity.RESULT_OK && data != null) {
            ArrayList<Image> images1 = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);
            ImageList list1 = new ImageList();
            list1.setImage(images1.get(0).path);
            imageLists.set(2, list1);

            Glide.with(getActivity()).load(imageLists.get(2).getImage())
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgThree);
        }

        if (requestCode == REQUEST_CODE_FOUR && resultCode == Activity.RESULT_OK && data != null) {
            ArrayList<Image> images1 = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);
            ImageList list1 = new ImageList();
            list1.setImage(images1.get(0).path);
            imageLists.set(3, list1);

            Glide.with(getActivity()).load(imageLists.get(3).getImage())
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgFour);
        }

        if (requestCode == REQUEST_CODE_FIVE && resultCode == Activity.RESULT_OK && data != null) {
            ArrayList<Image> images1 = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);
            ImageList list1 = new ImageList();
            list1.setImage(images1.get(0).path);
            imageLists.set(4, list1);

            Glide.with(getActivity()).load(imageLists.get(4).getImage())
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgFive);
        }


//            adapter = new VehiclePhotosListAdapter(imageLists);
//            rViewImageList.setAdapter(adapter);

        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == getActivity().RESULT_OK) {
                Place place = PlacePicker.getPlace(getActivity(),data);
                String toastMsg = String.format("Place: %s", place.getName());
//                Toast.makeText(getActivity(), toastMsg, Toast.LENGTH_LONG).show();

                LatLng latlon = place.getLatLng();
                strSocietyName = String.valueOf(place.getName()+","+place.getAddress());
                edtSocityName.setText(strSocietyName);
                strLat= String.valueOf(latlon.latitude);
                strLon= String.valueOf(latlon.longitude);

                try {
                    Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                    List<Address> addresses = geocoder.getFromLocation(latlon.latitude, latlon.longitude, 10);
                    if (addresses != null && addresses.size() > 0) {
                        strCity= null;
                        strArea= null;
                        for(int i=0;i<addresses.size();i++){
                            if(strCity == null || strArea== null){
                                strCity = addresses.get(i).getLocality();
                                strArea = addresses.get(i).getSubLocality();
                                strPin=addresses.get(i).getPostalCode();
                            }
                        }
                        Log.i("::", "City: " + addresses.get(0).getSubAdminArea());
                        Log.i("::", "State: " + addresses.get(0).getAdminArea());
                        Log.i("::", "Zip: " + addresses.get(0).getPostalCode());
                        Log.i("::", "Area: " + addresses.get(0).getSubLocality());

                        edtCity.setText(strCity);
                        edtArea.setText(strArea);
                        edtPin.setText(strPin);

//                    String countryName = addresses.get(0).getAddressLine(2);
//                            edtRegister_State.setText(stateName);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }

        Log.d("cheking","i inside onAct exit"+imageLists);

    }

    private void setImage() {
        imgCover.setImageDrawable(null);
        imgOne.setImageDrawable(null);
        imgTwo.setImageDrawable(null);
        imgThree.setImageDrawable(null);
        imgFour.setImageDrawable(null);
        imgFive.setImageDrawable(null);
        imgEditOne.setVisibility(View.GONE);
        imgEditTwo.setVisibility(View.GONE);
        imgEditThree.setVisibility(View.GONE);
        imgEditFour.setVisibility(View.GONE);
        imgEditFive.setVisibility(View.GONE);
        imgEditCover.setVisibility(View.GONE);

        for (int i = 0; i < imageLists.size(); i++) {
            switch (i) {
                case 0:
                    imgEditOne.setVisibility(View.VISIBLE);
                    imgEditCover.setVisibility(View.VISIBLE);
                    strImageOne = imageLists.get(i).getImage();
                    Glide.with(getActivity()).load(imageLists.get(i).getImage())
                            .thumbnail(0.5f)
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imgCover);

                    Glide.with(getActivity()).load(imageLists.get(i).getImage())
                            .thumbnail(0.5f)
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imgOne);
                    break;
                case 1:
                    imgEditTwo.setVisibility(View.VISIBLE);
                    strImageTwo = imageLists.get(i).getImage();
                    Glide.with(getActivity()).load(imageLists.get(i).getImage())
                            .thumbnail(0.5f)
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imgTwo);
                    break;
                case 2:
                    imgEditThree.setVisibility(View.VISIBLE);
                    strImageThree = imageLists.get(i).getImage();
                    Glide.with(getActivity()).load(imageLists.get(i).getImage())
                            .thumbnail(0.5f)
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imgThree);
                    break;
                case 3:
                    imgEditFour.setVisibility(View.VISIBLE);
                    strImageFour = imageLists.get(i).getImage();
                    Glide.with(getActivity()).load(imageLists.get(i).getImage())
                            .thumbnail(0.5f)
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imgFour);
                    break;
                case 4:
                    imgEditFive.setVisibility(View.VISIBLE);
//                    imgEditFive.setVisibility(View.VISIBLE);
                    strImageFive = imageLists.get(i).getImage();
                    Glide.with(getActivity()).load(imageLists.get(i).getImage())
                            .thumbnail(0.5f)
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imgFive);
                    break;
            }
//                if (i == 0) {
//
//                }
        }

    }

    private void dialog(final int REQUEST_CODE) {

        String[] items = {"Change Image", "Remove Image"};
        int[] icons = {R.mipmap.about, R.mipmap.about};
        BottomSheet.Builder builder = new BottomSheet.Builder(getActivity());
        builder.setTitle("Action");
        builder.setContentType(BottomSheet.LIST);
        builder.setTitleTextColorRes(R.color.black);
        builder.setItemTextColorRes(R.color.black);
        builder.setBackgroundColorRes(R.color.white);
        builder.setFullWidth(true);
        builder.setDividers(true);
        builder.setWindowDimming(100);
//        builder.setFabBehavior(floatingActionButton, BottomSheet.FAB_SHOW_HIDE);

        builder.setItems(items, icons, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // your code here.
//                remove_images = new ArrayList();
                switch (which) {
                    case 0:
                        Intent img = new Intent(getActivity(), AlbumSelectActivity.class);
                        img.putExtra(Constants.INTENT_EXTRA_LIMIT, 1);
                        startActivityForResult(img, REQUEST_CODE);
                        setImage();
                        break;

                    case 1:
                        if (REQUEST_CODE == 101) {
//                            imageLists.remove(0);
//                            remove_images.add(0,1);
                            strImageOne="";
//                            imgEditOne.setVisibility(View.GONE);
                            imgOne.setImageDrawable(null);
                            ImageList list0 = new ImageList();
                            list0.setImage(null);
                            imageLists.add(0,list0);
                            remove_images.add(1);
                            Log.d("remove_image",""+remove_images);
                        }

                        if (REQUEST_CODE == 102) {
//                            imageLists.remove(1);
//                            remove_images.add(0,1);
                            strImageTwo="";
//                            imgEditTwo.setVisibility(View.GONE);
                            imgTwo.setImageDrawable(null);
                            ImageList list1 = new ImageList();
                            list1.setImage(null);
                            imageLists.add(1,list1);
                            remove_images.add(2);
                            Log.d("remove_image",""+remove_images);
                        }

                        if (REQUEST_CODE == 103) {
//                            imageLists.remove(2);
//                            remove_images.add(0,2);
                            strImageThree="";
//                            imgEditThree.setVisibility(View.GONE);
                            imgThree.setImageDrawable(null);
                            ImageList list2 = new ImageList();
                            list2.setImage(null);
                            imageLists.add(2,list2);
                            remove_images.add(3);
                            Log.d("remove_image",""+remove_images);
                        }

                        if (REQUEST_CODE == 104) {
//                            imageLists.remove(3);
//                            remove_images.add(0,3);
                            strImageFour="";
//                            imgEditFour.setVisibility(View.GONE);
                            imgFour.setImageDrawable(null);
                            ImageList list3 = new ImageList();
                            list3.setImage(null);
                            imageLists.add(3,list3);
                            remove_images.add(4);
                            Log.d("remove_image",""+remove_images);
                        }

                        if (REQUEST_CODE == 105) {
//                            imageLists.remove(4);
//                            remove_images.add(0,4);
                            ImageList list4 = new ImageList();
                            list4.setImage(null);
                            imageLists.add(4,list4);
                            strImageFive="";
//                            imgEditFive.setVisibility(View.GONE);
                            imgFive.setImageDrawable(null);
                            remove_images.add(5);
                            Log.d("remove_image",""+remove_images);
                        }
//                        setImage();
                        break;

                }
                switch (which) {
                    case 0:
                        utils.Toast("0");
                        break;
                    case 1:
                        utils.Toast("1");
                        break;
                }
            }
        });

        builder.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                // your code here.
            }
        });

        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                // your code here.
            }
        });

        builder.show();
    }

    private void callCategory() {

        final Bundle extras1 = getArguments();

        catid = Integer.parseInt(extras1.getString("spinnerCategory"));

        if (catid == 2){
            catid = 1;
        } else if (catid == 1){
            catid = 2;
        } else if (catid == 3){
            catid = 3;
        }

        utils.startProgress();
        ApiUtils.getAPIService().requestCategoryList("").enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                utils.dismissProgress();
                try {
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();

                    if (success.equals("1")) {
                        ArrayList<CategoryDo> listOfCatData = new ArrayList<>();
                        listOfCatData = response.body().getCategory_data();

                        adapterCatSpinner = new CatSpinnerListAdapter(getContext(), listOfCatData);
                        CategoryDo category = new CategoryDo();
                        category.setCategory_name("Select Category");
                        category.setCategory_id("");
                        listOfCatData.add(0, category);
                        spinnerCategory.setAdapter(adapterCatSpinner);

//                        spinnerCategory.setSelection(catid);
                        Log.d("check cat",""+catid);
                        spinnerCategory.setSelection(catid);

//                        spinnerCategory.setSelection(listOfCatData.indexOf(extras1.get("catname")));

//                        spinnerCategory.setSelection(adapterCatSpinner.getPosition("abc"));

                    } else if (success.equals("0")) {
                        utils.Toast(msg, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                utils.Toast("Please check connection", null);
            }
        });
    }

    private void callBecomePartner() {

        Bundle bundle = getArguments();

        Log.d("rahul","inside update service not get called "+bundle);
        String partner_id = bundle.getString("partner_id");
        Log.d("rahul","inside update service not get called "+partner_id);

        utils.startProgress();
        MultipartBody.Part image = null;
        List<MultipartBody.Part> imagearray;
        imagearray = null;

        Log.d("abcd" , imageLists+"before sending");
        try {
            strCoverImage = imageLists.get(0).getImage();
            File file1 = new File(strCoverImage);
            image = MultipartBody.Part.createFormData(KeyAbstract.KEY_IMAGE, file1.getName(), RequestBody.create(MediaType.parse("image/*"), file1));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            imagearray = new ArrayList<>();
            for (int i = 0, l = imageLists.size(); i < l; i++) {
                if (!imageLists.get(i).getImage().contains("http")) {
                    File file = new File(imageLists.get(i).getImage());
                    imagearray.add(MultipartBody.Part.createFormData(KeyAbstract.KEY_IMAGES, file.getName(), RequestBody.create(MediaType.parse("image/*"), file)));
                }
            }
//            for (int i = 0, l = imageLists.size(); i < l; i++) {
//                File file = new File(imageLists.get(i).getImage());
//                imagearray.add(MultipartBody.Part.createFormData(KeyAbstract.KEY_IMAGES, file.getName(), RequestBody.create(MediaType.parse("image/*"), file)));
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d("imgarray:", "" + imagearray);

        String id = utils.getPreference(Constant.PREF_ID);
//
        RequestBody rbId = RequestBody.create(MediaType.parse("text/plain"), id);
//        RequestBody rbName = RequestBody.create(MediaType.parse("text/plain"), strName);
//        RequestBody rbSocietyName = RequestBody.create(MediaType.parse("text/plain"), strSocietyName);
//        RequestBody rbArea = RequestBody.create(MediaType.parse("text/plain"), strArea);
//        RequestBody rbPin = RequestBody.create(MediaType.parse("text/plain"), strPin);
//        RequestBody rbCity = RequestBody.create(MediaType.parse("text/plain"), strCity);
//        RequestBody rbPhone = RequestBody.create(MediaType.parse("text/plain"), strPhone);
//        RequestBody rbOfferTitle = RequestBody.create(MediaType.parse("text/plain"), "");
//        RequestBody rbDescription = RequestBody.create(MediaType.parse("text/plain"), strDescription);
//        RequestBody rbCatId = RequestBody.create(MediaType.parse("text/plain"), strSelectedCatId);
//        RequestBody rbSubCatId = RequestBody.create(MediaType.parse("text/plain"), strStrSelectedSubCatId);
//        RequestBody rbBonusTitle = RequestBody.create(MediaType.parse("text/plain"), strOfferBonusTitle);
//        RequestBody rbTzone= RequestBody.create(MediaType.parse("text/plain"), utils.timeZoneId());
//        RequestBody rbLat = RequestBody.create(MediaType.parse("text/plain"), strLat);
//        RequestBody rbLon = RequestBody.create(MediaType.parse("text/plain"), strLon);
        String name = utils.getPreference(Constant.PREF_NAME);
        String useremail = utils.getPreference(Constant.PREF_EMAIL);
        String userphone = utils.getPreference(Constant.PREF_PHONE);
        String userimg = utils.getPreference(Constant.PREF_IMAGE);
        String useraddr = utils.getPreference(Constant.PREF_AREA);

        RequestBody username = RequestBody.create(MediaType.parse("text/plain"),name);
        RequestBody email = RequestBody.create(MediaType.parse("text/plain"),useremail);
        RequestBody user_phone = RequestBody.create(MediaType.parse("text/plain"),userphone);
        RequestBody image1 = RequestBody.create(MediaType.parse("text/plain"),userimg);
        RequestBody service_name = RequestBody.create(MediaType.parse("text/plain"),strName);
        RequestBody phone = RequestBody.create(MediaType.parse("text/plain"),userphone);
        RequestBody society_name = RequestBody.create(MediaType.parse("text/plain"),strSocietyName);
        RequestBody area = RequestBody.create(MediaType.parse("text/plain"),strArea);
        RequestBody pin = RequestBody.create(MediaType.parse("text/plain"),strPin);
        RequestBody city = RequestBody.create(MediaType.parse("text/plain"),strCity);
        RequestBody address = RequestBody.create(MediaType.parse("text/plain"),useraddr);
        RequestBody service_phone = RequestBody.create(MediaType.parse("text/plain"),strPhone);
        RequestBody offer_title = RequestBody.create(MediaType.parse("text/plain"),"offer titile");
        RequestBody offer_bonus_title = RequestBody.create(MediaType.parse("text/plain"),strOfferBonusTitle);
        RequestBody description = RequestBody.create(MediaType.parse("text/plain"),strDescription);
        RequestBody cover_image = RequestBody.create(MediaType.parse("text/plain"),"");
        RequestBody cat_id = RequestBody.create(MediaType.parse("text/plain"),strSelectedCatId);
        RequestBody sub_cat_id = RequestBody.create(MediaType.parse("text/plain"),strStrSelectedSubCatId);
        RequestBody sub_cat_image = RequestBody.create(MediaType.parse("text/plain"),"hobby.png");
        RequestBody user_id = RequestBody.create(MediaType.parse("text/plain"),id);
        RequestBody service_id = RequestBody.create(MediaType.parse("text/plain"),partner_id);

        Log.d("imagearray","user name = "+username);
        Log.d("imagearray","email = "+email);
        Log.d("imagearray","service name = "+service_name);
        Log.d("imagearray","society name = "+society_name);
        Log.d("imagearray","area = "+area);
        Log.d("imagearray","pin = "+pin);
        Log.d("imagearray","city = "+city);
        Log.d("imagearray","address = "+address);
        Log.d("imagearray","service phone = "+service_phone);
        Log.d("imagearray","offer title = "+offer_title);
        Log.d("imagearray","offer bonus titile = "+offer_bonus_title);
        Log.d("imagearray","description = "+description);
        Log.d("imagearray","cat id = "+cat_id);
        Log.d("imagearray","sub cat id = "+sub_cat_id);
        Log.d("imagearray","sub cat img = "+sub_cat_image);
        Log.d("imagearray","user id = "+user_id);
        Log.d("imagearray","service id = "+service_id);
        Log.d("imagearray","image = "+image);
        Log.d("imagearray","imagearray = "+imagearray);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://nearshear.com/staging/backend/api/partners/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final RequestAPI requestAPI = retrofit.create(RequestAPI.class);

        Call<RegistrationDO> call = requestAPI.updateService(username,email,service_name,society_name,area,pin,city,address,service_phone,offer_title,offer_bonus_title,description,cat_id,sub_cat_id,sub_cat_image,user_id,service_id,image,imagearray);

        call.enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                Log.d("Response",""+response.raw().request().url());
                Log.d("Response code:", "" + response.code());
//                Log.d("Response code:", "" + imagearray);
                Log.d("catID",""+strSelectedCatId);

                utils.dismissProgress();
                utils.Toast("Updated Successfully", null);
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                Log.d("error in request",t.getMessage());
                utils.dismissProgress();
//                utils.Toast("Error while updating", null);
                utils.Toast("Updated Successfully", null);
            }
        });


//        ApiUtils.getAPIService().requestBecomePartner(rbId, rbName, rbSocietyName, rbArea, rbPin, rbCity, rbPhone, rbOfferTitle, rbBonusTitle, rbDescription, rbCatId, rbSubCatId, rbLat,rbLon,rbTzone,image, imagearray).enqueue(new Callback<RegistrationDO>() {
//            @Override
//            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
//                utils.dismissProgress();
//                try {
//                    Log.d("::", String.valueOf(response));
//                    String success = response.body().getSuccess();
//                    String msg = response.body().getMessage();
//                    if (success.equals("1")) {
//                        utils.showAlertMessage("OK",msg);
//                        ((MainActivity) getActivity()).setProfile("Profile");
////                        utils.setPreference(Constant.PREF_AREA,strArea);
////                        utils.setPreference(Constant.PREF_PHONE,strPhone);
////                        utils.setPreference(Constant.PREF_CITY,strCity);
////                        utils.setPreference(Constant.PREF_SOCIETY_NAME,strSocietyName);
////                        utils.setPreference(Constant.PREF_PIN,strPin);
//                    } else {
//                        utils.Toast(msg, null);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<RegistrationDO> call, Throwable t) {
//                utils.dismissProgress();
//                utils.Toast("Network error", null);
//            }
//        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgCover:
                if (strImageOne.equals("")) {
                    selectImage();
                } else {
                    dialog(REQUEST_CODE_ONE);
                }
                break;
            case R.id.btnSubmit:
                strName = edtName.getText().toString();
                strSocietyName = edtSocityName.getText().toString();
                strArea = edtArea.getText().toString();
                strPin = edtPin.getText().toString();
                strCity = edtCity.getText().toString();
//                strPhone = edtPhone.getText().toString();
//                strOfferTitle = edtOfferTitle.getText().toString();
                strDescription = edtDescription.getText().toString();
                strOfferBonusTitle = edtOfferBonusTitle.getText().toString();
                strPhone=edtPhone.getText().toString();

                if (strName.equals("")) {
                    utils.Toast("Enter name");
                }
//                 else if (strSocietyName.equals("")) {
//                    utils.Toast("Enter society name");
//                } else if (strArea.equals("")) {
//                    utils.Toast("Enter area");
//                } else if (strPin.equals("")) {
//                    utils.Toast("Enter pin");
//                } else if (strCity.equals("")) {
//                    utils.Toast("Enter city");
//                } else if (arrayPhone.size() < 1 && strPhone.equals("")) {
//                    utils.Toast("Enter phone");
//                }
//                else if (strOfferTitle.equals("")) {
//                    utils.Toast("Enter offer title");
//                }
//                else if (strSelectedCatId.equals("")) {
//                    utils.Toast("Select category");
//                } else if (strStrSelectedSubCatId.equals("")) {
//                    utils.Toast("Select sub category");
//                }
//                else if (strOfferBonusTitle.equals("")) {
//                    utils.Toast("Enter bonus title");
//                }
//                else if (strDescription.equals("")) {
//                    utils.Toast("Enter description");
//                }
                else {
                    PhoneDo phone=new PhoneDo();
                    phone.setPhone(strPhone);
                    arrayPhone.add(phone);


                    for (int i = 0; i < arrayPhone.size(); i++) {
                        if (i == 0) {
                            strPhone = arrayPhone.get(i).getPhone();
                        } else {
                            strPhone = strPhone + "," + arrayPhone.get(i).getPhone();
                        }
                    }
                    if (utils.isNetConnected()) {
                        callBecomePartner();
//                        callUpdateService();
                    } else {
                        utils.Toast(R.string.check_net, null);
                    }
                }
                break;
            case R.id.imgAddPhone:
                String strPhone = edtPhone.getText().toString();
                PhoneDo phone = null;
                if (!strPhone.equals("")) {
                    phone = new PhoneDo();

                    phone.setPhone(strPhone);
                    arrayPhone.add(phone);
                    Collections.reverse(arrayPhone);
                    adapterPhone = new PhoneListAdapter(arrayPhone, "add");
                    rViewPhone.setAdapter(adapterPhone);
                    edtPhone.setText("");
                } else {
                    utils.Toast("Enter phone no.");
                }
//                imgAddPhone.setImageResource(R.mipmap.favourite);
//                lay2ndPhone.setVisibility(View.VISIBLE);
                break;
            case R.id.imgOne:
                if (strImageOne.equals("")) {
                    selectImage();
                    Log.d("img","imgOne click");
                } else {
                    dialog(REQUEST_CODE_ONE);
//                    Intent img = new Intent(getActivity(), AlbumSelectActivity.class);
//                    img.putExtra(Constants.INTENT_EXTRA_LIMIT, 1);
//                    startActivityForResult(img, REQUEST_CODE_ONE);
                }
                break;
            case R.id.imgTwo:
                if (strImageTwo.equals("")) {
                    selectImage();
                } else {
                    dialog(REQUEST_CODE_TWO);
//                    Intent img = new Intent(getActivity(), AlbumSelectActivity.class);
//                    img.putExtra(Constants.INTENT_EXTRA_LIMIT, 1);
//                    startActivityForResult(img, REQUEST_CODE_TWO);
                }
                break;
            case R.id.imgThree:
                if (strImageThree.equals("")) {
                    selectImage();
                } else {
                    dialog(REQUEST_CODE_THREE);
//                    Intent img = new Intent(getActivity(), AlbumSelectActivity.class);
//                    img.putExtra(Constants.INTENT_EXTRA_LIMIT, 1);
//                    startActivityForResult(img, REQUEST_CODE_THREE);
                }
                break;
            case R.id.imgFour:
                if (strImageFour.equals("")) {
                    selectImage();
                } else {
                    dialog(REQUEST_CODE_FOUR);
//                    Intent img = new Intent(getActivity(), AlbumSelectActivity.class);
//                    img.putExtra(Constants.INTENT_EXTRA_LIMIT, 1);
//                    startActivityForResult(img, REQUEST_CODE_FOUR);
                }
                break;
            case R.id.imgFive:
                if (strImageFive.equals("")) {
                    selectImage();
                } else {
                    dialog(REQUEST_CODE_FIVE);
//                    Intent img = new Intent(getActivity(), AlbumSelectActivity.class);
//                    img.putExtra(Constants.INTENT_EXTRA_LIMIT, 1);
//                    startActivityForResult(img, REQUEST_CODE_FIVE);
                }
                break;
            case R.id.imgLocation:
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

                try {
                    utils.startProgress();
                    startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
                break;

        }
    }

    private void callUpdateService() {
        Log.d("stored values",""+utils.getPreference(Constant.PREF_EMAIL)+" "+utils.getPreference(Constant.PREF_NAME)+" "+utils.getPreference(Constant.PREF_PHONE)+" "+utils.getPreference(Constant.PREF_ID));


        JsonArray partner_images = new JsonArray();
        final JsonObject jsonObject = new JsonObject();
//        jsonObject.addProperty("username",utils.getPreference(Constant.PREF_NAME));
//        jsonObject.addProperty("email",utils.getPreference(Constant.PREF_EMAIL));
//        jsonObject.addProperty("user_phone",utils.getPreference(Constant.PREF_PHONE));
//        jsonObject.addProperty("image","");
//        jsonObject.addProperty("service_name","abc");
//        jsonObject.addProperty("phone","1234512345");
//        jsonObject.addProperty("society_name","dummy society name");
//        jsonObject.addProperty("area","dummy area");
//        jsonObject.addProperty("pin","1234");
//        jsonObject.addProperty("city","dummy city");
//        jsonObject.addProperty("address","dummy address");
//        jsonObject.addProperty("service_phone","1231231212");
//        jsonObject.addProperty("offer_title","dummy offer titile");
//        jsonObject.addProperty("offer_bonus_title","dummy offer_bonus_title");
//        jsonObject.addProperty("description","dummy description");
//        jsonObject.addProperty("cover_image","");
//        jsonObject.addProperty("cat_id","1");
//        jsonObject.addProperty("sub_cat_id","4");
//        jsonObject.addProperty("sub_cat_image","");
//        jsonObject.addProperty("user_id","65");
//        jsonObject.addProperty("id","51");
//        jsonObject.add("partner_images",partner_images);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://nearshear.com/staging/backend/api/partners/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final RequestAPI requestAPI = retrofit.create(RequestAPI.class);

//        Call<RegistrationDO> call = requestAPI.updateService(jsonObject);

//        call.enqueue(new Callback<RegistrationDO>() {
//            @Override
//            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
//                Log.d("url",""+response.raw().request().url());
//                Log.d("Response code:", "" + response.code());
//                Log.d("post object",""+jsonObject);
//            }
//
//            @Override
//            public void onFailure(Call<RegistrationDO> call, Throwable t) {
//                Log.d("error in request",t.getMessage());
//            }
//        });
    }

    private void selectImage() {
        Log.d("selectImg","img select button preesed"+imageLists);
        Intent intent = new Intent(getActivity(), AlbumSelectActivity.class);
        intent.putExtra(Constants.INTENT_EXTRA_LIMIT, 5 - imageLists.size());
        startActivityForResult(intent, Constants.REQUEST_CODE);
    }

    @Override
    public void onResume() {
        super.onResume();
        utils
                .dismissProgress();
    }

}
