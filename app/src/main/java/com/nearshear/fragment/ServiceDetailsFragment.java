package com.nearshear.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.nearshear.Constant;
import com.nearshear.MainActivity;
import com.nearshear.MyBounceInterpolator;
import com.nearshear.R;
import com.nearshear.adapter.FeaturedServicesListAdapter;
import com.nearshear.adapter.PhoneListAdapter;
import com.nearshear.adapter.ReviewsListAdapter;
import com.nearshear.adapter.ViewPagerAdapter;
import com.nearshear.api.ApiUtils;
import com.nearshear.api.RequestAPI;
import com.nearshear.model.PhoneDo;
import com.nearshear.model.RegistrationDO;
import com.nearshear.model.RegistrationInfo;
import com.nearshear.model.ReviewDo;
import com.nearshear.model.SellerDo;
import com.nearshear.model.ServiceDetailsDo;
import com.nearshear.model.SliderDo;
import com.nearshear.utils.Utils;

import java.util.ArrayList;

import io.supercharge.shimmerlayout.ShimmerLayout;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.facebook.FacebookSdk.getApplicationContext;

public class ServiceDetailsFragment extends Fragment implements View.OnClickListener {
    private View rootView;
    private Utils utils;
    private ViewPagerAdapter pagerAdapter;
    private ViewPager viewPager;
    private CircleIndicator indicator;
    private RecyclerView rViewReviewList;
    private LinearLayout layReviews;
    private TextView txtServiceTitle;
    private TextView txtPhone;
    private TextView txtAddress;
    private TextView txtDescription;
    private TextView txtSellerName;
    private SimpleRatingBar ratingBar;
    private ImageView imgFavourite;
    private TextView txtRateCount;
    private TextView txtRateCountReview;
    private LinearLayout layLoadMore;

    private LinearLayout layMyRating;
    private LinearLayout layRatingOne;
    private LinearLayout layRatingTwo;
    private LinearLayout layRatingThree;
    private LinearLayout layRatingFour;
    private LinearLayout layRatingFive;

    private SimpleRatingBar ratingBarOne;
    private SimpleRatingBar ratingBarTwo;
    private SimpleRatingBar ratingBarThree;
    private SimpleRatingBar ratingBarFour;
    private SimpleRatingBar ratingBarFive;
//    private SimpleRatingBar ratingBarReview;

    private TextView txtWriteReview;
    private TextView txtRateOne;
    private TextView txtRateTwo;
    private TextView txtRateThree;
    private TextView txtRateFour;
    private TextView txtRateFive;
    private TextView txtReview;
    private Dialog dialogReview;

    private String strServiceId;
    private double selectedRating;
    private ServiceDetailsDo model;
    private TextView txtTotalRating;

    ArrayList<ReviewDo> listOfReview;
    private int reviewCounter = 0;
    private int total_review = 0;
    private ReviewsListAdapter reviewsListAdapter;
    //Other services
    private RecyclerView rViewListFeaturedServices;
    private LinearLayoutManager layoutManager;
    private ShimmerLayout shimmerText;
    private FeaturedServicesListAdapter featuredServicesListAdapter;
    private LinearLayout layOtherServices;

    private PhoneListAdapter phoneListAdapter;
    private RecyclerView rViewPhone;
    private TextView txtOfferBonusTitle;
    private LinearLayout layMain;
    int colorWhite = Color.parseColor("#FFFFFF");

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_service_details, container, false);
        setViews();

        Bundle extras = getArguments();

        if (utils.isNetConnected()) {
            if (extras != null) {
                strServiceId = extras.getString("id");
                callServiceDetails();
//                callServiceDetailsCopy();
            }
        } else {
            layMain.setVisibility(View.GONE);
            utils.Toast(R.string.check_net, null);
        }
        return rootView;
    }

    private void callServiceDetailsCopy() {
        String id = utils.getPreference(Constant.PREF_ID);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://nearshear.com/staging/backend/api/partners/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final RequestAPI requestAPI = retrofit.create(RequestAPI.class);

        Call<RegistrationDO> call = requestAPI.getDetailsOfService();

        call.enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                Log.d("Response",""+response.raw().request().url());
                Log.d("Response code:", "" + response.code());
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                Log.d("error in request",t.getMessage());
            }
        });
    }

    private void setViews() {
        utils = new Utils(getActivity());
        listOfReview = new ArrayList<>();

        layMain = rootView.findViewById(R.id.layMain);
        viewPager = rootView.findViewById(R.id.viewPager);
        indicator = rootView.findViewById(R.id.indicator);
        ratingBar = rootView.findViewById(R.id.ratingBar);
//        ratingBarReview = rootView.findViewById(R.id.ratingBarReview);
        txtAddress = rootView.findViewById(R.id.txtAddress);
        txtDescription = rootView.findViewById(R.id.txtDescription);
        txtSellerName = rootView.findViewById(R.id.txtSellerName);
//        txtPhone = rootView.findViewById(R.id.txtPhone);
        txtServiceTitle = rootView.findViewById(R.id.txtServiceTitle);
        txtReview = rootView.findViewById(R.id.txtReview);
        layReviews = rootView.findViewById(R.id.layReviews);
        txtRateCount = rootView.findViewById(R.id.txtRateCount);
        txtRateCountReview = rootView.findViewById(R.id.txtRateCountReview);
        layLoadMore = rootView.findViewById(R.id.layLoadMore);
        layLoadMore.setOnClickListener(this);
        layOtherServices = rootView.findViewById(R.id.layOtherServices);
        txtTotalRating = rootView.findViewById(R.id.txtRating);


        RecyclerView.LayoutManager lLayout = new LinearLayoutManager(getContext());
        rViewReviewList = rootView.findViewById(R.id.rViewReviewList);
        rViewReviewList.setHasFixedSize(true);
        rViewReviewList.setLayoutManager(lLayout);

        RecyclerView.LayoutManager lLayout1 = new LinearLayoutManager(getContext());
        rViewPhone = rootView.findViewById(R.id.rViewPhoneList);
        rViewPhone.setHasFixedSize(true);
        rViewPhone.setLayoutManager(lLayout1);

        //rating
        layMyRating = rootView.findViewById(R.id.layMyRating);
        layRatingOne = rootView.findViewById(R.id.layRatingOne);
        layRatingTwo = rootView.findViewById(R.id.layRatingTwo);
        layRatingThree = rootView.findViewById(R.id.layRatingThree);
        layRatingFour = rootView.findViewById(R.id.layRatingFour);
        layRatingFive = rootView.findViewById(R.id.layRatingFive);

        ratingBarOne = rootView.findViewById(R.id.ratingBarOne);
        ratingBarTwo = rootView.findViewById(R.id.ratingBarTwo);
        ratingBarThree = rootView.findViewById(R.id.ratingBarThree);
        ratingBarFour = rootView.findViewById(R.id.ratingBarFour);
        ratingBarFive = rootView.findViewById(R.id.ratingBarFive);

        txtWriteReview = rootView.findViewById(R.id.txtWriteReview);
        txtRateOne = rootView.findViewById(R.id.txtRateOne);
        txtRateTwo = rootView.findViewById(R.id.txtRateTwo);
        txtRateThree = rootView.findViewById(R.id.txtRateThree);
        txtRateFour = rootView.findViewById(R.id.txtRateFour);
        txtRateFive = rootView.findViewById(R.id.txtRateFive);
        txtWriteReview.setOnClickListener(this);
        imgFavourite = rootView.findViewById(R.id.imgFavourite);
        imgFavourite.setOnClickListener(this);

        shimmerText = rootView.findViewById(R.id.shimmer_text);

        txtOfferBonusTitle = rootView.findViewById(R.id.txtOfferBonusTitle);

//        ServicesListAdapter.noteList


        layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rViewListFeaturedServices = rootView.findViewById(R.id.rViewListFeaturedServices);
        rViewListFeaturedServices.setHasFixedSize(true);
        rViewListFeaturedServices.setLayoutManager(layoutManager);
//        featuredServicesListAdapter = new FeaturedServicesListAdapter(ServicesListAdapter.noteList);
//        rViewListFeaturedServices.setAdapter(featuredServicesListAdapter);


    }

    private void callServiceDetails() {
        String id = utils.getPreference(Constant.PREF_ID);
        utils.startProgress();
//        final ShimmerLayout shimmerText = (ShimmerLayout) rootView.findViewById(R.id.shimmer_text);
//        shimmerText.startShimmerAnimation();
        ApiUtils.getAPIService().requestServiceDetails(id, strServiceId).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                utils.dismissProgress();
//                shimmerText.stopShimmerAnimation();
//                shimmerText.setVisibility(View.GONE);
                try {
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();

                    if (success.equals("1")) {
                        ArrayList<ServiceDetailsDo> serviceDetailsData = new ArrayList<>();
                        serviceDetailsData = response.body().getService_details_data();
                        model = serviceDetailsData.get(0);

                        ArrayList<SliderDo> listOfSliderData = new ArrayList<>();
                        listOfSliderData = response.body().getSlider_data();

                        pagerAdapter = new ViewPagerAdapter(getActivity(), listOfSliderData);
                        viewPager.setAdapter(pagerAdapter);
                        indicator.setViewPager(viewPager);

                        ArrayList<PhoneDo> listOfPhoneData = response.body().getPhone_data();
                        phoneListAdapter = new PhoneListAdapter(listOfPhoneData, "details");
                        rViewPhone.setAdapter(phoneListAdapter);

                        txtSellerName.setText(model.getSeller_name());
                        txtDescription.setText(model.getDescription());
                        txtOfferBonusTitle.setText(model.getOffer_bonus_title());
                        txtAddress.setText(model.getAddress());
                        txtServiceTitle.setText(model.getOrg_name());
                        try {
                            float rating = Float.parseFloat(model.getRate());
                            if (rating == 0) {
                                txtTotalRating.setBackgroundResource(R.drawable.rate_zero);
                                txtTotalRating.setText(model.getRating_text());
                            }
                            if (rating >= 1 && rating < 2) {
                                txtTotalRating.setBackgroundResource(R.drawable.rate_one_two);
                                txtTotalRating.setText(model.getRate());
                            }
                            if (rating >= 2 && rating < 3) {
                                txtTotalRating.setBackgroundResource(R.drawable.rate_one_two);
                                txtTotalRating.setText(model.getRate());
                            }
                            if (rating >= 3 && rating < 4) {
                                txtTotalRating.setBackgroundResource(R.drawable.rate_three);
                                txtTotalRating.setText(model.getRate());
                            }
                            if (rating >= 4 && rating <= 5) {
                                txtTotalRating.setBackgroundResource(R.drawable.rate_four_five);
                                txtTotalRating.setText(model.getRate());
                            }
//                            if (rating <= 5) {
//                                txtTotalRating.setBackgroundResource(R.color.rate_five);
//                                txtTotalRating.setText(model.getRate());
//                            }
//                            txtTotalRating.setText(model.getRate());
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }


                        if (model.getFavourite_flag().equals("1")) {
                            imgFavourite.setImageResource(R.mipmap.favourited);
                        } else {
                            imgFavourite.setImageResource(R.mipmap.favourite);
                        }

                        try {
                            ratingBar.setRating(Float.parseFloat(model.getRate()));
//                            ratingBarReview.setRating(Float.parseFloat(model.getRate()));
                            txtRateCount.setText("(" + model.getRate_count() + ")");

                            if(model.getRate_count().equals("0") || model.getRate_count().equals("")){
                                txtRateCountReview.setVisibility(View.GONE);
                            }else{
                                txtRateCountReview.setVisibility(View.VISIBLE);
                                txtRateCountReview.setText("(" + model.getRate_count() + ")");
                            }
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }

                        listOfReview = new ArrayList<>();
                        reviewCounter = 0;

                        //other services
                        ArrayList<SellerDo> listOfSellerData = new ArrayList<>();
                        listOfSellerData = response.body().getSeller_data();
                        if(listOfSellerData.size()>0){
                            layOtherServices.setVisibility(View.VISIBLE);
                            featuredServicesListAdapter = new FeaturedServicesListAdapter(listOfSellerData);
                            rViewListFeaturedServices.setAdapter(featuredServicesListAdapter);
                        }else{
                            layOtherServices.setVisibility(View.GONE);
                        }


                        callReview();
//                        callOtherServiceList();
                    } else if (success.equals("0")) {
                        utils.Toast(msg, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                utils.Toast("Please check connection", null);
            }
        });
    }

    private void callSubmitReview(String service_id, String review, String rating, final int pos) {
        utils.startProgress();
        String id = utils.getPreference(Constant.PREF_ID);
        ApiUtils.getAPIService().requestSubmitReviewAPI(id, service_id, review, rating, utils.timeZoneId()).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                utils.dismissProgress();
                try {
                    Log.d("::", String.valueOf(response));
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();
                    if (success.equals("1")) {
//                        dialogReview.dismiss();
//                        TripsList newDetails=pojo;
//                        newDetails.setIsRated("1");
//                        serviceList.set(pos,newDetails);
//                        notifyDataSetChanged();
                        dialogReview.dismiss();
                        callServiceDetails();
//                        callServiceDetailsCopy();
                    } else if (success.equals("0")) {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                utils.Toast("Network error");
            }
        });
    }

    public void setRating(float rating) {
        if (rating == 1) {
            layRatingOne.setBackgroundResource(R.drawable.rating_box_colored);
            layRatingTwo.setBackgroundResource(R.drawable.rating_box);
            layRatingThree.setBackgroundResource(R.drawable.rating_box);
            layRatingFour.setBackgroundResource(R.drawable.rating_box);
            layRatingFive.setBackgroundResource(R.drawable.rating_box);

            ratingBarOne.setBorderColor(getResources().getColor(R.color.orange));
            ratingBarOne.setFillColor(getResources().getColor(R.color.orange));
            ratingBarTwo.setBorderColor(getResources().getColor(R.color.black));
            ratingBarTwo.setFillColor(getResources().getColor(R.color.black));
            ratingBarThree.setBorderColor(getResources().getColor(R.color.black));
            ratingBarThree.setFillColor(getResources().getColor(R.color.black));
            ratingBarFour.setBorderColor(getResources().getColor(R.color.black));
            ratingBarFour.setFillColor(getResources().getColor(R.color.black));
            ratingBarFive.setBorderColor(getResources().getColor(R.color.black));
            ratingBarFive.setFillColor(getResources().getColor(R.color.black));

            txtRateOne.setTextColor(getResources().getColor(R.color.orange));
            txtRateTwo.setTextColor(getResources().getColor(R.color.black));
            txtRateThree.setTextColor(getResources().getColor(R.color.black));
            txtRateFour.setTextColor(getResources().getColor(R.color.black));
            txtRateFive.setTextColor(getResources().getColor(R.color.black));


        } else if (rating == 2) {
            layRatingOne.setBackgroundResource(R.drawable.rating_box_colored);
            layRatingTwo.setBackgroundResource(R.drawable.rating_box_colored);
            layRatingThree.setBackgroundResource(R.drawable.rating_box);
            layRatingFour.setBackgroundResource(R.drawable.rating_box);
            layRatingFive.setBackgroundResource(R.drawable.rating_box);

            ratingBarOne.setBorderColor(getResources().getColor(R.color.orange));
            ratingBarOne.setFillColor(getResources().getColor(R.color.orange));
            ratingBarTwo.setBorderColor(getResources().getColor(R.color.orange));
            ratingBarTwo.setFillColor(getResources().getColor(R.color.orange));
            ratingBarThree.setBorderColor(getResources().getColor(R.color.black));
            ratingBarThree.setFillColor(getResources().getColor(R.color.black));
            ratingBarFour.setBorderColor(getResources().getColor(R.color.black));
            ratingBarFour.setFillColor(getResources().getColor(R.color.black));
            ratingBarFive.setBorderColor(getResources().getColor(R.color.black));
            ratingBarFive.setFillColor(getResources().getColor(R.color.black));

            txtRateOne.setTextColor(getResources().getColor(R.color.orange));
            txtRateTwo.setTextColor(getResources().getColor(R.color.orange));
            txtRateThree.setTextColor(getResources().getColor(R.color.black));
            txtRateFour.setTextColor(getResources().getColor(R.color.black));
            txtRateFive.setTextColor(getResources().getColor(R.color.black));

        } else if (rating == 3) {
            layRatingOne.setBackgroundResource(R.drawable.rating_box_colored);
            layRatingTwo.setBackgroundResource(R.drawable.rating_box_colored);
            layRatingThree.setBackgroundResource(R.drawable.rating_box_colored);
            layRatingFour.setBackgroundResource(R.drawable.rating_box);
            layRatingFive.setBackgroundResource(R.drawable.rating_box);

            ratingBarOne.setBorderColor(getResources().getColor(R.color.orange));
            ratingBarOne.setFillColor(getResources().getColor(R.color.orange));
            ratingBarTwo.setBorderColor(getResources().getColor(R.color.orange));
            ratingBarTwo.setFillColor(getResources().getColor(R.color.orange));
            ratingBarThree.setBorderColor(getResources().getColor(R.color.orange));
            ratingBarThree.setFillColor(getResources().getColor(R.color.orange));
            ratingBarFour.setBorderColor(getResources().getColor(R.color.black));
            ratingBarFour.setFillColor(getResources().getColor(R.color.bgdash));
            ratingBarFive.setBorderColor(getResources().getColor(R.color.black));
            ratingBarFive.setFillColor(getResources().getColor(R.color.bgdash));

            txtRateOne.setTextColor(getResources().getColor(R.color.orange));
            txtRateTwo.setTextColor(getResources().getColor(R.color.orange));
            txtRateThree.setTextColor(getResources().getColor(R.color.orange));
            txtRateFour.setTextColor(getResources().getColor(R.color.black));
            txtRateFive.setTextColor(getResources().getColor(R.color.black));

        } else if (rating == 4) {
            layRatingOne.setBackgroundResource(R.drawable.rating_box_colored);
            layRatingTwo.setBackgroundResource(R.drawable.rating_box_colored);
            layRatingThree.setBackgroundResource(R.drawable.rating_box_colored);
            layRatingFour.setBackgroundResource(R.drawable.rating_box_colored);
            layRatingFive.setBackgroundResource(R.drawable.rating_box);

            ratingBarOne.setBorderColor(getResources().getColor(R.color.orange));
            ratingBarOne.setFillColor(getResources().getColor(R.color.orange));
            ratingBarTwo.setBorderColor(getResources().getColor(R.color.orange));
            ratingBarTwo.setFillColor(getResources().getColor(R.color.orange));
            ratingBarThree.setBorderColor(getResources().getColor(R.color.orange));
            ratingBarThree.setFillColor(getResources().getColor(R.color.orange));
            ratingBarFour.setBorderColor(getResources().getColor(R.color.orange));
            ratingBarFour.setFillColor(getResources().getColor(R.color.orange));
            ratingBarFive.setBorderColor(getResources().getColor(R.color.black));
            ratingBarFive.setFillColor(getResources().getColor(R.color.black));

            txtRateOne.setTextColor(getResources().getColor(R.color.orange));
            txtRateTwo.setTextColor(getResources().getColor(R.color.orange));
            txtRateThree.setTextColor(getResources().getColor(R.color.orange));
            txtRateFour.setTextColor(getResources().getColor(R.color.orange));
            txtRateFive.setTextColor(getResources().getColor(R.color.black));

        } else if (rating == 5) {
            layRatingOne.setBackgroundResource(R.drawable.rating_box_colored);
            layRatingTwo.setBackgroundResource(R.drawable.rating_box_colored);
            layRatingThree.setBackgroundResource(R.drawable.rating_box_colored);
            layRatingFour.setBackgroundResource(R.drawable.rating_box_colored);
            layRatingFive.setBackgroundResource(R.drawable.rating_box_colored);

            ratingBarOne.setBorderColor(getResources().getColor(R.color.orange));
            ratingBarOne.setFillColor(getResources().getColor(R.color.orange));
            ratingBarTwo.setBorderColor(getResources().getColor(R.color.orange));
            ratingBarTwo.setFillColor(getResources().getColor(R.color.orange));
            ratingBarThree.setBorderColor(getResources().getColor(R.color.orange));
            ratingBarThree.setFillColor(getResources().getColor(R.color.orange));
            ratingBarFour.setBorderColor(getResources().getColor(R.color.orange));
            ratingBarFour.setFillColor(getResources().getColor(R.color.orange));
            ratingBarFive.setBorderColor(getResources().getColor(R.color.orange));
            ratingBarFive.setFillColor(getResources().getColor(R.color.orange));

            txtRateOne.setTextColor(getResources().getColor(R.color.orange));
            txtRateTwo.setTextColor(getResources().getColor(R.color.orange));
            txtRateThree.setTextColor(getResources().getColor(R.color.orange));
            txtRateFour.setTextColor(getResources().getColor(R.color.orange));
            txtRateFive.setTextColor(getResources().getColor(R.color.orange));

        }
    }

    private void setRatingDialog(int rating, LinearLayout layRatingOne, LinearLayout layRatingTwo, LinearLayout layRatingThree, LinearLayout layRatingFour,
                                 LinearLayout layRatingFive, SimpleRatingBar ratingBarOne, SimpleRatingBar ratingBarTwo, SimpleRatingBar ratingBarThree, SimpleRatingBar ratingBarFour, SimpleRatingBar ratingBarFive, TextView txtRateOne, TextView txtRateTwo, TextView txtRateThree,
                                 TextView txtRateFour, TextView txtRateFive) {
        switch (rating) {
            case 1:
                selectedRating = 1.0;
                layRatingOne.setBackgroundResource(R.drawable.rating_one);
                layRatingTwo.setBackgroundResource(R.drawable.rating_box);
                layRatingThree.setBackgroundResource(R.drawable.rating_box);
                layRatingFour.setBackgroundResource(R.drawable.rating_box);
                layRatingFive.setBackgroundResource(R.drawable.rating_box);

                ratingBarOne.setBorderColor(getResources().getColor(R.color.rating_one));
                ratingBarOne.setFillColor(getResources().getColor(R.color.rating_one));
                ratingBarTwo.setBorderColor(getResources().getColor(R.color.black));
                ratingBarTwo.setFillColor(getResources().getColor(R.color.black));
                ratingBarThree.setBorderColor(getResources().getColor(R.color.black));
                ratingBarThree.setFillColor(getResources().getColor(R.color.black));
                ratingBarFour.setBorderColor(getResources().getColor(R.color.black));
                ratingBarFour.setFillColor(getResources().getColor(R.color.black));
                ratingBarFive.setBorderColor(getResources().getColor(R.color.black));
                ratingBarFive.setFillColor(getResources().getColor(R.color.black));

                txtRateOne.setTextColor(getResources().getColor(R.color.rating_one));
                txtRateTwo.setTextColor(getResources().getColor(R.color.black));
                txtRateThree.setTextColor(getResources().getColor(R.color.black));
                txtRateFour.setTextColor(getResources().getColor(R.color.black));
                txtRateFive.setTextColor(getResources().getColor(R.color.black));

                break;
            case 2:
                selectedRating = 2.0;
                layRatingOne.setBackgroundResource(R.drawable.rating_two);
                layRatingTwo.setBackgroundResource(R.drawable.rating_two);
                layRatingThree.setBackgroundResource(R.drawable.rating_box);
                layRatingFour.setBackgroundResource(R.drawable.rating_box);
                layRatingFive.setBackgroundResource(R.drawable.rating_box);

                ratingBarOne.setBorderColor(getResources().getColor(R.color.rating_two));
                ratingBarOne.setFillColor(getResources().getColor(R.color.rating_two));
                ratingBarTwo.setBorderColor(getResources().getColor(R.color.rating_two));
                ratingBarTwo.setFillColor(getResources().getColor(R.color.rating_two));
                ratingBarThree.setBorderColor(getResources().getColor(R.color.black));
                ratingBarThree.setFillColor(getResources().getColor(R.color.black));
                ratingBarFour.setBorderColor(getResources().getColor(R.color.black));
                ratingBarFour.setFillColor(getResources().getColor(R.color.black));
                ratingBarFive.setBorderColor(getResources().getColor(R.color.black));
                ratingBarFive.setFillColor(getResources().getColor(R.color.black));

                txtRateOne.setTextColor(getResources().getColor(R.color.rating_two));
                txtRateTwo.setTextColor(getResources().getColor(R.color.rating_two));
                txtRateThree.setTextColor(getResources().getColor(R.color.black));
                txtRateFour.setTextColor(getResources().getColor(R.color.black));
                txtRateFive.setTextColor(getResources().getColor(R.color.black));
                break;
            case 3:
                selectedRating = 3.0;
                layRatingOne.setBackgroundResource(R.drawable.rating_three);
                layRatingTwo.setBackgroundResource(R.drawable.rating_three);
                layRatingThree.setBackgroundResource(R.drawable.rating_three);
                layRatingFour.setBackgroundResource(R.drawable.rating_box);
                layRatingFive.setBackgroundResource(R.drawable.rating_box);

                ratingBarOne.setBorderColor(getResources().getColor(R.color.rating_three));
                ratingBarOne.setFillColor(getResources().getColor(R.color.rating_three));
                ratingBarTwo.setBorderColor(getResources().getColor(R.color.rating_three));
                ratingBarTwo.setFillColor(getResources().getColor(R.color.rating_three));
                ratingBarThree.setBorderColor(getResources().getColor(R.color.rating_three));
                ratingBarThree.setFillColor(getResources().getColor(R.color.rating_three));
                ratingBarFour.setBorderColor(getResources().getColor(R.color.black));
                ratingBarFour.setFillColor(getResources().getColor(R.color.black));
                ratingBarFive.setBorderColor(getResources().getColor(R.color.black));
                ratingBarFive.setFillColor(getResources().getColor(R.color.black));

                txtRateOne.setTextColor(getResources().getColor(R.color.rating_three));
                txtRateTwo.setTextColor(getResources().getColor(R.color.rating_three));
                txtRateThree.setTextColor(getResources().getColor(R.color.rating_three));
                txtRateFour.setTextColor(getResources().getColor(R.color.black));
                txtRateFive.setTextColor(getResources().getColor(R.color.black));
                break;
            case 4:
                selectedRating = 4.0;
                layRatingOne.setBackgroundResource(R.drawable.rating_four);
                layRatingTwo.setBackgroundResource(R.drawable.rating_four);
                layRatingThree.setBackgroundResource(R.drawable.rating_four);
                layRatingFour.setBackgroundResource(R.drawable.rating_four);
                layRatingFive.setBackgroundResource(R.drawable.rating_box);

                ratingBarOne.setBorderColor(getResources().getColor(R.color.rating_four));
                ratingBarOne.setFillColor(getResources().getColor(R.color.rating_four));
                ratingBarTwo.setBorderColor(getResources().getColor(R.color.rating_four));
                ratingBarTwo.setFillColor(getResources().getColor(R.color.rating_four));
                ratingBarThree.setBorderColor(getResources().getColor(R.color.rating_four));
                ratingBarThree.setFillColor(getResources().getColor(R.color.rating_four));
                ratingBarFour.setBorderColor(getResources().getColor(R.color.rating_four));
                ratingBarFour.setFillColor(getResources().getColor(R.color.rating_four));
                ratingBarFive.setBorderColor(getResources().getColor(R.color.black));
                ratingBarFive.setFillColor(getResources().getColor(R.color.black));

                txtRateOne.setTextColor(getResources().getColor(R.color.rating_four));
                txtRateTwo.setTextColor(getResources().getColor(R.color.rating_four));
                txtRateThree.setTextColor(getResources().getColor(R.color.rating_four));
                txtRateFour.setTextColor(getResources().getColor(R.color.rating_four));
                txtRateFive.setTextColor(getResources().getColor(R.color.black));
                break;
            case 5:
                selectedRating = 5.0;
                layRatingOne.setBackgroundResource(R.drawable.rating_five);
                layRatingTwo.setBackgroundResource(R.drawable.rating_five);
                layRatingThree.setBackgroundResource(R.drawable.rating_five);
                layRatingFour.setBackgroundResource(R.drawable.rating_five);
                layRatingFive.setBackgroundResource(R.drawable.rating_five);

                ratingBarOne.setBorderColor(getResources().getColor(R.color.rating_five));
                ratingBarOne.setFillColor(getResources().getColor(R.color.rating_five));
                ratingBarTwo.setBorderColor(getResources().getColor(R.color.rating_five));
                ratingBarTwo.setFillColor(getResources().getColor(R.color.rating_five));
                ratingBarThree.setBorderColor(getResources().getColor(R.color.rating_five));
                ratingBarThree.setFillColor(getResources().getColor(R.color.rating_five));
                ratingBarFour.setBorderColor(getResources().getColor(R.color.rating_five));
                ratingBarFour.setFillColor(getResources().getColor(R.color.rating_five));
                ratingBarFive.setBorderColor(getResources().getColor(R.color.rating_five));
                ratingBarFive.setFillColor(getResources().getColor(R.color.rating_five));

                txtRateOne.setTextColor(getResources().getColor(R.color.rating_five));
                txtRateTwo.setTextColor(getResources().getColor(R.color.rating_five));
                txtRateThree.setTextColor(getResources().getColor(R.color.rating_five));
                txtRateFour.setTextColor(getResources().getColor(R.color.rating_five));
                txtRateFive.setTextColor(getResources().getColor(R.color.rating_five));
                break;
        }

    }

    private void dialogReview() {
        dialogReview = new Dialog(getContext());
        dialogReview.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogReview.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
       dialogReview.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogReview.setContentView(R.layout.dialog_review);
        dialogReview.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        dialogReview.setCanceledOnTouchOutside(false);
        dialogReview.show();

        final LinearLayout layRatingOne = dialogReview.findViewById(R.id.layRatingOne);
        final LinearLayout layRatingTwo = dialogReview.findViewById(R.id.layRatingTwo);
        final LinearLayout layRatingThree = dialogReview.findViewById(R.id.layRatingThree);
        final LinearLayout layRatingFour = dialogReview.findViewById(R.id.layRatingFour);
        final LinearLayout layRatingFive = dialogReview.findViewById(R.id.layRatingFive);

        final SimpleRatingBar ratingBarOne = dialogReview.findViewById(R.id.ratingBarOne);
        final SimpleRatingBar ratingBarTwo = dialogReview.findViewById(R.id.ratingBarTwo);
        final SimpleRatingBar ratingBarThree = dialogReview.findViewById(R.id.ratingBarThree);
        final SimpleRatingBar ratingBarFour = dialogReview.findViewById(R.id.ratingBarFour);
        final SimpleRatingBar ratingBarFive = dialogReview.findViewById(R.id.ratingBarFive);

        final TextView txtRateOne = dialogReview.findViewById(R.id.txtRateOne);
        final TextView txtRateTwo = dialogReview.findViewById(R.id.txtRateTwo);
        final TextView txtRateThree = dialogReview.findViewById(R.id.txtRateThree);
        final TextView txtRateFour = dialogReview.findViewById(R.id.txtRateFour);
        final TextView txtRateFive = dialogReview.findViewById(R.id.txtRateFive);
        final TextView txtTitle = dialogReview.findViewById(R.id.txtTitle);

        ImageView imgBack=dialogReview.findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogReview.dismiss();
            }
        });
        imgBack.setColorFilter(colorWhite);


        txtTitle.setText(model.getOrg_name());

        layRatingOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layRatingOne.startAnimation(getAnimation());
                setRatingDialog(1, layRatingOne, layRatingTwo, layRatingThree, layRatingFour, layRatingFive, ratingBarOne, ratingBarTwo, ratingBarThree, ratingBarFour, ratingBarFive, txtRateOne, txtRateTwo, txtRateThree, txtRateFour, txtRateFive);
            }
        });
        layRatingTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layRatingTwo.startAnimation(getAnimation());
                setRatingDialog(2, layRatingOne, layRatingTwo, layRatingThree, layRatingFour, layRatingFive, ratingBarOne, ratingBarTwo, ratingBarThree, ratingBarFour, ratingBarFive, txtRateOne, txtRateTwo, txtRateThree, txtRateFour, txtRateFive);
            }
        });
        layRatingThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layRatingThree.startAnimation(getAnimation());
                setRatingDialog(3, layRatingOne, layRatingTwo, layRatingThree, layRatingFour, layRatingFive, ratingBarOne, ratingBarTwo, ratingBarThree, ratingBarFour, ratingBarFive, txtRateOne, txtRateTwo, txtRateThree, txtRateFour, txtRateFive);
            }
        });
        layRatingFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layRatingFour.startAnimation(getAnimation());
                setRatingDialog(4, layRatingOne, layRatingTwo, layRatingThree, layRatingFour, layRatingFive, ratingBarOne, ratingBarTwo, ratingBarThree, ratingBarFour, ratingBarFive, txtRateOne, txtRateTwo, txtRateThree, txtRateFour, txtRateFive);
            }
        });
        layRatingFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layRatingFive.startAnimation(getAnimation());
                setRatingDialog(5, layRatingOne, layRatingTwo, layRatingThree, layRatingFour, layRatingFive, ratingBarOne, ratingBarTwo, ratingBarThree, ratingBarFour, ratingBarFive, txtRateOne, txtRateTwo, txtRateThree, txtRateFour, txtRateFive);
            }
        });

        final EditText edtReview = dialogReview.findViewById(R.id.edtReview);
        Button btnSubmit = dialogReview.findViewById(R.id.btnSubmitReview);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strReview = edtReview.getText().toString();
//                String strRating = String.valueOf(ratingBar.getRating());
//                if (strReview.equals("")) {
//                    utils.Toast("Please write review");
//                } else
                    if (selectedRating < 1) {
                    utils.Toast("Please Select rating");
                } else {
                    callSubmitReview(strServiceId, strReview, String.valueOf(selectedRating), 0);
                }
            }
        });
    }

    public Animation getAnimation(){
        final Animation myAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
        double animationDuration = 2 * 1000;
        myAnim.setDuration((long) animationDuration);
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.10, 20);
        myAnim.setInterpolator(interpolator);

        return myAnim;
    }
    private void callFavourite(final ServiceDetailsDo model, final String action) {
        String id = utils.getPreference(Constant.PREF_ID);
        utils.startProgress();
        ApiUtils.getAPIService().requestAddRemoveFavourite(id, model.getService_id(), action).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                utils.dismissProgress();
                try {
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();

                    if (success.equals("1")) {
                        ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                        listOfPostData = response.body().getPostdata();
                        String favourite_count = listOfPostData.get(0).getTotal_fav_count();
                        ((MainActivity) getActivity()).setFavouriteCount(favourite_count);

                        ServiceDetailsDo service = new ServiceDetailsDo();
                        service = model;
                        if (action.equals("1")) {
                            service.setFavourite_flag("1");
                            imgFavourite.setImageResource(R.mipmap.favourited);
                        } else if (action.equals("0")) {
                            service.setFavourite_flag("0");
                            imgFavourite.setImageResource(R.mipmap.favourite);
                        }

//                        noteList.set(position,service);
//                        notifyDataSetChanged();
                    } else if (success.equals("0")) {
                        utils.Toast(msg, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                utils.Toast("Please check connection", null);
            }
        });
    }

    private void callReview() {
        utils.startProgress();
        ApiUtils.getAPIService().requestReview(strServiceId, String.valueOf(reviewCounter)).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                utils.dismissProgress();
                try {
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();

                    if (success.equals("1")) {
                        listOfReview.addAll(response.body().getReview_data());
                        if (listOfReview.size() > 0) {
                            total_review = Integer.parseInt(response.body().getTotal_review());
                            if (listOfReview.size() < total_review) {
                                layLoadMore.setVisibility(View.VISIBLE);
                                reviewCounter = reviewCounter + 5;
                            } else {
                                layLoadMore.setVisibility(View.GONE);
                            }
                            layReviews.setVisibility(View.VISIBLE);
                            reviewsListAdapter = new ReviewsListAdapter(listOfReview);
                            rViewReviewList.setAdapter(reviewsListAdapter);
                        } else {
                            layReviews.setVisibility(View.GONE);
                        }
                    } else if (success.equals("0")) {
                        utils.Toast(msg, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                utils.Toast("Please check connection", null);
            }
        });
    }


//    private void callServiceList() {
//        String id = utils.getPreference(Constant.PREF_ID);
//        shimmerText.setVisibility(View.VISIBLE);
//        shimmerText.startShimmerAnimation();
//        ApiUtils.getAPIService().requestServiceList(id, subCatId).enqueue(new Callback<RegistrationDO>() {
//            @Override
//            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
//                shimmerText.stopShimmerAnimation();
//                shimmerText.setVisibility(View.GONE);
//                try {
//                    String success = response.body().getSuccess();
//                    String msg = response.body().getMessage();
//
//                    if (success.equals("1")) {
//                        ArrayList<SellerDo> listOfSellerData = new ArrayList<>();
//                        listOfSellerData = response.body().getSeller_data();
//
//                        featuredServicesListAdapter = new FeaturedServicesListAdapter(listOfSellerData);
//                        rViewListFeaturedServices.setAdapter(featuredServicesListAdapter);
//
//                    } else if (success.equals("0")) {
//                        utils.Toast(msg, null);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<RegistrationDO> call, Throwable t) {
//                utils.dismissProgress();
//                utils.Toast("Please check connection", null);
//            }
//        });
//    }

    private void callOtherServiceList() {
        String id = utils.getPreference(Constant.PREF_ID);
        shimmerText.startShimmerAnimation();
        ApiUtils.getAPIService().requestOtherServiceList(id, strServiceId).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                shimmerText.stopShimmerAnimation();
                shimmerText.setVisibility(View.GONE);
                try {
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();

                    if (success.equals("1")) {
                        ArrayList<SellerDo> listOfSellerData = new ArrayList<>();
                        listOfSellerData = response.body().getSeller_data();

                        layOtherServices.setVisibility(View.VISIBLE);
                        featuredServicesListAdapter = new FeaturedServicesListAdapter(listOfSellerData);
                        rViewListFeaturedServices.setAdapter(featuredServicesListAdapter);

                    } else if (success.equals("0")) {
                        layOtherServices.setVisibility(View.GONE);
//                        utils.Toast(msg, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                utils.Toast("Please check connection", null);
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtWriteReview:
                dialogReview();
                break;
            case R.id.imgFavourite:
                if (model.getFavourite_flag().equals("1")) {
                    callFavourite(model, "0");
                } else {
                    callFavourite(model, "1");
                }
                break;
            case R.id.layLoadMore:
                callReview();
                break;
        }
    }
}
