package com.nearshear.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.nearshear.MainActivity;
import com.nearshear.R;
import com.nearshear.api.ApiUtils;
import com.nearshear.api.RequestAPI;
import com.nearshear.model.Post;
import com.nearshear.model.RegistrationDO;
import com.nearshear.utils.Utils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AddServices.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AddServices#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddServices extends Fragment implements View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private Utils utils;
    private View rootView;
    private String mParam1;
    private String mParam2;
    private Button addService;
    private Button service1;

    private TextView textViewResult;
    private TextView textViewHead;
    private TextView textDesc;

    public AddServices() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddServices.
     */
    // TODO: Rename and change types and number of parameters
    public static AddServices newInstance(String param1, String param2) {
        AddServices fragment = new AddServices();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        callReq();

//        addService = rootView.findViewById(R.id.addService);
//
//        addService.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d("button","click");
//                ((MainActivity) getActivity()).setBecomePartner("Become a Partner");
//            }
//        });
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_add_services, container, false);
        setView();

        if (utils.isNetConnected()) {
            Log.d("you are ","connected");
//            requestGetAllServices();
        } else {
            utils.Toast(R.string.check_net, null);
        }

//        addService.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ((MainActivity) getActivity()).setBecomePartner("Become a Partner");
//            }
//        });
//
//        addService.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ((MainActivity) getActivity()).setUpdateService("Update");
//            }
//        });
        return rootView;
    }

    public void AddService(){

    }

    private void setView(){
        utils = new Utils(getActivity());
        addService = rootView.findViewById(R.id.addService);
        service1 = rootView.findViewById(R.id.service1);

        textViewResult = rootView.findViewById(R.id.text_view_result);
        textViewHead = rootView.findViewById(R.id.textViewHead);
        textDesc = rootView.findViewById(R.id.textDesc);

        addService.setOnClickListener(this);
        service1.setOnClickListener(this);
    }

    private void requestGetAllServices(){
        Log.d("Call","getServices");
        ApiUtils.getAPIService().requestGetAllServices().enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                try {
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();



                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.addService:
                ((MainActivity) getActivity()).setBecomePartner("Become a Partner");
                break;
            case R.id.service1:
//                ((MainActivity) getActivity()).setUpdateService("Update","12345");
                break;
        }
    }

    private void callReq(){

//        textViewResult = findViewById(R.id.text_view_result);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        RequestAPI requestAPI = retrofit.create(RequestAPI.class);

        Call<List<Post>> call = requestAPI.getPost();

        call.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {

                if (!response.isSuccessful()){
                    textViewResult.setText("code: "+response.code());
                    Log.d("isSuccefull error ",""+response.code());
                    return;
                }

//                Log.d("rahul ",""+response.body());

                List<Post> posts = response.body();

//                Log.d("rahul ",""+posts);

                for (Post post:posts) {
                    String content = "";
                    content += "ID : " + post.getId() + "\n";
//                    content += "USER ID : " + post.getUserId() + "\n";
                    content += "TITLE : " + post.getTitle() + "\n";
                    content += "TEXT : " + post.getText() + "\n\n\n";

                    textViewResult.append(content);

                    String title = post.getTitle();
                    String text = post.getText();

                    textViewHead.setText(title);
                    textDesc.setText(text);

//                    textViewHead.append(title);
//                    textDesc.append(text);

                }
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                textViewResult.setText(t.getMessage());
                Log.d("error ",t.getMessage());
            }
        });
//        to be continued...


    }
}
