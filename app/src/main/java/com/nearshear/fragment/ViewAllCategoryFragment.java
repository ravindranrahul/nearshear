package com.nearshear.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.nearshear.Constant;
import com.nearshear.MainActivity;
import com.nearshear.R;
import com.nearshear.adapter.FeaturedServicesListAdapter;
import com.nearshear.adapter.ServicesListAdapter;
import com.nearshear.adapter.ViewAllSubCategoryListAdapter;
import com.nearshear.api.ApiUtils;
import com.nearshear.model.RegistrationDO;
import com.nearshear.model.SellerDo;
import com.nearshear.utils.Utils;

import java.util.ArrayList;

import io.supercharge.shimmerlayout.ShimmerLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewAllCategoryFragment extends Fragment implements View.OnClickListener {
    private View rootView;
    private Utils utils;
    private RecyclerView rViewList;
    private LinearLayoutManager layoutManager;
    private ViewAllSubCategoryListAdapter adapter;
    private RecyclerView rViewListFeaturedServices;
    private FeaturedServicesListAdapter featuredServicesListAdapter;
    private String strCatId="";
    private ShimmerLayout shimmerText;
    private EditText edtSearch;
    private LinearLayout layFeaturedServices;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_view_all_category, container, false);
        setViews();

        Bundle extras = getArguments();

        if (utils.isNetConnected()) {
            if (extras != null) {
                strCatId= extras.getString("catid");
                callFeaturedServiceList();
            }
        } else {
            utils.Toast(R.string.check_net, null);
        }
        return rootView;
    }

    private void setViews() {
        utils = new Utils(getActivity());
        RecyclerView.LayoutManager lLayout = new LinearLayoutManager(getContext());
        rViewList = rootView.findViewById(R.id.rViewAllSubCatList);
        rViewList.setHasFixedSize(true);
        rViewList.setLayoutManager(lLayout);
        edtSearch=rootView.findViewById(R.id.edtSearch);
        edtSearch.setOnClickListener(this);
        layFeaturedServices=rootView.findViewById(R.id.layFeaturedServices);

        layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rViewListFeaturedServices = rootView.findViewById(R.id.rViewListFeaturedServices);
        rViewListFeaturedServices.setHasFixedSize(true);
        rViewListFeaturedServices.setLayoutManager(layoutManager);

        adapter = new ViewAllSubCategoryListAdapter(Constant.allSubCatList);
        rViewList.setAdapter(adapter);

        shimmerText =  rootView.findViewById(R.id.shimmer_text);

    }


    private void callFeaturedServiceList() {
        String id=utils.getPreference(Constant.PREF_ID);
        shimmerText.setVisibility(View.VISIBLE);
        shimmerText.startShimmerAnimation();

        ApiUtils.getAPIService().requestFeaturedServiceList(id,strCatId).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                shimmerText.stopShimmerAnimation();
                shimmerText.setVisibility(View.GONE);
                try {
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();

                    if (success.equals("1")) {
                        ArrayList<SellerDo> listOfSellerData = new ArrayList<>();
                        listOfSellerData  = response.body().getSeller_data();


                            layFeaturedServices.setVisibility(View.VISIBLE);
                            featuredServicesListAdapter = new FeaturedServicesListAdapter(listOfSellerData);
                            rViewListFeaturedServices.setAdapter(featuredServicesListAdapter);


                    } else if (success.equals("0")) {
                        layFeaturedServices.setVisibility(View.GONE);
//                        utils.Toast(msg, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                utils.Toast("Please check connection", null);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.edtSearch:
                ((MainActivity)getActivity()).setSearch("Search");
                break;
        }
    }
}
