package com.nearshear.api;

/**
 * Created by Rsjesh on 11/23/2017.
 */

public class ApiUtils {

//    public static final String BASE_URL = "http://ecommercedevelopment.org/near_shear/";
//    public static final String BASE_URL = "http://teamtech24.com/near_shear/";
//    public static final String BASE_URL = "http://nearshear.com/near_shear/";
    public static final String BASE_URL = "http://nearshear.com/staging/backend/";

    private ApiUtils() {
    }

    public static RequestAPI getAPIService() {
        return RetrofitClient.getClient(BASE_URL).create(RequestAPI.class);
    }
}
