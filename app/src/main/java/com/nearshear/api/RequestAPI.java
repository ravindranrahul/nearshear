package com.nearshear.api;


import com.google.gson.JsonObject;
import com.nearshear.fragment.UpdateService;
import com.nearshear.model.GetAllServices;
import com.nearshear.model.Post;
import com.nearshear.model.RegistrationDO;
import com.nearshear.model.UpdateImg;
import com.nearshear.model.UpdateImg1;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

import static com.nearshear.api.KeyAbstract.*;


public interface RequestAPI {

    //User registration
    @POST(LINK_USER_SIGNUP)
    @FormUrlEncoded
    Call<RegistrationDO> requestUserRegistration(
            @Field(KEY_FIRST_NAME) String fName,
            @Field(KEY_EMAIL) String email,
            @Field(KEY_PASSWORD) String password,
            @Field(KEY_PHONE) String mobile,
            @Field(KEY_DEVICE_ID) String deviceId,
            @Field(KEY_DEVICE_TYPE) String deviceType,
            @Field(KEY_TIME_ZONE) String timeZone,
            @Field(KEY_LOGIN_TYPE) String logintype
    );

    //User Login
    @POST(LOGIN_LINK)
    @FormUrlEncoded
    Call<RegistrationDO> requestLogin(
            @Field(KEY_NAME) String name,
            @Field(KEY_IMAGE) String image,
            @Field(KEY_EMAIL) String email,
            @Field(KEY_PASSWORD) String password,
            @Field(KEY_LOGIN_TYPE) String logintype,
            @Field(KEY_DEVICE_ID) String deviceId,
            @Field(KEY_DEVICE_TYPE) String deviceType,
            @Field(KEY_TIME_ZONE) String timeZone
    );

    //forgor password screen to sent link in email to reset
    @POST(FORGOT_PASSWORD)
    @FormUrlEncoded
    Call<RegistrationDO> requestForgotPassword(
            @Field(KEY_EMAIL) String email
    );

    @POST(NEW_PASSWORD)
    @FormUrlEncoded
    Call<RegistrationDO> requestNewPassword(
            @Field(KEY_ID) String id,
            @Field(KEY_PASSWORD) String psw
    );


    @POST(OTP_LINK)
    @FormUrlEncoded
    Call<RegistrationDO> requestVerifyOtp(
            @Field(KEY_ID) String id,
            @Field(KEY_OTP) String otp
    );

    @POST(OTP_CATEGORY_LIST)
    @FormUrlEncoded
    Call<RegistrationDO> requestCategoryList(
            @Field(KEY_ID) String id
    );

    @POST(BECOME_PARTNER_LINK)
    @Multipart
    Call<RegistrationDO> requestBecomePartner(@Part(KEY_ID) RequestBody id,
                                              @Part(KEY_ORG_NAME) RequestBody name,
                                              @Part(KEY_SOCIETY_NAME) RequestBody sname,
                                              @Part(KEY_AREA) RequestBody area,
                                              @Part(KEY_PIN) RequestBody pin,
                                              @Part(KEY_CITY) RequestBody city,
                                              @Part(KEY_PHONE) RequestBody phone,
                                              @Part(KEY_OFFER_TITLE) RequestBody otitle,
                                              @Part(KEY_OFFER_BONUS_TITLE) RequestBody btitle,
                                              @Part(KEY_DESCRIPTION) RequestBody dsc,
                                              @Part(KEY_CAT_ID) RequestBody catid,
                                              @Part(KEY_SUB_CAT_ID) RequestBody  subcatid,
                                              @Part(KEY_LAT) RequestBody lat,
                                              @Part(KEY_LON) RequestBody lon,
                                              @Part(KEY_TIME_ZONE) RequestBody time,
                                              @Part MultipartBody.Part image,
                                              @Part List<MultipartBody.Part> imagearray
    );

    @POST(SERVICES_LIST_LINK)
    @FormUrlEncoded
    Call<RegistrationDO> requestServiceList(
            @Field(KEY_ID) String id,
            @Field(KEY_SUB_CAT_ID)String subCatid,
            @Field(KEY_LAT) String lat,
            @Field(KEY_LON) String lon
    );

    @POST(FAVOURITE_LIST_LINK)
    @FormUrlEncoded
    Call<RegistrationDO> requestFavoriteList(
            @Field(KEY_ID) String id
    );

    @POST(ADD_REMOVE_FAVORITE_LINK)
    @FormUrlEncoded
    Call<RegistrationDO> requestAddRemoveFavourite(
            @Field(KEY_ID) String id,
            @Field(KEY_SERVICE_ID)String serviceid,
            @Field(KEY_ACTION)String action
    );

    @POST(SERVICES_DETAILS_LINK)
    @FormUrlEncoded
    Call<RegistrationDO> requestServiceDetails(
            @Field(KEY_ID) String id,
            @Field(KEY_SERVICE_ID)String subCatid
    );

    @POST(SUBMIT_REVIEW_LINK)
    @FormUrlEncoded
    Call<RegistrationDO> requestSubmitReviewAPI(
            @Field(KEY_ID) String id,
            @Field(KEY_SERVICE_ID) String sid,
            @Field(KEY_REVIEW) String review,
            @Field(KEY_RATE) String rating,
            @Field(KEY_TIME_ZONE) String timeZone
    );


    @POST(FEATURED_SERVICES_LIST_LINK)
    @FormUrlEncoded
    Call<RegistrationDO> requestFeaturedServiceList(
            @Field(KEY_ID) String id,
            @Field(KEY_CAT_ID)String Catid
    );

    @POST(SEARCH_LINK)
    @FormUrlEncoded
    Call<RegistrationDO> requestSearchList(
            @Field(KEY_ID) String id,
            @Field(KEY_KEYWORD)String word,
            @Field(KEY_LAT) String lat,
            @Field(KEY_LON) String lon
    );

    @POST(REVIEW_LINK)
    @FormUrlEncoded
    Call<RegistrationDO> requestReview(
            @Field(KEY_SERVICE_ID) String id,
            @Field(KEY_COUNTER)String countr
    );

    @POST(OTHER_SERVICES_LIST_LINK)
    @FormUrlEncoded
    Call<RegistrationDO> requestOtherServiceList(
            @Field(KEY_ID) String id,
            @Field(KEY_SERVICE_ID)String Catid
    );

    @POST(RESEND_OTP_LINK)
    @FormUrlEncoded
    Call<RegistrationDO> requestResendOTP(
            @Field(KEY_ID) String id
    );

    @POST(UPDATE_IMAGE_LINK)
    @Multipart
    Call<RegistrationDO> requestUpdateProfileImageAPI(
            @Part(KEY_ID) RequestBody id,
            @Part MultipartBody.Part image
    );

    @POST(UPDATE_LINK)
    @Multipart
    Call<RegistrationDO> requestUpdateProfileAPI(
            @Part(KEY_ID) RequestBody id,
            @Part(KEY_NAME) RequestBody name,
            @Part(KEY_PHONE) RequestBody phone,
            @Part MultipartBody.Part image
    );

    @GET(GET_SERVICES_LINK)
    Call<RegistrationDO> requestGetAllServices(

    );

    @GET("posts")
    Call<List<Post>> getPost();

    @GET("partner_list_by_user.php?id=")
    Call<GetAllServices> getAllServices(@Query("id") String id);

    @GET("edit_partner.php?id=67&partner_id=48")
    Call<RegistrationDO> getDetailsOfService();

//    @Headers({
//            "Content-Type: application/json"
//    })
////    @POST("update_partner.php")
////    Call<RegistrationDO> updateService(@Body JsonObject jsonObject);

    @POST("update_partner.php")
    @Multipart
    Call<RegistrationDO> updateService(@Part("username") RequestBody username,
                                       @Part("email") RequestBody email,
                                       @Part("service_name") RequestBody service_name,
                                       @Part("society_name") RequestBody society_name,
                                       @Part("area") RequestBody area,
                                       @Part("pin") RequestBody pin,
                                       @Part("city") RequestBody city,
                                       @Part("address") RequestBody address,
                                       @Part("service_phone") RequestBody service_phone,
                                       @Part("offer_title") RequestBody offer_title,
                                       @Part("offer_bonus_title") RequestBody offer_bonus_title,
                                       @Part("description") RequestBody description,
                                       @Part("cat_id") RequestBody cat_id,
                                       @Part("sub_cat_id") RequestBody  sub_cat_id,
                                       @Part("sub_cat_image") RequestBody sub_cat_image,
                                       @Part("user_id") RequestBody user_id,
                                       @Part("id") RequestBody id,
                                       @Part MultipartBody.Part image,
                                       @Part List<MultipartBody.Part> imagearray
    );

    @POST("activate_partner.php")
    @Multipart
    Call<RegistrationDO> updateActive(@Part("id") RequestBody id,
                                      @Part("active") RequestBody active
    );

    @GET("partner_photos.php?id=")
    Call<UpdateImg> getAllImages(@Query("id") String id);

    @GET("partner_images.php?id=")
    Call<List<UpdateImg1>> getAllImages1(@Query("id") String id);
}