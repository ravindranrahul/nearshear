package com.nearshear.api;

public class KeyAbstract {
    public static final String LOGIN_LINK = "login.php";
    public static final String LINK_USER_SIGNUP = "register.php";
//    public static final String LINK_FORGOT_PASS = "forgot_password.php";
    public static final String OTP_LINK= "verify_otp.php";
    public static final String OTP_CATEGORY_LIST  = "get_category_list.php";
    public static final String BECOME_PARTNER_LINK  = "partners_register.php";
    public static final String SERVICES_LIST_LINK  = "service_list.php";
    public static final String ADD_REMOVE_FAVORITE_LINK  = "add_remove_favourite.php";
    public static final String FAVOURITE_LIST_LINK  = "favourite_list.php";
    public static final String SERVICES_DETAILS_LINK= "service_details.php";
    public static final String SUBMIT_REVIEW_LINK= "add_review.php";
    public static final String FEATURED_SERVICES_LIST_LINK= "feature_services.php";
    public static final String SEARCH_LINK= "search.php";
    public static final String FORGOT_PASSWORD= "forgot_password.php";
    public static final String NEW_PASSWORD= "new_password.php";
    public static final String REVIEW_LINK= "review.php";
    public static final String OTHER_SERVICES_LIST_LINK= "other_services.php";
    public static final String RESEND_OTP_LINK= "resend_otp.php";
    public static final String UPDATE_IMAGE_LINK= "upload_profile.php";
    public static final String UPDATE_LINK= "user_update_profile.php";
    public static final String GET_SERVICES_LINK = "";
    public static final String UPDATE_SERVICE = "";


    /**
     * json key's
     */
    public static final String KEY_SUCCESS = "success";
    public static final String KEY_SUCCESS_MSG = "message";
    public static final String KEY_POST_DATA = "post_data";
    public static final String KEY_CAT_DATA= "category_list";
    public static final String KEY_SUB_CAT_DATA= "sub_category_list";
    public static final String KEY_SELLER_LIST_DATA= "service_list_data";
    public static final String KEY_FAVOURITE_DATA= "favourite_list_data";
    public static final String KEY_SLIDER_DATA= "slider_data";
    public static final String KEY_SERVIE_DETAILS_DATA= "service_details_data";
    public static final String KEY_MY_REVIEW_DATA= "my_review_data";
    public static final String KEY_REVIEW_DATA= "review_data";

// login

    public static final String KEY_ID = "keyid";
    public static final String KEY_FIRST_NAME = "keyname";
    public static final String KEY_EMAIL = "keyemail";
    public static final String KEY_PASSWORD = "keypassword";
    public static final String KEY_PHONE_DATA= "phone_data";
    public static final String KEY_LOGIN_TYPE = "keylogin_type";
    public static final String KEY_DEVICE_ID = "keydevice_id";
    public static final String KEY_DEVICE_TYPE = "keydevice_type";
    public static final String KEY_TIME_ZONE = "keytimezone";
    public static final String KEY_NAME = "keyname";
    public static final String KEY_OTP_VERIFIED = "keyotp";
    public static final String KEY_OTP= "keyverify_number";
    public static final String KEY_IMAGE= "keyimage";
    public static final String KEY_ADDRESS= "keyaddress";

    public static final String KEY_CAT_ID= "keycat_id";
    public static final String KEY_CAT_NAME= "keycat_name";
    public static final String KEY_SUB_CAT_ID= "keysub_cat_id";
    public static final String KEY_SUB_CAT_NAME= "keysub_cat_name";
    public static final String KEY_SUB_CAT_IMAGE= "keysub_cat_image";
    public static final String KEY_SOCIETY_NAME= "keysociety_name";
    public static final String KEY_AREA= "keyarea";
    public static final String KEY_PIN= "keypin";
    public static final String KEY_OFFER_TITLE= "keyoffer_title";
    public static final String KEY_DESCRIPTION= "keydescription";
    public static final String KEY_IMAGES= "keyimagearray[]";
    public static final String KEY_PHONE= "keyphone";
    public static final String KEY_CITY= "keycity";

    public static final String KEY_FAVOURITE_FLAG= "keyfavourite_flag";
//    public static final String KEY_RATING= "keyrating";
    public static final String KEY_SERVICE_ID= "keyservice_id";
    public static final String KEY_ACTION  = "keyaction";
    public static final String KEY_RATE= "keyrate";
    public static final String KEY_RATE_COUNT= "keyrate_count";
    public static final String KEY_REVIEW= "keyreview";
    public static final String KEY_KEYWORD= "keykeyword";
    public static final String KEY_COUNTER= "keycounter";
    public static final String KEY_TOTOTAL_COUNT= "keytotal_count";
    public static final String KEY_ORG_NAME= "keyorg_name";
    public static final String KEY_RATE_TEXT= "keyrating_text";
    public static final String KEY_OFFER_BONUS_TITLE= "keyoffer_bonus_title";
    public static final String KEY_LAT= "keylatitude";
    public static final String KEY_LON= "keylongitude";
//    public static final String KEY_OTP= "keyverify_number";
//    public static final String KEY_OTP= "keyverify_number";
//    public static final String KEY_OTP= "keyverify_number";
//    public static final String KEY_OTP= "keyverify_number";
//    public static final String KEY_OTP= "keyverify_number";
//    public static final String KEY_OTP= "keyverify_number";
//    public static final String KEY_OTP= "keyverify_number";
//    public static final String KEY_OTP= "keyverify_number";
//    public static final String KEY_OTP= "keyverify_number";
//    public static final String KEY_OTP= "keyverify_number";
//    public static final String KEY_OTP= "keyverify_number";
//    public static final String KEY_OTP= "keyverify_number";


}