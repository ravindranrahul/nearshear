package com.nearshear;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.nearshear.BaseActivity;
import com.nearshear.R;
import com.nearshear.api.ApiUtils;
import com.nearshear.model.RegistrationDO;
import com.nearshear.model.RegistrationInfo;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewPasswordActivity extends BaseActivity implements View.OnClickListener {
    private String strNewPassword;
    private EditText edtNewPassword;
    private Button btnSave;
    private String strId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.color.white_gray);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(this.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_password);
        setViews();

    }

    private void setViews() {
        Bundle extras = getIntent().getExtras();
        String value;
        if (extras != null) {
            strId = extras.getString("id");
        }

        edtNewPassword = findViewById(R.id.edtNewPassword);
        btnSave = findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);

//        Uri data = getIntent().getData();
//        System.out.println("::"+data);
//        if (data != null && data.isHierarchical() && NewPasswordActivity.this != null) {
//            if (data.getQueryParameter("eid") != null) {
//                String param1 = data.getQueryParameter("eid");
//                // do some stuff
//                strId=param1;
//                System.out.println("::"+param1);
//            }
//        }
    }


    private void callNewPassword() {
        utils.startProgress();
        ApiUtils.getAPIService().requestNewPassword(strId, strNewPassword).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                utils.dismissProgress();
                try {
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();

                    if (success.equals("1")) {
//                        ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
//                        listOfPostData = response.body().getPostdata();
//                        RegistrationInfo registrationInfo = listOfPostData.get(0);
//                        String id = registrationInfo.getStrKeyId();

                        Intent login = new Intent(NewPasswordActivity.this, SignUpActivity.class);
                        startActivity(login);
                        finish();
                        utils.Toast(msg, null);
                    } else if (success.equals("0")) {
                        utils.Toast(msg, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                utils.Toast("Please check connection", null);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSave:
                strNewPassword=edtNewPassword.getText().toString();
                if(strNewPassword.equals("")){
                    utils.Toast("Enter password");
                }else{
                    if (utils.isNetConnected()) {
                        callNewPassword();
                    } else {
                        utils.Toast(R.string.check_net, null);
                    }
                }
                break;
        }
    }
}
