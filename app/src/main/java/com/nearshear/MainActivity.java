package com.nearshear;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.nearshear.fragment.AboutUs;
import com.nearshear.fragment.AboutUsInfo;
import com.nearshear.fragment.AddServiceTest;
import com.nearshear.fragment.AddServices;
import com.nearshear.fragment.BecomePartnerFragment;
import com.nearshear.fragment.EditProfile;
import com.nearshear.fragment.FavouriteFragment;
import com.nearshear.fragment.GetAllServices;
import com.nearshear.fragment.GetServiceDetails;
import com.nearshear.fragment.HelpCenter;
import com.nearshear.fragment.HomeFragment;
import com.nearshear.fragment.PrivacyPolicy;
import com.nearshear.fragment.ProfileFragment;
import com.nearshear.fragment.SearchFragment;
import com.nearshear.fragment.ServicesListFragment;
import com.nearshear.fragment.TermsCondition;
import com.nearshear.fragment.UpdateService;
import com.nearshear.fragment.ViewAllCategoryFragment;
import com.nearshear.fragment.ServiceDetailsFragment;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.facebook.FacebookSdk.getApplicationContext;

public class MainActivity extends BaseActivity implements View.OnClickListener {
    private LinearLayout layHome;
    private LinearLayout layFavourites;
    private LinearLayout layProfile;
    private LinearLayout laySearch;

    private ImageView imgHome;
    private ImageView imgFavourite;
    private ImageView imgProfile;

    private TextView txtHome;
    private TextView txtFavourite;
    private TextView txtProfile;

    private Fragment fragment;
    private TextView txtTitle;
    private String strLoginType;
    public static GoogleSignInClient mGoogleSignInClient;
    private Toolbar toolbar;
//    private Toolbar toolbarWhite;
    private ImageView imgBack;
    private String originFragment;
    String currentFragment;
    ArrayList<String> arrayTitleList;
    EditText edtSearch;
    MenuItem item;
    MenuItem addItem;
    ImageView imgSearch;
    LinearLayout layFavouriteCount;
    TextView txtFavouriteCount;
    int colorBlack = Color.parseColor("#787878");
    int colorWhite = Color.parseColor("#FFFFFF");

    public static String strImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
////            Window w = getWindow();
////            w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
////            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
//            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
//        }
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Window window = this.getWindow();
//            Drawable background = this.getResources().getDrawable(R.color.toolbar_gradiant);
//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(this.getResources().getColor(android.R.color.transparent));
////            window.setNavigationBarColor(this.getResources().getColor(android.R.color.transparent));
//            window.setBackgroundDrawable(background);
//        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setViews();

        if (strLoginType.equals("isGplus")) {
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();

            mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        }

        setHome("Near Shear", "start");

        edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // Your piece of code on keyboard search click
                    String word = edtSearch.getText().toString();
                    if (word.length() > 0) {
                        if (utils.isNetConnected()) {
                            SearchFragment fragment = (SearchFragment) getSupportFragmentManager().findFragmentById(R.id.frame_container);
                            fragment.callServiceList(word);
                        } else {
                            utils.Toast(R.string.check_net, null);
                        }

                    } else {
                        utils.Toast("Enter text to search");
                    }
                    return true;
                }
                return false;
            }
        });
    }

    private void setViews() {
        arrayTitleList = new ArrayList<>();

        toolbar = findViewById(R.id.toolbar);
//        toolbarWhite = findViewById(R.id.toolbarWhite);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        strLoginType = utils.getPreference(Constant.PREF_LOGIN_TYPE);
        layHome = findViewById(R.id.layHome);
        layFavourites = findViewById(R.id.layfavourites);
        layProfile = findViewById(R.id.layProfile);
        laySearch = findViewById(R.id.laySearch);
        edtSearch = findViewById(R.id.edtSearchBox);
        imgBack = findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);
        imgSearch = findViewById(R.id.imgSearch);
        imgSearch.setOnClickListener(this);


        txtTitle = findViewById(R.id.txtTitle);
        imgHome = findViewById(R.id.imgHome);
        imgFavourite = findViewById(R.id.imgFavourite);
        imgProfile = findViewById(R.id.imgProfile);

        txtHome = findViewById(R.id.txtHome);
        txtFavourite = findViewById(R.id.txtFavourite);
        txtProfile = findViewById(R.id.txtProfile);

        layHome.setOnClickListener(this);
        layFavourites.setOnClickListener(this);
        layProfile.setOnClickListener(this);

        layFavouriteCount = findViewById(R.id.layFavouriteCount);
        txtFavouriteCount = findViewById(R.id.txtFavouriteCount);
    }

    public void setTitle(String strTitle) {
        String title = strTitle.substring(0, 1).toUpperCase() + strTitle.substring(1);
        txtTitle.setText(title);
    }

    private void setResource(int resource) {
        switch (resource) {
            case 1:
                txtHome.setTextColor(getResources().getColor(R.color.orange));
                txtFavourite.setTextColor(getResources().getColor(R.color.black));
                txtProfile.setTextColor(getResources().getColor(R.color.black));

                imgHome.setImageDrawable(getResources().getDrawable(R.mipmap.home_coloured));
                imgFavourite.setImageDrawable(getResources().getDrawable(R.mipmap.favourites_black));
                imgProfile.setImageDrawable(getResources().getDrawable(R.mipmap.profile_black));
                setTitle("Near Shear");
                break;

            case 2:
                txtHome.setTextColor(getResources().getColor(R.color.black));
                txtFavourite.setTextColor(getResources().getColor(R.color.orange));
                txtProfile.setTextColor(getResources().getColor(R.color.black));

                imgHome.setImageDrawable(getResources().getDrawable(R.mipmap.home_black));
                imgFavourite.setImageDrawable(getResources().getDrawable(R.mipmap.favourite_coloured));
                imgProfile.setImageDrawable(getResources().getDrawable(R.mipmap.profile_black));
                setTitle("Favourite");
                break;

            case 3:
                txtHome.setTextColor(getResources().getColor(R.color.black));
                txtFavourite.setTextColor(getResources().getColor(R.color.black));
                txtProfile.setTextColor(getResources().getColor(R.color.orange));

                imgHome.setImageDrawable(getResources().getDrawable(R.mipmap.home_black));
                imgFavourite.setImageDrawable(getResources().getDrawable(R.mipmap.favourites_black));
                imgProfile.setImageDrawable(getResources().getDrawable(R.mipmap.profile_coloured));
                setTitle("My Profile");
                break;
        }
    }

    public void setHome(String strTitle, String from) {
        setToolBarTheme("orange");
        fragment = new HomeFragment();
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        fragmentManager.beginTransaction()
////                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
//                .replace(R.id.frame_container, fragment).commit();
        setToolBarTheme("orange");
        setFragment();
        setResource(1);
        arrayTitleList.add(strTitle);
        originFragment = "home";
        backArrow(false);
        if (!from.equals("start")) {
            item.setVisible(true);
            addItem.setVisible(false);
        }
//        addItem.setVisible(false);

    }

    public void setFavourite(String strTitle) {
        setToolBarTheme("orange");
        fragment = new FavouriteFragment();

        setFragment();
        setResource(2);
        arrayTitleList.add(strTitle);
        originFragment = "favourite";
        backArrow(false);
        item.setVisible(true);
//        addItem.setVisible(false);
    }

    public void setProfile(String strTitle) {
        setToolBarTheme("orange");
        fragment = new ProfileFragment();

        setFragment();
        setResource(3);
        backArrow(false);
        arrayTitleList.add(strTitle);
        originFragment = "profile";

        item.setVisible(false);
//        addItem.setVisible(false);
    }

    public void setEditProfile(String strTitle) {
        setTitle(strTitle);
        setToolBarTheme("orange");
        fragment = new EditProfile();

        setFragment();
        backArrow(true);
        arrayTitleList.add(strTitle);
        originFragment = "edit_profile";

        item.setVisible(false);
//        addItem.setVisible(false);
    }

    public void setViewAllSubCategoreies(String strTitle, String catId) {
        setTitle(strTitle);
        setToolBarTheme("orange");

        Bundle args = new Bundle();
        args.putString("catid", catId);
        fragment = new ViewAllCategoryFragment();
        fragment.setArguments(args);

        setFragment();
        backArrow(true);
        arrayTitleList.add(strTitle);
        currentFragment = strTitle;
        item.setVisible(true);
//        addItem.setVisible(false);
    }

    public void setBecomePartner(String strTitle) {
        setTitle(strTitle);
        setToolBarTheme("orange");

        fragment = new BecomePartnerFragment();

        setFragment();
        backArrow(true);
        arrayTitleList.add(strTitle);
        currentFragment = strTitle;
        item.setVisible(false);
//        addItem.setVisible(false);
    }

//    public void setAdService(String strTitle) {
//        setTitle(strTitle);
//        setToolBarTheme("orange");
//
//        fragment = new AddServices();
//
//        setFragment();
//        backArrow(true);
//        arrayTitleList.add(strTitle);
//        currentFragment = strTitle;
//        item.setVisible(false);
//    }

//    public void setAdService(String strTitle) {
//        setTitle(strTitle);
//        setToolBarTheme("orange");
//
//        fragment = new AddServiceTest();
//
//        setFragment();
//        backArrow(true);
//        arrayTitleList.add(strTitle);
//        currentFragment = strTitle;
//        item.setVisible(false);
//    }

    public void getServiceDetails(String strTitle, String id){
        setTitle(strTitle);
        setToolBarTheme("orange");

        Log.d("rahul","check id in main "+id);

        Bundle args = new Bundle();
//        args.putString("strTitle",strTitle);
        args.putString("id",id);

        fragment = new GetServiceDetails();
        fragment.setArguments(args);

        setFragment();
        backArrow(true);
        arrayTitleList.add(strTitle);
        currentFragment = strTitle;
        item.setVisible(false);
//        addItem.setVisible(false);
    }

    public void setAdService(String strTitle) {
        setTitle(strTitle);
        setToolBarTheme("orange");

        fragment = new GetAllServices();

        setFragment();
        backArrow(true);
        arrayTitleList.add(strTitle);
        currentFragment = strTitle;
        item.setVisible(false);
        addItem.setVisible(true);
    }

    public void setUpdateService(String strTitle,String partner_id ,String dispName, String addr, String area,String edtPin, String city, String edtPhone, String spinnerCategory, String spinnerSubCategory, String edtOfferBonusTitle, String description, String catname, String subcatname, String coverimage1) {
//        setTitle(strTitle);
        txtTitle.setText("Update Service");
        setToolBarTheme("orange");

        Bundle args = new Bundle();
        args.putString("id","Service");
        args.putString("partner_id",partner_id);
        args.putString("displayName",dispName);
        args.putString("addr",addr);
        args.putString("area",area);
        args.putString("edtPin",edtPin);
        args.putString("city",city);
        args.putString("edtPhone",edtPhone);
        args.putString("spinnerCategory",spinnerCategory);
        args.putString("spinnerSubCategory",spinnerSubCategory);
        args.putString("edtOfferBonusTitle",edtOfferBonusTitle);
        args.putString("description",description);
        args.putString("catname",catname);
        args.putString("subcatname",subcatname);
        args.putString("coverimage1",coverimage1);

        fragment = new UpdateService();
        fragment.setArguments(args);

        setFragment();
        backArrow(true);
        arrayTitleList.add(strTitle);
        currentFragment = strTitle;
        item.setVisible(false);
//        addItem.setVisible(false);
    }

    public void setServicesList(String strTitle, String id) {
        setTitle(strTitle);
        setToolBarTheme("orange");

        Bundle args = new Bundle();
        args.putString("id", id);
        fragment = new ServicesListFragment();
        fragment.setArguments(args);

        setFragment();
        backArrow(true);
        arrayTitleList.add(strTitle);
        currentFragment = strTitle;
        item.setVisible(true);
//        addItem.setVisible(false);
    }

    public void setServiceDetails(String strTitle, String id) {
        setTitle(strTitle);
        setToolBarTheme("orange");
        Bundle args = new Bundle();
        args.putString("id", id);
        fragment = new ServiceDetailsFragment();
        fragment.setArguments(args);

        setFragment();
        backArrow(true);
        arrayTitleList.add(strTitle);
        currentFragment = strTitle;
        item.setVisible(true);
//        addItem.setVisible(false);

        hideKey();
    }

    public void setSearch(String strTitle) {
        setTitle(strTitle);
        SearchFragment.word = "";
        edtSearch.setText("");
        setToolBarTheme("white");
        fragment = new SearchFragment();

        setFragment();
        backArrow(true);
        arrayTitleList.add(strTitle);
        edtSearch.setFocusable(true);
//        addItem.setVisible(false);

    }

    public void setAboutUs(String strTitle) {
        setTitle(strTitle);
        setToolBarTheme("orange");
        fragment = new AboutUs();

        setFragment();
        backArrow(true);
        arrayTitleList.add(strTitle);
        edtSearch.setFocusable(true);
//        addItem.setVisible(false);
    }

    public void setAboutUsInfo(String strTitle) {
        setTitle(strTitle);
        setToolBarTheme("orange");
        fragment = new AboutUsInfo();

        setFragment();
        backArrow(true);
        arrayTitleList.add(strTitle);
        edtSearch.setFocusable(true);
//        addItem.setVisible(false);
    }

    public void setPrivacyPolicy(String strTitle) {
        setTitle(strTitle);
        setToolBarTheme("orange");
        fragment = new PrivacyPolicy();

        setFragment();
        backArrow(true);
        arrayTitleList.add(strTitle);
        edtSearch.setFocusable(true);
//        addItem.setVisible(false);
    }

    public void setTermsCondition(String strTitle) {
        setTitle(strTitle);
        setToolBarTheme("orange");
        fragment = new TermsCondition();

        setFragment();
        backArrow(true);
        arrayTitleList.add(strTitle);
        edtSearch.setFocusable(true);
//        addItem.setVisible(false);
    }

    public void setHelpCenter(String strTitle) {
        setTitle(strTitle);
        setToolBarTheme("orange");
        fragment = new HelpCenter();

        setFragment();
        backArrow(true);
        arrayTitleList.add(strTitle);
        edtSearch.setFocusable(true);
//        addItem.setVisible(false);
    }

    public void setFavouriteCount(String count) {
        if (count.equals("0") || count.isEmpty()) {
            layFavouriteCount.setVisibility(View.GONE);
        } else {
            layFavouriteCount.setVisibility(View.VISIBLE);
            txtFavouriteCount.setText("  " + count + "  ");
        }
    }

    private void setToolBarTheme(String color) {
        switch (color) {
            case "orange":
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Window window = this.getWindow();
                    Drawable background = this.getResources().getDrawable(R.drawable.toolbar_gradiant);
                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    window.setStatusBarColor(this.getResources().getColor(android.R.color.transparent));
                    window.setBackgroundDrawable(background);
                }
                toolbar.setBackgroundResource(R.drawable.toolbar_gradiant);
                laySearch.setVisibility(View.GONE);
                imgBack.setColorFilter(colorWhite);
//                toolbar.setVisibility(View.VISIBLE);
//                toolbarWhite.setVisibility(View.GONE);
                break;
            case "white":
//                R.color.black_trans
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Window window = this.getWindow();
                    Drawable background = this.getResources().getDrawable(R.color.white_gray);
                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    window.setStatusBarColor(this.getResources().getColor(android.R.color.transparent));
                    window.setBackgroundDrawable(background);
                }
                toolbar.setBackgroundResource(R.color.white);
                laySearch.setVisibility(View.VISIBLE);
                imgBack.setColorFilter(colorBlack);
//                toolbar.setVisibility(View.GONE);
//                toolbarWhite.setVisibility(View.VISIBLE);
                break;

        }
    }

    public void setFragment() {
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    //.setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                    .replace(R.id.frame_container, fragment).addToBackStack(null).commit();

        } else {
            // error in creating fragment
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == Activity.RESULT_OK) {
                Uri resultUri = result.getUri();


                InputStream a = getInputStreamFromUri(resultUri);
                BufferedInputStream bufferedInputStream = new BufferedInputStream(a);
                Bitmap bmp = BitmapFactory.decodeStream(bufferedInputStream);
//                Bitmap cropped = result.getBitmap();
////
                String fileName = "test.jpg";
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//
                File ExternalStorageDirectory = Environment.getExternalStorageDirectory();
                File file = new File(ExternalStorageDirectory + File.separator + fileName);

                FileOutputStream fileOutputStream = null;
                try {
                    file.createNewFile();
                    fileOutputStream = new FileOutputStream(file);
                    fileOutputStream.write(bytes.toByteArray());

//                    Toast.makeText(this,
//                            file.getAbsolutePath(),
//                            Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } finally {
                    if (fileOutputStream != null) {
                        try {
                            fileOutputStream.close();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                }

                Uri uri = Uri.fromFile(file);
                strImage = String.valueOf(uri);


                if (originFragment.equals("profile")) {
                    ProfileFragment fragment = (ProfileFragment) getSupportFragmentManager().findFragmentById(R.id.frame_container);
                    fragment.setProfilePic(resultUri);
                }

                if (originFragment.equals("edit_profile")) {
                    EditProfile fragment = (EditProfile) getSupportFragmentManager().findFragmentById(R.id.frame_container);
                    fragment.setProfilePic(resultUri);
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }

        if (requestCode == 1000 && resultCode == Activity.RESULT_OK) {
            HomeFragment fragment = (HomeFragment) getSupportFragmentManager().findFragmentById(R.id.frame_container);
            fragment.checkLoc();
        }
        if (requestCode == 1000 && resultCode == Activity.RESULT_CANCELED) {
            HomeFragment fragment = (HomeFragment) getSupportFragmentManager().findFragmentById(R.id.frame_container);
            fragment.checkLoc();
        }


    }

    private InputStream getInputStreamFromUri(Uri uri) {
        try {
            File imageFile = new File(uri.getPath());
            return new FileInputStream(imageFile);
        } catch (FileNotFoundException e) {
            Log.w("", "getInputStreamFromUri: exception while opening input stream from file. try from content resolver");
        }

        try {
            return getApplicationContext().getContentResolver().openInputStream(uri);
        } catch (FileNotFoundException e) {
            Log.e("", "getInputStreamFromUri: exception while opening input stream from content resolver. returning");
        }

        return null;
    }

    private void backArrow(boolean back) {
        if (back) {
            imgBack.setVisibility(View.VISIBLE);
        } else {
            imgBack.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        item = menu.findItem(R.id.action_settings);
        addItem = menu.findItem(R.id.action_add);
        addItem.setVisible(false);

//        getMenuInflater().inflate(R.menu.menu_add,menu);
//        addItem = menu.findItem(R.id.action_add);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_add){
            Log.d("add","click");
            setBecomePartner("Become a Partner");
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        fm.popBackStack();

        int count = fm.getBackStackEntryCount();
//        if(count==0){
//            arrayTitleList=null;
//        }
//        if (count == 1) {
//            backArrow(false);
//
//        }

        if (count > 1) {
            arrayTitleList.remove(arrayTitleList.size() - 1);
            String title = arrayTitleList.get(count - 2);
            setTitle(title);
            if (title.equals("Near Shear")) {
                setToolBarTheme("orange");
                setResource(1);
                laySearch.setVisibility(View.GONE);
                backArrow(false);
                item.setVisible(true);
                addItem.setVisible(false);
                hideKey();
            } else if (title.equals("Favourite")) {
                item.setVisible(true);
                setResource(2);
                setToolBarTheme("orange");
                backArrow(false);
                addItem.setVisible(false);
                laySearch.setVisibility(View.GONE);
            } else if (title.equals("My Profile")) {
                setResource(3);
                setToolBarTheme("orange");
                backArrow(false);
                addItem.setVisible(false);
                laySearch.setVisibility(View.GONE);
                item.setVisible(false);
                addItem.setVisible(false);
                originFragment = "profile";
            } else if (title.equals("Search")) {
                setToolBarTheme("white");
                laySearch.setVisibility(View.VISIBLE);
                item.setVisible(false);
                addItem.setVisible(false);
            } else if (title.equals("Become a Partner")) {
                item.setVisible(false);
                addItem.setVisible(false);
            } else if (title.equals("Edit Profile Details")) {
                originFragment = "edit_profile";
            } else if (title.equals("Manage Services")){
                item.setVisible(false);
                addItem.setVisible(true);
            } else {
                item.setVisible(true);
                addItem.setVisible(false);
                backArrow(true);
                setToolBarTheme("orange");
                laySearch.setVisibility(View.GONE);
            }
        } else {
            finish();
        }


//        for (int i = 0; i < count; ++i) {
//        fm.popBackStack();
//        }

    }

    private void hideKey() {
        try {
            InputMethodManager inputManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);

            if (inputManager.isActive()) {
                inputManager.hideSoftInputFromWindow(layHome.getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layHome:
                setHome("Near Shear", "lay");
                break;
            case R.id.layfavourites:
                setFavourite("Favourite");
                break;
            case R.id.layProfile:
                setProfile("My Profile");
                break;
            case R.id.imgBack:
                onBackPressed();
                break;
            case R.id.imgSearch:
                String word = edtSearch.getText().toString();
                if (word.length() > 0) {
                    SearchFragment fragment = (SearchFragment) getSupportFragmentManager().findFragmentById(R.id.frame_container);
                    fragment.callServiceList(word);
                } else {
                    utils.Toast("Enter text to search");
                }
                break;

        }
    }

    public void exitApp() {
        finish();
    }
}
