package com.nearshear.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.nearshear.api.KeyAbstract;

public class ReviewDo extends KeyAbstract {
    @SerializedName(KEY_ID)
    @Expose
    private String id;
    @SerializedName(KEY_RATE)
    @Expose
    private String rate;

    @SerializedName(KEY_REVIEW)
    @Expose
    private String review;

    @SerializedName(KEY_NAME)
    @Expose
    private String name;

    @SerializedName(KEY_IMAGE)
    @Expose
    private String image;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
