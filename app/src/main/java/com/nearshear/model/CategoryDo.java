package com.nearshear.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.nearshear.api.KeyAbstract;

import java.util.ArrayList;

public class CategoryDo extends KeyAbstract {
    @SerializedName(KEY_SUB_CAT_DATA)
    @Expose
    private ArrayList<SubCategoryDo> sub_category_data = new ArrayList<>();

    @SerializedName(KEY_CAT_ID)
    @Expose
    private String category_id;

    @SerializedName(KEY_CAT_NAME)
    @Expose
    private String category_name;


    public ArrayList<SubCategoryDo> getSub_category_data() {
        return sub_category_data;
    }

    public void setSub_category_data(ArrayList<SubCategoryDo> sub_category_data) {
        this.sub_category_data = sub_category_data;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

}
