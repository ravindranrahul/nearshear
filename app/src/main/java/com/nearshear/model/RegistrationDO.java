package com.nearshear.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.nearshear.api.KeyAbstract;

import java.util.ArrayList;



public class RegistrationDO extends KeyAbstract {

    @SerializedName(KEY_POST_DATA)
    @Expose
    private ArrayList<RegistrationInfo> postdata = new ArrayList<>();

    @SerializedName(KEY_SUCCESS)
    @Expose
    private String success;

    @SerializedName(KEY_SUCCESS_MSG)
    @Expose
    private String message;

    @SerializedName(KEY_TOTOTAL_COUNT)
    @Expose
    private String total_review;




    public ArrayList<SellerDo> getFavourite_data() {
        return favourite_data;
    }

    public void setFavourite_data(ArrayList<SellerDo> favourite_data) {
        this.favourite_data = favourite_data;
    }

    @SerializedName(KEY_FAVOURITE_DATA)
    @Expose
    private ArrayList<SellerDo> favourite_data = new ArrayList<>();

    @SerializedName(KEY_SELLER_LIST_DATA)
    @Expose
    private ArrayList<SellerDo> seller_data = new ArrayList<>();

    @SerializedName(KEY_CAT_DATA)
    @Expose
    private ArrayList<CategoryDo> category_data = new ArrayList<>();

    @SerializedName(KEY_REVIEW_DATA)
    @Expose
    private ArrayList<ReviewDo> review_data = new ArrayList<>();

    @SerializedName(KEY_PHONE_DATA)
    @Expose
    private ArrayList<PhoneDo> phone_data = new ArrayList<>();
    @SerializedName(KEY_MY_REVIEW_DATA)
    @Expose
    private ArrayList<ReviewDo> my_review_data = new ArrayList<>();


    @SerializedName(KEY_SERVIE_DETAILS_DATA)
    @Expose
    private ArrayList<ServiceDetailsDo> service_details_data = new ArrayList<>();
    public ArrayList<SliderDo> getSlider_data() {
        return slider_data;
    }

    public void setSlider_data(ArrayList<SliderDo> slider_data) {
        this.slider_data = slider_data;
    }

    @SerializedName(KEY_SLIDER_DATA)
    @Expose
    private ArrayList<SliderDo> slider_data = new ArrayList<>();

    public ArrayList<SellerDo> getSeller_data() {
        return seller_data;
    }

    public void setSeller_data(ArrayList<SellerDo> seller_data) {
        this.seller_data = seller_data;
    }



    public ArrayList<CategoryDo> getCategory_data() {
        return category_data;
    }

    public void setCategory_data(ArrayList<CategoryDo> category_data) {
        this.category_data = category_data;
    }

    public RegistrationDO() {        //default constructor
    }

    public ArrayList<RegistrationInfo> getPostdata() {
        return postdata;
    }
    public void setPostdata(ArrayList<RegistrationInfo> postdata) {
        this.postdata = postdata;
    }

    public String getSuccess() {
        return success;
    }
    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<ServiceDetailsDo> getService_details_data() {
        return service_details_data;
    }

    public void setService_details_data(ArrayList<ServiceDetailsDo> service_details_data) {
        this.service_details_data = service_details_data;
    }

    public ArrayList<ReviewDo> getReview_data() {
        return review_data;
    }

    public void setReview_data(ArrayList<ReviewDo> review_data) {
        this.review_data = review_data;
    }

    public ArrayList<ReviewDo> getMy_review_data() {
        return my_review_data;
    }

    public void setMy_review_data(ArrayList<ReviewDo> my_review_data) {
        this.my_review_data = my_review_data;
    }

    public String getTotal_review() {
        return total_review;
    }

    public void setTotal_review(String total_review) {
        this.total_review = total_review;
    }

    public ArrayList<PhoneDo> getPhone_data() {
        return phone_data;
    }

    public void setPhone_data(ArrayList<PhoneDo> phone_data) {
        this.phone_data = phone_data;
    }

}

