package com.nearshear.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.nearshear.api.KeyAbstract;

public class SellerDo extends KeyAbstract{

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    @SerializedName(KEY_SERVICE_ID)
    @Expose
    private String service_id;

    @SerializedName(KEY_OFFER_TITLE)
    @Expose
    private String service_title;

    @SerializedName(KEY_NAME)
    @Expose
    private String seller_name;

    @SerializedName(KEY_ORG_NAME)
    @Expose
    private String org_name;


    @SerializedName(KEY_RATE)
    @Expose
    private String rating;

    @SerializedName(KEY_RATE_TEXT)
    @Expose
    private String rating_text;

    @SerializedName(KEY_FAVOURITE_FLAG)
    @Expose
    private String favourite_flag;

    @SerializedName(KEY_IMAGE)
    @Expose
    private String image;

    @SerializedName(KEY_OFFER_BONUS_TITLE)
    @Expose
    private String deal;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @SerializedName(KEY_DESCRIPTION)
    @Expose
    private String description;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    public String getService_title() {
        return service_title;
    }

    public void setService_title(String service_title) {
        this.service_title = service_title;
    }

    public String getSeller_name() {
        return seller_name;
    }

    public void setSeller_name(String seller_name) {
        this.seller_name = seller_name;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getFavourite_flag() {
        return favourite_flag;
    }

    public void setFavourite_flag(String favourite_flag) {
        this.favourite_flag = favourite_flag;
    }

    public String getOrg_name() {
        return org_name;
    }

    public void setOrg_name(String org_name) {
        this.org_name = org_name;
    }

    public String getRating_text() {
        return rating_text;
    }

    public void setRating_text(String rating_text) {
        this.rating_text = rating_text;
    }

    public String getDeal() {
        return deal;
    }

    public void setDeal(String deal) {
        this.deal = deal;
    }
}
