package com.nearshear.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.nearshear.api.KeyAbstract;

public class SubCategoryDo extends KeyAbstract {

    public String getSub_category_id() {
        return sub_category_id;
    }

    public void setSub_category_id(String sub_category_id) {
        this.sub_category_id = sub_category_id;
    }

    public String getSub_category_name() {
        return sub_category_name;
    }

    public void setSub_category_name(String sub_category_name) {
        this.sub_category_name = sub_category_name;
    }

    public String getSub_category_image() {
        return sub_category_image;
    }

    public void setSub_category_image(String sub_category_image) {
        this.sub_category_image = sub_category_image;
    }

    @SerializedName(KEY_SUB_CAT_ID)
    @Expose
    private String sub_category_id;

    @SerializedName(KEY_SUB_CAT_NAME)
    @Expose
    private String sub_category_name;

    @SerializedName(KEY_SUB_CAT_IMAGE)
    @Expose
    private String sub_category_image;

}
