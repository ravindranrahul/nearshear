package com.nearshear.model;

import com.google.gson.annotations.SerializedName;

public class Post {

//    private int userId;
    private int id;
    private String title;

    @SerializedName("body")
    private String text;

//    public int getUserId() {
//        return userId;
//    }


    public Post(int id, String title, String text) {
        this.id = id;
        this.title = title;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setText(String text) {
        this.text = text;
    }
}
