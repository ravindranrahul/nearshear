package com.nearshear.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.nearshear.api.KeyAbstract;

import java.util.ArrayList;

public class RegistrationInfo extends KeyAbstract {


    @SerializedName(KEY_FIRST_NAME)
    @Expose
    private String strFirstName;

    @SerializedName(KEY_ID)
    @Expose
    private String strKeyId;

    @SerializedName(KEY_EMAIL)
    @Expose
    private String strEmail;

    @SerializedName(KEY_PHONE)
    @Expose
    private String strMobile;

    @SerializedName(KEY_OTP_VERIFIED)
    @Expose
    private String strOtpVerified;

    @SerializedName(KEY_AREA)
    @Expose
    private String area;

    @SerializedName(KEY_PIN)
    @Expose
    private String pin;

    @SerializedName(KEY_CITY)
    @Expose
    private String city;

    @SerializedName(KEY_IMAGE)
    @Expose
    private String image;

    @SerializedName(KEY_TOTOTAL_COUNT)
    @Expose
    private String total_fav_count;

//    @SerializedName(KEY_PHONE)
//    @Expose
//    private ArrayList<PhoneDo> phone;


    public RegistrationInfo() {
        //default constructor
    }

    public String getStrFirstName() {
        return strFirstName;
    }

    public void setStrFirstName(String strFirstName) {
        this.strFirstName=strFirstName;
    }


    public String getStrEmail() {
        return strEmail;
    }

    public void setStrEmail(String strEmail) {
        this.strEmail=strEmail;
    }

    public String getStrMobile() {
        return strMobile;
    }

    public void setStrMobile(String strMobile) {
        this.strMobile=strMobile;
    }

    public String getStrKeyId() {
        return strKeyId;
    }

    public void setStrKeyId(String strKeyId) {
        this.strKeyId=strKeyId;
    }

    public String getStrOtpVerified() {
        return strOtpVerified;
    }

    public void setStrOtpVerified(String strOtpVerified) {
        this.strOtpVerified=strOtpVerified;
    }


    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }


    public String getTotal_fav_count() {
        return total_fav_count;
    }

    public void setTotal_fav_count(String total_fav_count) {
        this.total_fav_count = total_fav_count;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

//    public ArrayList<PhoneDo> getPhone() {
//        return phone;
//    }
//
//    public void setPhone(ArrayList<PhoneDo> phone) {
//        this.phone = phone;
//    }
}
