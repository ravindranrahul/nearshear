package com.nearshear.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.nearshear.api.KeyAbstract;

public class ServiceDetailsDo extends KeyAbstract{
    @SerializedName(KEY_ID)
    @Expose
    private String service_id;

    @SerializedName(KEY_NAME)
    @Expose
    private String seller_name;

    @SerializedName(KEY_ORG_NAME)
    @Expose
    private String org_name;

    @SerializedName(KEY_PHONE)
    @Expose
    private String phone;

    @SerializedName(KEY_ADDRESS)
    @Expose
    private String address;

    @SerializedName(KEY_OFFER_TITLE)
    @Expose
    private String title;

    @SerializedName(KEY_DESCRIPTION)
    @Expose
    private String description;

    public String getFavourite_flag() {
        return favourite_flag;
    }

    public void setFavourite_flag(String favourite_flag) {
        this.favourite_flag = favourite_flag;
    }

    @SerializedName(KEY_FAVOURITE_FLAG)
    @Expose
    private String favourite_flag;

    @SerializedName(KEY_RATE)
    @Expose
    private String rate;

    @SerializedName(KEY_RATE_TEXT)
    @Expose
    private String rating_text;


    @SerializedName(KEY_OFFER_BONUS_TITLE)
    @Expose
    private String offer_bonus_title;

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getRate_count() {
        return rate_count;
    }

    public void setRate_count(String rate_count) {
        this.rate_count = rate_count;
    }

    @SerializedName(KEY_RATE_COUNT)
    @Expose
    private String rate_count;


    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public String getSeller_name() {
        return seller_name;
    }

    public void setSeller_name(String seller_name) {
        this.seller_name = seller_name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOrg_name() {
        return org_name;
    }

    public void setOrg_name(String org_name) {
        this.org_name = org_name;
    }

    public String getRating_text() {
        return rating_text;
    }

    public void setRating_text(String rating_text) {
        this.rating_text = rating_text;
    }


    public String getOffer_bonus_title() {
        return offer_bonus_title;
    }

    public void setOffer_bonus_title(String offer_bonus_title) {
        this.offer_bonus_title = offer_bonus_title;
    }
}
