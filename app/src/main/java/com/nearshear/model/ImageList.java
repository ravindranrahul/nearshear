package com.nearshear.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.nearshear.api.KeyAbstract;

/**
 * Created by User on 12/26/2017.
 */

public class ImageList extends KeyAbstract {
//    @SerializedName(KeyAbstract.KEY_IMAGE)
//    @Expose
    private String image;
//    @SerializedName(KeyAbstract.KEY_IMAGE_ID)
//    @Expose
    private String image_id;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage_id() {
        return image_id;
    }

    public void setImage_id(String image_id) {
        this.image_id = image_id;
    }
}
