package com.nearshear.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.nearshear.api.KeyAbstract;

public class SliderDo extends KeyAbstract {
    @SerializedName(KEY_IMAGE)
    @Expose
    private String image;

    @SerializedName(KEY_ID)
    @Expose
    private String id;

    @SerializedName(KEY_OFFER_TITLE)
    @Expose
    private String title;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
