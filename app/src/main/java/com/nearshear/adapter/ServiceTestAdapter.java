package com.nearshear.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nearshear.MainActivity;
import com.nearshear.R;
import com.nearshear.model.ListItem;

import org.w3c.dom.Text;

import java.util.List;

public class ServiceTestAdapter extends RecyclerView.Adapter<ServiceTestAdapter.ViewHolder> {

    private List<ListItem> listItems;
    private Context context;

    public ServiceTestAdapter(List<ListItem> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_add_service_test,parent,false);


        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final ListItem listItem = listItems.get(position);

        holder.textViewHead.setText(listItem.getTitle());
        holder.textViewDesc.setText(listItem.getText());

        holder.servicelistcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ((MainActivity)context).setUpdateService(listItem.getTitle(),"1234");
            }
        });

    }

    @Override
    public int getItemCount() {

        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView textViewHead;
        public TextView textViewDesc;
        private LinearLayout servicelistcard;

        public ViewHolder(View itemView) {
            super(itemView);

            textViewHead = (TextView) itemView.findViewById(R.id.txtServiceTitle);
            textViewDesc = (TextView) itemView.findViewById(R.id.txtSellerName);
            servicelistcard = itemView.findViewById(R.id.servicelistcard);
        }
    }
}
