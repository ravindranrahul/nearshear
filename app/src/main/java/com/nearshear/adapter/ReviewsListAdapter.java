package com.nearshear.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.nearshear.Constant;
import com.nearshear.MainActivity;
import com.nearshear.R;
import com.nearshear.api.ApiUtils;
import com.nearshear.model.RegistrationDO;
import com.nearshear.model.ReviewDo;
import com.nearshear.model.SellerDo;
import com.nearshear.utils.Utils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ReviewsListAdapter extends RecyclerView.Adapter<ReviewsListAdapter.MyViewHolder> {

    public static ArrayList<ReviewDo> noteList;
    Context context;
    Utils utils;

    public class MyViewHolder extends RecyclerView.ViewHolder  {
        private TextView txtName;
        private TextView txtReview;
        private ImageView imgUser;
        private TextView ratingBar;

        public MyViewHolder(View view) {
            super(view);
            imgUser = view.findViewById(R.id.imgUser);
            txtName = view.findViewById(R.id.txtName);
            txtReview = view.findViewById(R.id.txtReview);
            ratingBar = view.findViewById(R.id.txtRating);
        }
    }

    public ReviewsListAdapter(ArrayList<ReviewDo> moviesList) {
        this.noteList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_list_item_reviews_list, parent, false);
        context = parent.getContext();
        utils=new Utils(context);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ReviewDo model = noteList.get(position);


        holder.txtName.setText(model.getName());
        holder.txtReview.setText(model.getReview());

        try {
            float rating = Float.parseFloat(model.getRate());
            if (rating == 0) {
                holder.ratingBar.setBackgroundResource(R.drawable.rate_zero);
            } else if (rating == 1) {
                holder.ratingBar.setBackgroundResource(R.drawable.rate_one_two);
            } else if (rating == 2) {
                holder.ratingBar.setBackgroundResource(R.drawable.rate_one_two);
            } else if (rating == 3) {
                holder.ratingBar.setBackgroundResource(R.drawable.rate_three);
            } else if (rating == 4) {
                holder.ratingBar.setBackgroundResource(R.drawable.rate_four_five);
            } else if (rating == 5) {
                holder.ratingBar.setBackgroundResource(R.drawable.rate_four_five);
            }
            holder.ratingBar.setText(model.getRate());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }


        try {
            holder.ratingBar.setText(model.getRate());
//            holder.ratingBar.setRating(Float.parseFloat(model.getRate()));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }


        if (model.getImage() != null && !model.getImage().equals("")) {
            Glide.with(context).load(model.getImage())
                    .thumbnail(0.5f)
                    .crossFade()
                    .placeholder(R.drawable.user_img)
                    .error(R.drawable.user_img)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.imgUser);
        }
    }

    @Override
    public int getItemCount() {
        return noteList.size();
    }

}