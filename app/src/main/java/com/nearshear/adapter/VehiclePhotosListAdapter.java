package com.nearshear.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nearshear.R;
import com.nearshear.model.ImageList;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class VehiclePhotosListAdapter extends RecyclerView.Adapter<VehiclePhotosListAdapter.MyViewHolder> {
    private  ArrayList<ImageList> imageList;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgImage, imgDelete;

        public MyViewHolder(View view) {
            super(view);
            imgImage = view.findViewById(R.id.imgImage);
//            imgDelete = view.findViewById(R.id.imgDelete);
        }
    }

    public VehiclePhotosListAdapter(ArrayList<ImageList> moviesList) {
        this.imageList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_list_item_images, parent, false);
        context = parent.getContext();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ImageList pojo = imageList.get(position);
//
//        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });


        if (pojo.getImage() != null && !pojo.getImage().equals("")) {
            Glide.with(context).load(pojo.getImage())
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.imgImage);
        } else {
            Glide.with(context).load(R.mipmap.image)
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.imgImage);
        }

    }

    @Override
    public int getItemCount() {
        return imageList.size();
    }

}