package com.nearshear.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nearshear.Constant;
import com.nearshear.MainActivity;
import com.nearshear.R;
import com.nearshear.api.ApiUtils;
import com.nearshear.model.RegistrationDO;
import com.nearshear.model.RegistrationInfo;
import com.nearshear.model.SellerDo;
import com.nearshear.utils.Utils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FavouriteListAdapter extends RecyclerView.Adapter<FavouriteListAdapter.MyViewHolder> {

    public static ArrayList<SellerDo> noteList;
    Context context;
    Utils utils;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtServiceTitle;
        public TextView txtSellerName;
        private ImageView imgService;
        private ImageView imgFavourite;
        private LinearLayout layMain;
        private TextView txtRating;
        private  TextView txtDeal;

        public MyViewHolder(View view) {
            super(view);
            imgService = view.findViewById(R.id.imgService);
            txtServiceTitle = view.findViewById(R.id.txtServiceTitle);
            txtSellerName = view.findViewById(R.id.txtSellerName);
            imgFavourite = view.findViewById(R.id.imgFavourite);
            layMain = view.findViewById(R.id.layMain);
            txtRating = view.findViewById(R.id.txtRating);
            txtDeal = view.findViewById(R.id.txtDeal);
        }
    }

    public FavouriteListAdapter(ArrayList<SellerDo> moviesList) {
        this.noteList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_list_item_services_list, parent, false);
        context = parent.getContext();
        utils = new Utils(context);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final SellerDo model = noteList.get(position);


        holder.txtServiceTitle.setText(model.getOrg_name());
        holder.imgFavourite.setImageResource(R.mipmap.like_coloured);
        holder.txtSellerName.setText(model.getDescription());
        holder.txtDeal.setText(model.getDeal());


        try {
            float rating = Float.parseFloat(model.getRating());
            if (rating == 0) {
                holder.txtRating.setBackgroundResource(R.color.rate_zero);
                holder.txtRating.setText(model.getRating_text());
            }
            if (rating >= 1 && rating<2) {
                holder.txtRating.setBackgroundResource(R.color.rate_one_two);
                holder.txtRating.setText(model.getRating());
            }
            if (rating >= 2 && rating<3) {
                holder.txtRating.setBackgroundResource(R.color.rate_one_two);
                holder.txtRating.setText(model.getRating());
            }
            if (rating >= 3 && rating<4) {
                holder.txtRating.setBackgroundResource(R.color.rate_three);
                holder.txtRating.setText(model.getRating());
            }
            if (rating >= 4 && rating<=5) {
                holder.txtRating.setBackgroundResource(R.color.rate_four);
                holder.txtRating.setText(model.getRating());
            }
//            if (rating >= 5) {
//                holder.txtRating.setBackgroundResource(R.color.rate_five);
//                holder.txtRating.setText(model.getRating());
//            }

        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        holder.imgFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callFavourite(model, "0", position);
            }
        });

        holder.layMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)context).setServiceDetails("Service Details",model.getService_id());
            }
        });

        if (model.getImage() != null && !model.getImage().equals("")) {
            Glide.with(context).load(model.getImage())
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.imgService);
        }


    }

    @Override
    public int getItemCount() {
        return noteList.size();
    }

    private void callFavourite(final SellerDo model, final String action, final int position) {
        String id = utils.getPreference(Constant.PREF_ID);
        utils.startProgress();
        ApiUtils.getAPIService().requestAddRemoveFavourite(id, model.getService_id(), action).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                utils.dismissProgress();
                try {
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();

                    if (success.equals("1")) {
                        ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                        listOfPostData = response.body().getPostdata();
                        String favourite_count = listOfPostData.get(0).getTotal_fav_count();
                        ((MainActivity)context).setFavouriteCount(favourite_count);

                        noteList.remove(position);
                        notifyDataSetChanged();
                    } else if (success.equals("0")) {
                        utils.Toast(msg, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                utils.Toast("Please check connection", null);
            }
        });
    }

}