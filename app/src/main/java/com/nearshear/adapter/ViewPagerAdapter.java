package com.nearshear.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nearshear.R;
import com.nearshear.model.SliderDo;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import jp.wasabeef.picasso.transformations.BlurTransformation;

public class ViewPagerAdapter extends PagerAdapter {

	Context context;
	ArrayList<SliderDo> imageViewPager;
	LayoutInflater inflater;

	public ViewPagerAdapter(Context context, ArrayList<SliderDo> imageViewPager) {
		this.context = context;
		this.imageViewPager = imageViewPager;
	}

	@Override
	public int getCount() {
		return imageViewPager.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == ((LinearLayout) object);
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {

		ImageView img;
		final ImageView imgBack;
		TextView txtTitle;

		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View itemView = inflater.inflate(R.layout.raw_list_item_slider,
				container, false);

		img=  itemView.findViewById(R.id.img);
		imgBack =  itemView.findViewById(R.id.imgBack);
		txtTitle =  itemView.findViewById(R.id.txtTitle);

		SliderDo model = imageViewPager.get(position);
		txtTitle.setText(model.getTitle());
		if (imageViewPager.get(position).toString() != null) {
			Glide.with(context).load(model.getImage())
					.thumbnail(0.5f)
					.crossFade()
					.diskCacheStrategy(DiskCacheStrategy.ALL)
					.into(img);

			Picasso.with(context).load(model.getImage()).transform(new BlurTransformation(context)).into(imgBack);

		}

		((ViewPager) container).addView(itemView);

		return itemView;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		// Remove viewpager_item.xml from ViewPager
		((ViewPager) container).removeView((LinearLayout) object);

	}

}
