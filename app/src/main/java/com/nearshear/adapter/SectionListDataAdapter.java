package com.nearshear.adapter;

/**
 * Created by pratap.kesaboyina on 24-12-2014.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nearshear.MainActivity;
import com.nearshear.R;
import com.nearshear.model.SubCategoryDo;

import java.util.ArrayList;

public class SectionListDataAdapter extends RecyclerView.Adapter<SectionListDataAdapter.SingleItemRowHolder> {

    private ArrayList<SubCategoryDo> itemsList;
    private Context mContext;

    public SectionListDataAdapter(Context context, ArrayList<SubCategoryDo> itemsList) {
        this.itemsList = itemsList;
        this.mContext = context;
    }

    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.raw_list_item_sub_category_list, null);
        SingleItemRowHolder mh = new SingleItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(SingleItemRowHolder holder, int i) {

        final SubCategoryDo model = itemsList.get(i);



        holder.txtSubCategoryName.setText(model.getSub_category_name());
        holder.layMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)mContext).setServicesList(model.getSub_category_name(),model.getSub_category_id());
            }
        });
        holder.txtSubCategoryName.setText(model.getSub_category_name());

        if (model.getSub_category_image() != null && !model.getSub_category_name().equals("")) {
            Glide.with(mContext).load(model.getSub_category_image())
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.imgSubCategoryImage);
        } else {
//            holder.imgUserImage.setImageResource(0);
        }

       /* Glide.with(mContext)
                .load(feedItem.getImageURL())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .error(R.drawable.bg)
                .into(feedListRowHolder.thumbView);*/
    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView txtSubCategoryName;
        private ImageView imgSubCategoryImage;
        private LinearLayout layMain;


        public SingleItemRowHolder(View view) {
            super(view);

            this.txtSubCategoryName = (TextView) view.findViewById(R.id.txtSubCategoryName);
            this.imgSubCategoryImage = view.findViewById(R.id.imgSubCategoryImage);
            this.txtSubCategoryName = view.findViewById(R.id.txtSubCategoryName);
            this.layMain = view.findViewById(R.id.layMain);


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });


        }

    }

}