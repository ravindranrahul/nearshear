package com.nearshear.adapter;

import android.content.Context;
import android.media.Image;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nearshear.Constant;
import com.nearshear.MainActivity;
import com.nearshear.R;
import com.nearshear.model.CategoryDo;
import com.nearshear.model.SubCategoryDo;

import java.util.ArrayList;

public class RecyclerViewDataAdapter extends RecyclerView.Adapter<RecyclerViewDataAdapter.ItemRowHolder> {

    private ArrayList<CategoryDo> dataList;
    private Context mContext;
    private LinearLayoutManager layoutManager;

    public RecyclerViewDataAdapter(Context context, ArrayList<CategoryDo> dataList) {
        this.dataList = dataList;
        this.mContext = context;
    }

    @Override
    public ItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.raw_list_item_category, null);
        ItemRowHolder mh = new ItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(final ItemRowHolder itemRowHolder, int i) {

        final CategoryDo model = dataList.get(i);

        itemRowHolder.txtCategoryName.setText(model.getCategory_name());

        SectionListDataAdapter itemListDataAdapter = new SectionListDataAdapter(mContext, model.getSub_category_data());

        itemRowHolder.recycler_view_list.setHasFixedSize(true);

        layoutManager
                = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        itemRowHolder.recycler_view_list.setLayoutManager(layoutManager);
        itemRowHolder.recycler_view_list.setAdapter(itemListDataAdapter);

        ArrayList<SubCategoryDo> data = model.getSub_category_data();
        if (data.size() > 3) {
            itemRowHolder.img_right_scroll.setVisibility(View.VISIBLE);
        } else {
            itemRowHolder.img_right_scroll.setVisibility(View.GONE);
        }

        itemRowHolder.img_right_scroll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int val = layoutManager.findLastVisibleItemPosition();
                Log.d("val", String.valueOf(val));
                itemRowHolder.recycler_view_list.smoothScrollToPosition(layoutManager.findLastVisibleItemPosition() + 1);
            }
        });


        itemRowHolder.recycler_view_list.setNestedScrollingEnabled(false);


       /*  itemRowHolder.recycler_view_list.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;
                    case MotionEvent.ACTION_UP:
                        //Allow ScrollView to intercept touch events once again.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }
                // Handle RecyclerView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });*/

        itemRowHolder.btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constant.allSubCatList=model.getSub_category_data();
                ((MainActivity)mContext).setViewAllSubCategoreies(model.getCategory_name(),model.getCategory_id());
            }
        });

    }

    @Override
    public int getItemCount() {
        return (null != dataList ? dataList.size() : 0);
    }

    public class ItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView txtCategoryName;
        protected RecyclerView recycler_view_list;
        protected TextView btnMore;
        protected ImageView img_right_scroll;


        public ItemRowHolder(View view) {
            super(view);

            this.txtCategoryName = view.findViewById(R.id.txtCategoryName);
            this.recycler_view_list = view.findViewById(R.id.rViewListSubCategory);
            this.btnMore = view.findViewById(R.id.txtViewAll);
            this.img_right_scroll = view.findViewById(R.id.img);
        }

    }

}