package com.nearshear.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nearshear.MainActivity;
import com.nearshear.R;
import com.nearshear.model.SubCategoryDo;

import java.util.ArrayList;


public class SubCatistAdapter extends RecyclerView.Adapter<SubCatistAdapter.MyViewHolder> {

    public static ArrayList<SubCategoryDo> noteList;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtSubCategoryName;
        private ImageView imgSubCategoryImage;
        private LinearLayout layMain;

        public MyViewHolder(View view) {
            super(view);
            imgSubCategoryImage = view.findViewById(R.id.imgSubCategoryImage);
            txtSubCategoryName = view.findViewById(R.id.txtSubCategoryName);
            layMain = view.findViewById(R.id.layMain);

        }
    }


    public SubCatistAdapter(ArrayList<SubCategoryDo> moviesList) {
        this.noteList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_list_item_sub_category_list, parent, false);
        context = parent.getContext();

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final SubCategoryDo model = noteList.get(position);


        holder.layMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)context).setServicesList(model.getSub_category_name(),model.getSub_category_id());
            }
        });
        holder.txtSubCategoryName.setText(model.getSub_category_name());

        if (model.getSub_category_image() != null && !model.getSub_category_name().equals("")) {
            Glide.with(context).load(model.getSub_category_image())
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.imgSubCategoryImage);
        } else {
//            holder.imgUserImage.setImageResource(0);
        }
    }

    @Override
    public int getItemCount() {
        return noteList.size();
    }

}