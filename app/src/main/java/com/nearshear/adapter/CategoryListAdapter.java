package com.nearshear.adapter;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nearshear.Constant;
import com.nearshear.MainActivity;
import com.nearshear.R;
import com.nearshear.SpacesItemDecoration;
import com.nearshear.model.CategoryDo;

import java.util.ArrayList;

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.MyViewHolder> {

    public static ArrayList<CategoryDo> noteList;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView txtCategoryName, txtViewAll;
        private RecyclerView rViewList;
        private LinearLayoutManager layoutManager;
        GridLayoutManager gridLayoutManager;

        private SubCatistAdapter adapterSubCat;

        public MyViewHolder(View view) {
            super(view);
            txtCategoryName = view.findViewById(R.id.txtCategoryName);
            txtViewAll = view.findViewById(R.id.txtViewAll);
            layoutManager
                    = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
//            gridLayoutManager = new GridLayoutManager(context, 1, GridLayoutManager.HORIZONTAL, false);
//
//            gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
//                @Override
//                public int getSpanSize(int position) {
//                    return (position % 1 == 0 ? 1 : 1);
//                }
//            });
            rViewList = view.findViewById(R.id.rViewListSubCategory);
            rViewList.setHasFixedSize(true);
            rViewList.setLayoutManager(layoutManager);
//            int spacingInPixels = context.getResources().getDimensionPixelSize(R.dimen.space5);
//            rViewList.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

//            ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(context, R.dimen.item_offset);
//            mRecyclerView.addItemDecoration(itemDecoration);
        }
    }

    public CategoryListAdapter(ArrayList<CategoryDo> moviesList) {
        this.noteList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_list_item_category, parent, false);
        context = parent.getContext();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final CategoryDo model = noteList.get(position);

        holder.txtCategoryName.setText(model.getCategory_name());
        holder.adapterSubCat = new SubCatistAdapter(model.getSub_category_data());
        holder.rViewList.setAdapter(holder.adapterSubCat);
        holder.txtViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constant.allSubCatList = model.getSub_category_data();
                ((MainActivity) context).setViewAllSubCategoreies(model.getCategory_name(), model.getCategory_id());
            }
        });
    }

    @Override
    public int getItemCount() {
        return noteList.size();
    }

}