package com.nearshear.adapter;

import android.content.Context;
import android.graphics.Color;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.nearshear.MainActivity;
import com.nearshear.R;
import com.nearshear.api.RequestAPI;
import com.nearshear.model.Datum;
import com.nearshear.model.GetAllServices;
import com.nearshear.model.ListItem;
import com.nearshear.model.RegistrationDO;
import com.nearshear.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GetAllServicesAdapter extends RecyclerView.Adapter<GetAllServicesAdapter.ViewHolder> {

    private List<Datum> listItems;
    private Context context;

    public GetAllServicesAdapter(List<Datum> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }

    @NonNull
    @Override
    public GetAllServicesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_get_all_services,parent,false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final GetAllServicesAdapter.ViewHolder holder, int position) {

        Log.d("rahul","On bind called adapter");

        final Datum getAllServices = listItems.get(position);

        holder.txtServiceTitle.setText(getAllServices.getServiceName());



//        commented rating code...
//        if (getAllServices.getRating().equalsIgnoreCase("0.0")){
//            Log.d("rating","inside if");
//            holder.txtRating.setText("No Rating Available");
//            holder.txtRating.setBackgroundColor(Color.parseColor("#FF6600"));
//        } else {
//            holder.txtRating.setText(getAllServices.getRating());
//        }
//        holder.txtSellerName.setText(getAllServices.getDescription());
//        holder.txtDeal.setText(getAllServices.getOfferBonusTitle());
//        holder.imgService.setImageResource();
//        holder.imgService.

        String check = getAllServices.getStatus();

        Log.d("check ",check);

        if (check.equalsIgnoreCase("1")){
            holder.imgApproved.setVisibility(View.VISIBLE);
            holder.txtStatus.setVisibility(View.GONE);
        } else {
            holder.txtStatus.setVisibility(View.VISIBLE);
            holder.imgApproved.setVisibility(View.GONE);
            holder.switchButton.setClickable(false);
        }

        if (getAllServices.getActive().equalsIgnoreCase("1")){
            Log.d("check","inside if");
            holder.switchButton.setChecked(true);
            holder.switchButton.setText("Active");
        } else {
            holder.switchButton.setChecked(false);
            holder.txtActive.setText("Inactive");
            holder.switchButton.setText("Inactive");
        }

//        holder.noDataImg.setVisibility(View.GONE);
//
//        holder.noDataImg.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d("noData","clicked");
//                ((MainActivity)context).setBecomePartner("Become Partner");
//            }
//        });

//        holder.servicelistcard.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d("rahul adapter",""+""+getAllServices.getPartnerId()+getAllServices.getName()+" "+getAllServices.getPhone()+" "+getAllServices.getServiceName()+" "+getAllServices.getArea()+" "+getAllServices.getPin());
//                Log.d("rahul ",""+getAllServices.getCity()+" "+getAllServices.getAddress()+" "+getAllServices.getOfferBonusTitle()+" "+getAllServices.getCatName()+" "+getAllServices.getSubCatName());
//                ((MainActivity)context).getServiceDetails("details",getAllServices.getPartnerId());
//            }
//        });

        holder.servicelistcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("rahul adapter",""+""+getAllServices.getPartnerId()+getAllServices.getName()+" "+getAllServices.getPhone()+" "+getAllServices.getServiceName()+" "+getAllServices.getArea()+" "+getAllServices.getPin());
                Log.d("rahul ",""+getAllServices.getCity()+" "+getAllServices.getAddress()+" "+getAllServices.getOfferBonusTitle()+" "+getAllServices.getCatName()+" "+getAllServices.getSubCatName());
                ((MainActivity)context).setServiceDetails("details",getAllServices.getPartnerId());
            }
        });

        holder.llnoshow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Log.d("noshow","do nothing");
            }
        });

        holder.lledit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("rahul adapter",""+getAllServices.getName()+" "+getAllServices.getPhone()+" "+getAllServices.getServiceName()+" "+getAllServices.getArea()+" "+getAllServices.getPin());
                Log.d("rahul ",""+getAllServices.getCity()+" "+getAllServices.getAddress()+" "+getAllServices.getOfferBonusTitle()+" "+getAllServices.getCatName()+" "+getAllServices.getSubCatName());
                ((MainActivity)context).setUpdateService("Update "+getAllServices.getServiceName(),getAllServices.getPartnerId(),getAllServices.getServiceName(),getAllServices.getSocietyName(),getAllServices.getArea(),getAllServices.getPin(),getAllServices.getCity(),getAllServices.getServicePhone(),getAllServices.getCatId(),getAllServices.getSubCatId(),getAllServices.getOfferBonusTitle(),getAllServices.getDescription(),getAllServices.getCatName(),getAllServices.getSubCatName(),getAllServices.getCoverImage());
            }
        });

        if (getAllServices.getCoverImage().equalsIgnoreCase("")){
            Log.d("check ","inside if do nothing");
        } else {
            Picasso.with(context)
                    .load("http://nearshear.com/staging/backend/"+getAllServices.getCoverImage())
                    .into(holder.imgService);
        }



        Log.d("img","http://nearshear.com/staging/backend/"+getAllServices.getCoverImage());

        holder.switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.d("switch","button changed");

                if (isChecked){
                    View v = (View) buttonView.getParent();
//                    v.findViewById()
                    buttonView.setText("Active");
                    Log.d("switch","true");
                    Toast.makeText(context,"Your service is Active now",Toast.LENGTH_SHORT).show();

//                    holder.txtActive.setVisibility(View.VISIBLE);
//                    holder.imgApproved.setVisibility(View.VISIBLE);
                    holder.txtActive.setText("Active");

                    RequestBody id = RequestBody.create(MediaType.parse("text/plain"),getAllServices.getPartnerId());
                    RequestBody active = RequestBody.create(MediaType.parse("text/plain"),"1");

                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl("http://nearshear.com/staging/backend/api/partners/")
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();

                    final RequestAPI requestAPI = retrofit.create(RequestAPI.class);

                    Call<RegistrationDO> call = requestAPI.updateActive(id,active);

                    call.enqueue(new Callback<RegistrationDO>() {
                        @Override
                        public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                            Log.d("Response",""+response.raw().request().url());
                            Log.d("Response code:", "" + response.code());
                        }

                        @Override
                        public void onFailure(Call<RegistrationDO> call, Throwable t) {
                            Log.d("error in request",t.getMessage());
                        }
                    });

                } else {
                    Log.d("switch","false");
                    buttonView.setText("Inactive");
                    Toast.makeText(context,"You have temporarily de-activated your service",Toast.LENGTH_SHORT).show();

                    holder.txtActive.setText("Inactive");

                    RequestBody id = RequestBody.create(MediaType.parse("text/plain"),getAllServices.getPartnerId());
                    RequestBody active = RequestBody.create(MediaType.parse("text/plain"),"0");

                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl("http://nearshear.com/staging/backend/api/partners/")
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();

                    final RequestAPI requestAPI = retrofit.create(RequestAPI.class);

                    Call<RegistrationDO> call = requestAPI.updateActive(id,active);

                    call.enqueue(new Callback<RegistrationDO>() {
                        @Override
                        public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                            Log.d("Response",""+response.raw().request().url());
                            Log.d("Response code:", "" + response.code());
                        }

                        @Override
                        public void onFailure(Call<RegistrationDO> call, Throwable t) {
                            Log.d("error in request",t.getMessage());
                        }
                    });

//                    holder.imgApproved.setVisibility(View.INVISIBLE);

                }
            }
        });


    }


    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtServiceTitle;
        public TextView txtSellerName;
        private LinearLayout servicelistcard;
        private ImageView imgFavourite;
        private ImageView action_add;
        private TextView txtDeal;
        private ImageView imgService;
        private TextView txtRating;
        private Switch switchButton;
        private ImageView imgApproved;

        private String checkStatus;

        private LinearLayout lledit;
        private TextView txtActive;
        private LinearLayout llnoshow;
        private TextView txtStatus;

        private ImageView noDataImg;

        public ViewHolder(View itemView) {
            super(itemView);

            txtServiceTitle = (TextView) itemView.findViewById(R.id.txtServiceTitle);
//            txtSellerName = (TextView) itemView.findViewById(R.id.txtSellerName);
            servicelistcard = itemView.findViewById(R.id.servicelistcard);
//            imgFavourite = (ImageView) itemView.findViewById(R.id.imgFavourite);
//            txtDeal = (TextView) itemView.findViewById(R.id.txtDeal);
            imgService = (ImageView) itemView.findViewById(R.id.imgService);
//            txtRating = (TextView) itemView.findViewById(R.id.txtRating);
            switchButton = (Switch) itemView.findViewById(R.id.switchButton);

            imgApproved = (ImageView) itemView.findViewById(R.id.imgApproved);
            lledit = itemView.findViewById(R.id.lledit);

            txtActive = (TextView) itemView.findViewById(R.id.txtActive);
            llnoshow = itemView.findViewById(R.id.llnoshow);
            txtStatus = (TextView) itemView.findViewById(R.id.txtStatus);

            noDataImg = (ImageView) itemView.findViewById(R.id.noDataImg);
        }
    }
}
