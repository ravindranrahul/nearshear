package com.nearshear.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nearshear.Constant;
import com.nearshear.MainActivity;
import com.nearshear.R;
import com.nearshear.api.ApiUtils;
import com.nearshear.model.PhoneDo;
import com.nearshear.model.RegistrationDO;
import com.nearshear.model.SellerDo;
import com.nearshear.utils.Utils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PhoneListAdapter extends RecyclerView.Adapter<PhoneListAdapter.MyViewHolder> {

    public static ArrayList<PhoneDo> noteList;
    private Context context;
    private Utils utils;
    private String action = "";


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtPhoneNo;
        private ImageView imgRemove;
        private RelativeLayout layAdd;
        private LinearLayout layDetail;
        private TextView txtPhone;
        private ImageView imgCall;


        public MyViewHolder(View view) {
            super(view);
            imgRemove = view.findViewById(R.id.imgRemove);
            txtPhoneNo = view.findViewById(R.id.txtPhoneNo);
            layAdd = view.findViewById(R.id.layAdd);
            layDetail = view.findViewById(R.id.layDetail);
            txtPhone = view.findViewById(R.id.txtPhone);
            imgCall = view.findViewById(R.id.imgCall);
        }
    }

    public PhoneListAdapter(ArrayList<PhoneDo> moviesList, String action) {
        this.noteList = moviesList;
        this.action = action;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_list_item_phone_list, parent, false);
        context = parent.getContext();
        utils = new Utils(context);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final PhoneDo model = noteList.get(position);


        if (action.equals("add")) {
            holder.txtPhoneNo.setText(model.getPhone());
            holder.imgRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    noteList.remove(position);
                    notifyDataSetChanged();
                }
            });
            holder.layDetail.setVisibility(View.GONE);
        } else {
            holder.layAdd.setVisibility(View.GONE);
            holder.txtPhone.setText(model.getPhone());
        }

        holder.imgCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+model.getPhone()));
                context.startActivity(intent);
            }
        });

        holder.layDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+model.getPhone()));
                context.startActivity(intent);
            }
        });



    }

    @Override
    public int getItemCount() {
        return noteList.size();
    }

}