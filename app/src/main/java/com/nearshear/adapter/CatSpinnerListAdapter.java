package com.nearshear.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nearshear.R;
import com.nearshear.model.CategoryDo;

import java.util.ArrayList;


public class CatSpinnerListAdapter extends BaseAdapter {
    private Context context;
    public static ArrayList<CategoryDo> serviceList;
    ViewHolder holder = null;


    public CatSpinnerListAdapter(Context context,
                                 ArrayList<CategoryDo> service) {
        this.context = context;
        this.serviceList = service;
    }

    @Override
    public int getCount() {
        return serviceList.size();
    }

    @Override
    public Object getItem(int position) {
        return serviceList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // ViewHolder holder = null;
        final int pos = position;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.raw_list_item_spinner_category,
                    parent, false);
            holder = new ViewHolder();

            holder.txtSpinnerText= convertView.findViewById(R.id.txtSpinnerText);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        try {
            final CategoryDo pojo = serviceList.get(position);

            holder.txtSpinnerText.setText(pojo.getCategory_name());

//            if(position!=0){
//                holder.txtSpinnerText.setTextColor(context.getResources().getColor(R.color.black));
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    class ViewHolder {
        TextView txtSpinnerText;
    }

}

